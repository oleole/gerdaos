define([ "./textparser" ], function(e) {
    function n(e, n) {
        var t = new Uint8Array(e.byteLength + n.byteLength);
        return t.set(new Uint8Array(e), 0), t.set(new Uint8Array(n), e.byteLength), t.buffer;
    }
    function t() {
        o.apply(this, arguments);
    }
    var o = e.TextParser;
    return t.prototype = {
        parse: function(e) {
            this._buffer = this._buffer ? n(this._buffer, e) : e, o.prototype.parse.apply(this, arguments);
        },
        complete: function() {
            var e = o.prototype.complete.apply(this, arguments);
            return e.buffer = this._buffer, e;
        }
    }, {
        SnippetParser: t
    };
});