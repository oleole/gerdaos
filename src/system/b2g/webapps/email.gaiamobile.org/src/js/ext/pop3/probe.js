define([ "./pop3", "../syncbase", "logic", "../errorutils", "exports" ], function(e, n, r, t, o) {
    function s(e) {
        return e && e.name ? "bad-user-or-pass" === e.name && e.message && i.test(e.message) ? "pop3-disabled" : "bad-user-or-pass" === e.name && e.message && c.test(e.message) ? "pop3-disabled" : "unresponsive-server" === e.name && e.exception && e.exception.name && /security/i.test(e.exception.name) ? "bad-security" : ("unresponsive-server" === e.name || "bad-user-or-pass" === e.name) && e.message && /\[(LOGIN-DELAY|SYS|IN-USE)/i.test(e.message) ? "server-maintenance" : e.name : null;
    }
    var a = r.scope("Pop3Prober");
    o.probeAccount = function(t, o) {
        var s = {
            host: o.hostname,
            port: o.port,
            crypto: o.crypto,
            username: t.username,
            password: t.password,
            connTimeout: n.CONNECT_TIMEOUT_MS
        };
        r(a, "connecting", {
            connInfo: o
        });
        var i, c, u = new Promise(function(e, n) {
            i = e, c = n;
        }), d = new e.Pop3Client(s, function(e) {
            return e ? (c(e), void 0) : (d.protocol.sendRequest("UIDL", [ "1" ], !1, function(e, n) {
                n ? d.protocol.sendRequest("TOP", [ "1", "0" ], !0, function(e, n) {
                    n ? i(d) : e.err ? (r(a, "server-not-great", {
                        why: "no TOP"
                    }), c("pop-server-not-great")) : c(n.err);
                }) : d.protocol.sendRequest("UIDL", [], !0, function(e, n) {
                    n ? i(d) : e.err ? (r(a, "server-not-great", {
                        why: "no UIDL"
                    }), c("pop-server-not-great")) : c(n.err);
                });
            }), void 0);
        });
        return u.then(function(e) {
            return r(a, "success"), {
                conn: e,
                timezoneOffset: null
            };
        }).catch(function(e) {
            return e = l(e), r(a, "error", {
                error: e
            }), d && d.close(), Promise.reject(e);
        });
    };
    var i = /\[SYS\/PERM\] Your account is not enabled for POP/, c = /\[SYS\/PERM\] POP access is disabled for your domain\./, l = o.normalizePop3Error = function(e) {
        var n = s(e) || t.analyzeException(e) || "unknown";
        return r(a, "normalized-error", {
            error: e,
            reportAs: n
        }), n;
    };
});