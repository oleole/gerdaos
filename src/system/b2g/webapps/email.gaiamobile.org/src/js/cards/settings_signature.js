define([ "require", "tmpl!./sig/save_signature.html", "cards", "./base", "template!./settings_signature.html", "./editor_mixins" ], function(e) {
    var t = e("tmpl!./sig/save_signature.html"), n = e("cards"), o = /\s+$/;
    return [ e("./base")(e("template!./settings_signature.html")), e("./editor_mixins"), {
        onArgs: function(e) {
            this.account = e.account, this.identity = this.account.identities[0], this._bindEditor(this.signatureNode), 
            this.populateEditor(this.identity.signature || "");
        },
        getTextFromEditor: function() {
            var e = this.fromEditor().replace(o, "");
            return e;
        },
        goBack: function() {
            n.removeCardAndSuccessors(this, "animate", 1);
        },
        onBack: function() {
            var e = this.getTextFromEditor();
            if (e === this.identity.signature) return this.goBack(), void 0;
            var n = t.cloneNode(!0);
            this._savePromptMenu = n, document.body.appendChild(n);
            var o = function(t) {
                switch (document.body.removeChild(n), this._savePromptMenu = null, t.explicitOriginalTarget.id) {
                  case "sig-save":
                    this.identity.modifyIdentity({
                        signature: e
                    }), this.goBack();
                    break;

                  case "sig-discard":
                    this.goBack();
                    break;

                  case "sig-cancel":                }
                return !1;
            }.bind(this);
            n.addEventListener("submit", o);
        },
        onClickDone: function() {
            var e = this.getTextFromEditor();
            e !== this.identity.signature && this.identity.modifyIdentity({
                signature: e
            }), this.onBack();
        },
        onCardVisible: function() {
            var e = window.getSelection();
            e.rangeCount > 0 && e.removeAllRanges();
            var t = this.signatureNode.lastChild;
            if (t) {
                var n = document.createRange();
                n.setStartAfter(t), n.setEndAfter(t), e.addRange(n);
            }
            this.signatureNode.focus();
        },
        die: function() {}
    } ];
});