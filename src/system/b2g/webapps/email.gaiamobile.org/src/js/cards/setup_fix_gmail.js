define([ "require", "cards", "./base", "template!./setup_fix_gmail.html" ], function(e) {
    var t = e("cards");
    return [ e("./base")(e("template!./setup_fix_gmail.html")), {
        extraClasses: [ "anim-fade", "anim-overlay" ],
        onArgs: function(e) {
            this.account = e.account, this.restoreCard = e.restoreCard, this.accountNode.textContent = this.account.name;
            var t = {
                "sup-account-header-label": "setup-gmail-{ACCOUNT_TYPE}-header",
                "sup-enable-label": "setup-gmail-{ACCOUNT_TYPE}-message",
                "sup-dismiss-btn": "setup-gmail-{ACCOUNT_TYPE}-retry"
            }, n = "imap+smtp" === this.account.type ? "imap" : "pop3";
            for (var o in t) {
                var r = t[o].replace("{ACCOUNT_TYPE}", n);
                this.getElementsByClassName(o)[0].setAttribute("data-l10n-id", r);
            }
        },
        die: function() {},
        onDismiss: function() {
            this.account.clearProblems(), t.removeCardAndSuccessors(this, "animate", 1, this.restoreCard);
        }
    } ];
});