
define('tmpl!cards/msg/contact_menu.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form class="msg-contact-menu" role="dialog" data-type="action">\n   <header></header>\n   <menu>\n    <button class="msg-contact-menu-new collapsed" data-l10n-id="message-contact-menu-new">\n    </button>\n    <button class="msg-contact-menu-reply collapsed" data-l10n-id="message-contact-menu-reply">\n    </button>\n    <button class="msg-contact-menu-view collapsed" data-l10n-id="message-contact-menu-view">\n    </button>\n    <button class="msg-contact-menu-create-contact collapsed" data-l10n-id="message-contact-menu-create">\n    </button>\n    <button class="msg-contact-menu-add-to-existing-contact collapsed" data-l10n-id="message-contact-menu-add-existing">\n    </button>\n     <button class="msg-contact-menu-cancel" data-l10n-id="message-multiedit-cancel">\n    </button>\n   </menu>\n </form>'); });

define('tmpl!cards/msg/reply_menu.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form class="msg-reply-menu" role="dialog" data-type="action">\n  <header></header>\n  <menu>\n    <button class="msg-reply-menu-reply" data-l10n-id="message-reply-menu-reply">\n    </button>\n    <button class="msg-reply-menu-reply-all" data-l10n-id="message-reply-menu-reply-all">\n    </button>\n    <button class="msg-reply-menu-forward" data-l10n-id="message-reply-menu-forward">\n    </button>\n    <button class="msg-reply-menu-cancel" data-l10n-id="message-reply-menu-cancel">\n    </button>\n  </menu>\n</form>\n'); });

define('tmpl!cards/msg/browse_confirm.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form role="dialog" class="msg-browse-confirm" data-type="confirm">\n  <section>\n    <h1 data-l10n-id="confirm-dialog-title"></h1>\n    <p></p>\n  </section>\n  <menu>\n    <button id="msg-browse-cancel" data-l10n-id="message-multiedit-cancel"></button>\n    <button id="msg-browse-ok" class="recommend" data-l10n-id="dialog-button-ok"></button>\n  </menu>\n</form>'); });

define('tmpl!cards/msg/peep_bubble.html',['tmpl'], function (tmpl) { return tmpl.toDom('<div class="msg-peep-bubble peep-bubble" dir="auto" role="option">\n  <span class="msg-peep-content p-pri"></span>\n</div>\n'); });

define('tmpl!cards/msg/attachment_item.html',['tmpl'], function (tmpl) { return tmpl.toDom('<li class="msg-attachment-item">\n  <span class="msg-attachment-icon" role="presentation"></span>\n  <span class="msg-attachment-fileinfo">\n    <span dir="auto" class="msg-attachment-filename"></span>\n    <span class="msg-attachment-filesize"></span>\n    <span data-l10n-id="message-attachment-too-large"\n          class="msg-attachment-too-large"></span>\n  </span>\n  <button class="msg-attachment-download" data-l10n-id="download-button">\n    <span class="icon icon-download"></span></button>\n  <span class="msg-attachment-downloading">\n    <progress class="small" data-l10n-id="downloading-progress"></progress>\n  </span>\n  <button class="msg-attachment-view" data-l10n-id="view-button">\n    <span data-l10n-id="message-attachment-view" class="icon icon-view"></span>\n  </button>\n</li>\n'); });

define('tmpl!cards/msg/attachment_disabled_confirm.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form role="dialog" class="msg-attachment-disabled-confirm" data-type="confirm">\n  <section>\n    <p><span data-l10n-id="message-send-attachment-disabled-confirm"></span></p>\n  </section>\n  <menu>\n    <button id="msg-attachment-disabled-cancel" data-l10n-id="message-multiedit-cancel"></button>\n    <button id="msg-attachment-disabled-ok" data-l10n-id="dialog-button-ok"></button>\n  </menu>\n</form>'); });

define('tmpl!cards/msg/attachment_did_not_open_alert.html',['tmpl'], function (tmpl) { return tmpl.toDom('<form role="dialog" class="msg-attachment-did-not-open" data-type="confirm">\n  <section>\n    <h1 data-l10n-id="message-attachment-did-not-open-label"></h1>\n    <p><span data-l10n-id="message-attachment-did-not-open-body"></span></p>\n  </section>\n  <menu>\n    <button id="msg-attachment-did-not-open-ok" data-l10n-id="dialog-button-ok"></button>\n  </menu>\n</form>\n'); });



define('marquee',[],function() {

/**
 * HTML Marquee in JavaScript/CSS
 * - slow scrolling of text depending on `behavior' and `timingFunction'
 *   parameters provided to the `activate' method
 * - start aligned left with delay (see marquee.css for details on classes)
 *
 * Creates a HTML element of the form:
 *  <headerNode>
 *    <div id="marquee-h-wrapper">
 *      <div id="marquee-h-text" class="marquee">
 *          Marqueed text content
 *      </div>
 *    </div>
 *  </headerNode>
 * where 'headerNode' is the node that will containt the text that needs a
 * marquee (i.e. <header>, <h1>, etc.).
 */
var Marquee = {
  /**
   * List of supported timing functions
   */
  timingFunction: ['linear', 'ease'],

  /**
   * Setup the marquee DOM structure
   * @param {string} text the string of text that requires a marquee.
   * @param {element} headerNode the DOM element parent of the text.
   */
  setup: function marquee_setup(text, headerNode) {
    this._headerNode = headerNode;

    this._headerWrapper = document.getElementById('marquee-h-wrapper');
    if (!this._headerWrapper) {
      this._headerWrapper = document.createElement('div');
      this._headerWrapper.id = 'marquee-h-wrapper';
      this._headerNode.appendChild(this._headerWrapper);
    }

    var headerText = document.getElementById('marquee-h-text');
    if (!headerText) {
      headerText = document.createElement('div');
      headerText.id = 'marquee-h-text';
      this._headerWrapper.dir = 'auto';
      this._headerWrapper.appendChild(headerText);
    }

    headerText.textContent = text;
  },

  /**
   * Activate the marquee
   * NOTE: This should only be called once the DOM structure is updated with
   *       Marquee.setup() and all created DOM elements are appended to the
   *       document, otherwise the text overflow check will not work properly.
   * @param {string} behavior the way the marquee behaves: 'scroll' (default)
   *                           for continuous right-to-left (rtl) scrolling or
   *                           'alternate' for alternating right-to-left and
   *                           left-to-right scrolling.
   * @param {string} timingFun the animation timing function: 'linear' (default)
   *                           for linear animation speed, or 'ease' for slow
   *                           start of the animation.
   */
  activate: function marquee_activate(behavior, timingFun) {
    if (!this._headerNode || !this._headerWrapper) {
      return;
    }

    // Set defaults for arguments
    var mode = behavior || 'scroll';
    var tf = timingFun || null;
    var timing = (Marquee.timingFunction.indexOf(tf) >= 0) ? tf : 'linear';
    var marqueeCssClass = 'marquee';

    var titleText = document.getElementById('marquee-h-text');
    var cssClass, width;

    // Regardless of text character alignment, still want shorter text that does
    // not span the whole element area to be aligned to match the UI. The
    // dir="auto" on titleText will still preserve the correct order of the
    // characters in the text.
    titleText.classList.remove('ltr-align');
    titleText.classList.remove('rtl-align');
    titleText.classList.add((document.dir === 'rtl' ? 'rtl' : 'ltr') +
                           '-align');

    // Check if the title text overflows, and if so, add the marquee class
    // NOTE: this can only be checked it the DOM structure is updated
    //       through Marquee.setup()
    if (this._headerWrapper.clientWidth < this._headerWrapper.scrollWidth) {
      // Track the CSS classes added to the text
      this._marqueeCssClassList = [];
      switch (mode) {
        case 'scroll':
          cssClass = marqueeCssClass + '-rtl';
          // Set the width of the marquee to match the text contents length
          width = this._headerWrapper.scrollWidth;
          titleText.style.width = width + 'px';
          // Start the marquee animation (aligned left with delay)
          titleText.classList.add(cssClass + '-start-' + timing);
          this._marqueeCssClassList.push(cssClass + '-start-' + timing);

          var self = this;
          titleText.addEventListener('animationend', function() {
            titleText.classList.remove(cssClass + '-start-' + timing);
            this._marqueeCssClassList.pop();
            // Correctly calculate the width of the marquee
            var visibleWidth = self._headerWrapper.clientWidth + 'px';
            titleText.style.transform = 'translateX(' + visibleWidth + ')';
            // Enable the continuous marquee
            titleText.classList.add(cssClass);
            this._marqueeCssClassList.push(cssClass);
          });
          break;
        case 'alternate':
          // If rtl text, then need to switch the direction of the animation.
          var dirSuffix = '';
          if (window.getComputedStyle(titleText).direction === 'rtl') {
            dirSuffix = '-rtl';
          }
          timing += dirSuffix;

          cssClass = marqueeCssClass + '-alt-';
          // Set the width of the marquee to match the text contents length
          width =
              this._headerWrapper.scrollWidth - this._headerWrapper.clientWidth;
          titleText.style.width = width + 'px';

          // Start the marquee animation (aligned left with delay)
          titleText.classList.add(cssClass + timing);
          break;
      }
    } else {
      if (!this._marqueeCssClassList) {
        return;
      }
      // Remove the active marquee CSS classes
      for (var titleCssClass in this._marqueeCssClassList) {
        titleText.classList.remove(titleCssClass);
      }

      titleText.style.transform = '';
    }
  }
};

return Marquee;

});


define('mime_to_class',[],function() {
  /**
   * Given a mime type, generates a CSS class name that uses just the first part
   * of the mime type. So, audio/ogg becomes mime-audio.
   * @param  {String} mimeType
   * @return {String} a class name usable in CSS.
   */
  return function mimeToClass(mimeType) {
    mimeType = mimeType || '';
    return 'mime-' + (mimeType.split('/')[0] || '');
  };
});


define('file_display',['require','l10n!'],function(require) {
  var mozL10n = require('l10n!');
  const kbSize = 1024;

  return {
    /**
     * Display a human-readable file size.
     */
    fileSize: function(node, sizeInBytes) {
      var fileSize;
      var unitName;
      var fileInfo = {};

      if (sizeInBytes >= kbSize * kbSize) {
        fileSize = parseFloat(sizeInBytes / kbSize / kbSize).toFixed(1);
        fileInfo = { megabytes: fileSize };
        unitName = 'attachment-size-meb';
      } else if (sizeInBytes < kbSize) {
        fileInfo = { bytes: sizeInBytes };
        unitName = 'attachment-size-byte';
      } else {
        fileSize = parseFloat(sizeInBytes / kbSize).toFixed(1);
        fileInfo = { kilobytes: fileSize };
        unitName = 'attachment-size-kib';
      }
      mozL10n.setAttributes(node, unitName, fileInfo);
    }
  };
});


define('contacts',['require'],function(require) {

  var filterFns = {
    contains: function(a, b) {
      a = a.toLowerCase();
      b = b.toLowerCase();
      return a.contains(b);
    },
    equality: function(a, b) {
      a = a.toLowerCase();
      b = b.toLowerCase();
      return a === b;
    }
  };

  function isMatch(contact, criteria, filterFn) {
    var found = {};

    outer:
    for (var i = 0, ilen = criteria.terms.length; i < ilen; i++) {
      var term = criteria.terms[i];
      for (var j = 0, jlen = criteria.fields.length; j < jlen; j++) {
        var field = criteria.fields[j];

        if (!contact[field]) {
          continue;
        }

        for (var k = 0, klen = contact[field].length; k < klen; k++) {
          var value = contact[field][k];
          if (typeof value.value !== 'undefined') {
            value = value.value;
          }

          if ((found[term] = filterFn(value.trim(), term))) {
            continue outer;
          }
        }
      }
    }

    return Object.keys(found).every(function(key) {
      return found[key];
    });
  }


  var Contacts = {
    rspaces: /\s+/,

    getCount: function() {
      return window.navigator.mozContacts.getCount();
    },

    /* callback is used to render the list of suggestions */
    findContactByString: function (filterValue, insertNode, callback) {
      var props = ['email', 'givenName', 'familyName'];
      return this.findBy({
        filterBy: props,
        filterOp: 'contains',
        filterValue: filterValue
      }, insertNode, callback);
    },

    findBy: function (filter, insertNode, callback) {
      var lower = [];
      var filterValue = (filter.filterValue || '').trim();
      var terms, request;

      if (!navigator.mozContacts || !filterValue.length) {
        setTimeout(function() {
          callback(
            typeof filter.filterValue === 'undefined' ? null :
                [], {} , insertNode
          );
        });
        return;
      }

      terms = filterValue.split(this.rspaces);

      filter.filterValue = terms.length === 1 ?
        terms[0] :
        terms.reduce(function(initial, term) {
          lower.push(term.toLowerCase());
          return term.length > initial.length ? term : initial;
        }, '');

      if (filter.filterValue.length < 3) {
        filter.filterLimit = 5;
      }

      lower.splice(lower.indexOf(filter.filterValue.toLowerCase()), 1);

      lower.push.apply(lower, terms);

      request = navigator.mozContacts.find(filter);

      request.onsuccess = function onsuccess() {
        var contacts = this.result.slice();
        var fields = ['email', 'givenName', 'familyName'];
        var criteria = { fields: fields, terms: lower };
        var results = [];
        var contact;

        if (terms.length > 1) {
          while ((contact = contacts.pop())) {
            if (isMatch(contact, criteria, filterFns.contains)) {
              results.push(contact);
            }
          }
        } else {
          results = contacts;
        }

        callback(results, {
          terms: terms
        }, insertNode);
      };

      request.onerror = function onerror() {
        this.onsuccess = this.onerror = null;
        callback(null);
      };
    }
  };

  return Contacts;
});

define('template!cards/message_reader.html',['template'], function(template) { return {
createdCallback: template.templateCreatedCallback,
template: template.objToFn({"id":"cards/message_reader.html","deps":[],"text":"<section class=\"msg-reader-header\" role=\"region\" data-statuscolor=\"default\">\n  <header>\n    <h1 class=\"msg-reader-header-label\">\n      <menu class=\"msg-reader-menu\" type=\"toolbar\">\n        <div data-prop=\"previousBtn\" class=\"msg-left-btn\"\n             data-l10n-id=\"message-reader-next-button\">\n          <span data-prop=\"previousIcon\" class=\"icon icon-previous\"></span>\n        </div>\n        <div id=\"msg-reader-all-header\">\n          <div id=\"msg-reader-header-text-item\">\n            <div dir=\"auto\" id=\"msg-reader-header-label\"></div>\n          </div>\n        </div>\n        <div data-prop=\"nextBtn\" class=\"msg-right-btn\"\n             data-l10n-id=\"message-reader-previous-button\">\n          <span data-prop=\"nextIcon\" class=\"icon icon-next\"></span>\n        </div>\n      </menu>\n    </h1>\n    <h2 class=\"msg-reader-header-datetime\">\n      <span dir=\"auto\" id=\"msg-reader-header-date-time\"></span>\n    </h2>\n  </header>\n</section>\n<div data-prop=\"scrollContainer\"\n     class=\"scrollregion-below-header scrollregion-horizontal-too\"\n     role=\"heading\" aria-labelledby=\"message-reader-header\">\n  <div data-prop=\"envelopeBar\" class=\"msg-envelope-bar\">\n    <div class=\"msg-envelope-line msg-envelope-from-line focusable\"\n         role=\"menuitem\">\n      <span class=\"msg-envelope-key\"\n             data-l10n-id=\"envelope-from\"></span>\n    </div>\n    <!-- the details starts out collapsed, but can be toggled -->\n    <div class=\"msg-envelope-details\">\n      <div class=\"msg-envelope-line msg-envelope-to-line focusable\"\n           role=\"menuitem\">\n        <span class=\"msg-envelope-key\"\n               data-l10n-id=\"envelope-to\"></span>\n      </div>\n      <div class=\"msg-envelope-line msg-envelope-cc-line focusable\"\n           role=\"menuitem\">\n        <span class=\"msg-envelope-key\"\n               data-l10n-id=\"envelope-cc\"></span>\n      </div>\n      <div class=\"msg-envelope-line msg-envelope-bcc-line focusable\"\n           role=\"menuitem\">\n        <span class=\"msg-envelope-key\"\n               data-l10n-id=\"envelope-bcc\"></span>\n      </div>\n    </div>\n    <div class=\"msg-envelope-subject-container focusable\" role=\"menuitem\">\n      <div data-prop =\"starBtn\" class =\"msg-star-btn collapsed\"></div>\n      <span class=\"msg-envelope-key\"\n            data-l10n-id=\"msg-display-subject\"></span>\n      <span aria-level=\"2\" class=\"msg-envelope-subject p-pri\"\n          data-l10n-id=\"subject\" dir=\"auto\"></span>\n    </div>\n  </div>\n  <div data-prop=\"attachmentsContainer\"\n        class=\"msg-reader-attachments-container focusable collapsed\"\n        data-event=\"click:goReaderAttachmentsList\" role=\"menuitem\">\n  </div>\n  <!-- Tells us about remote/not downloaded images, asks to show -->\n  <div data-prop=\"loadBar\" class=\"msg-reader-load-infobar focusable collapsed\"\n       role=\"button\">\n    <p data-prop=\"loadBarText\" class=\"msg-reader-load-infobar-text\"></p>\n  </div>\n  <div data-prop=\"rootBodyNode\"\n       class=\"msg-body-container focusable\"\n       data-l10n-id=\"message-body-container\" role=\"menuitem\">\n    <progress data-l10n-id=\"message-body-container-progress\"></progress>\n  </div>\n</div>\n\n<ul class=\"bb-tablist msg-reader-action-toolbar\" role=\"toolbar\">\n  <li role=\"presentation\">\n    <button data-prop=\"deleteBtn\" class=\"icon msg-delete-btn\"\n            data-l10n-id=\"message-delete-button\"></button>\n  </li>\n  <li role=\"presentation\">\n    <button data-prop=\"readBtn\" class=\"icon msg-mark-read-btn\"\n            data-l10n-id=\"message-mark-read-button\"></button>\n  </li>\n  <li role=\"presentation\">\n    <button data-prop=\"moveBtn\" class=\"icon msg-move-btn\"\n            data-l10n-id=\"message-move-button\"></button>\n  </li>\n  <li role=\"presentation\">\n    <button data-prop=\"replyBtn\" class=\"icon msg-reply-btn\"\n            data-l10n-id=\"message-reply-forward-button\"></button>\n  </li>\n</ul>\n"})}; });

/*global MozActivity */

define('cards/message_reader',['require','tmpl!./msg/contact_menu.html','tmpl!./msg/reply_menu.html','tmpl!./msg/browse_confirm.html','tmpl!./msg/peep_bubble.html','tmpl!./msg/attachment_item.html','tmpl!./msg/attachment_disabled_confirm.html','tmpl!./msg/attachment_did_not_open_alert.html','cards','confirm_dialog','date','toaster','model','header_cursor','evt','iframe_shims','marquee','l10n!','query_uri','mime_to_class','file_display','contacts','message_display','./base','template!./message_reader.html'],function(require) {

var MimeMapper,
    msgContactMenuNode = require('tmpl!./msg/contact_menu.html'),
    msgReplyMenuNode = require('tmpl!./msg/reply_menu.html'),
    msgBrowseConfirmNode = require('tmpl!./msg/browse_confirm.html'),
    msgPeepBubbleNode = require('tmpl!./msg/peep_bubble.html'),
    msgAttachmentItemNode = require('tmpl!./msg/attachment_item.html'),
    msgAttachmentDisabledConfirmNode =
                         require('tmpl!./msg/attachment_disabled_confirm.html'),
    msgAttachmentDidNotOpenAlertNode =
      require('tmpl!./msg/attachment_did_not_open_alert.html'),
    cards = require('cards'),
    ConfirmDialog = require('confirm_dialog'),
    date = require('date'),
    toaster = require('toaster'),
    model = require('model'),
    headerCursor = require('header_cursor').cursor,
    evt = require('evt'),
    iframeShims = require('iframe_shims'),
    Marquee = require('marquee'),
    mozL10n = require('l10n!'),
    queryURI = require('query_uri'),
    mimeToClass = require('mime_to_class'),
    fileDisplay = require('file_display'),
    Contacts = require("contacts"),
    messageDisplay = require('message_display');

var CONTENT_TYPES_TO_CLASS_NAMES = [
    null,
    'msg-body-content',
    'msg-body-signature',
    'msg-body-leadin',
    null,
    'msg-body-disclaimer',
    'msg-body-list',
    'msg-body-product',
    'msg-body-ads'
  ];
var CONTENT_QUOTE_CLASS_NAMES = [
    'msg-body-q1',
    'msg-body-q2',
    'msg-body-q3',
    'msg-body-q4',
    'msg-body-q5',
    'msg-body-q6',
    'msg-body-q7',
    'msg-body-q8',
    'msg-body-q9'
  ];
var MAX_QUOTE_CLASS_NAME = 'msg-body-qmax';

var OCTET_STREAM_TYPE = 'application/octet-stream';
var readerDeleteShown = false;
var readerConfirmShown = false;
var readerParams;
var instance;
var bodyRepType = '';
var softkeyType = '';
var stepHeight = document.documentElement.clientHeight / 8;
var iframeScale = null;
var MAX_ATTCHMENT_SIZE = 5 * 1024 * 1024;

// This function exists just to avoid lint errors around
// "do not use 'new' for side effects.
function sendActivity(obj) {
  return new MozActivity(obj);
}

return [
  require('./base')(require('template!./message_reader.html')),
  {
    updateSoftKey: function(type) {
      var params = [];
      if (!instance.header) {
        NavigationMap.setSoftKeyBar(params);
        return;
      }
      if (window.option.menuVisible) {
        return;
      }
      // message reader softkey bar & option menu
      if (iframeScale && iframeScale < 0.8) {
        params.push({
          name: 'Browse Mail',
          l10nId: 'browse-mail',
          priority: 5,
          method: function () {
            // due to after click option menu's item, navigation map need
            // restore focus to message reader page, so before use web api
            // window.open to browse mail, we have to wait here to avoid
            // focus lost in browser window.
            setTimeout(() => {
              instance.onBrowse();
            }, 500);
          }
        });
      }
      if (!(document.querySelector('.msg-reader-attachments-container')
              .classList.contains('collapsed'))) {
        params.push({
          name: 'View Attachment',
          l10nId: 'view-attachment',
          priority: 5,
          method: this.viewReaderAttachment.bind(this)
        });
      }
      if (instance.header.to && instance.header.to.length) {
        params.push({
          name: 'View All Recipients',
          l10nId: 'view-all-recipients',
          priority: 5,
          method: this.viewRecipients.bind(this)
        });
      }
      if (instance.header.cc && instance.header.cc.length) {
        params.push({
          name: 'View All Cc',
          l10nId: 'view-all-cc',
          priority: 5,
          method: this.viewCc.bind(this)
        });
      }
      if (instance.canReply) {
        params.push({
          name: 'Reply to All',
          l10nId: 'opt-reply-all',
          priority: 5,
          method: this.replyAll.bind(this)
        });
        params.push({
          name: 'Forward',
          l10nId: 'opt-forward',
          priority: 5,
          method: this.forward.bind(this)
        });
      }
      params.push({
        name: (!instance.hackMutationHeader.isStarred) ?
                'Add Flag' : 'Remove Flag',
        l10nId: (!instance.hackMutationHeader.isStarred) ?
                'opt-add-flag' : 'opt-remove-flag',
        priority: 5,
        method: this.onToggleStar.bind(this)
      });
      params.push({
        name: 'Move to Folder',
        l10nId: 'opt-move-message',
        priority: 5,
        method: this.onMove.bind(this)
      });
      params.push({
        name: 'View Folders',
        l10nId: 'opt-folders',
        priority: 5,
        method: this.onViewFolders.bind(this)
      });
      params.push({
        name: 'Delete',
        l10nId: 'opt-delete-message',
        priority: 5,
        method: this.onDelete.bind(this)
      });
      readerParams = params;
      this.setSoftkey(type);
    },

    setSoftkey: function(type) {
      var params = [];
      if (this.canReply) {
        params.push({
          name: 'Reply',
          l10nId: 'opt-reply',
          priority: 1,
          method: this.reply.bind(this)
        });
      }
      if (type === 'open') {
        params.push({
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: this.onHyperlinkOpen.bind(this)
        });
      } else if (type === 'select') {
        params.push({
          name: 'Select',
          l10nId: 'select',
          priority: 2
        });
      }
      NavigationMap.setSoftKeyBar(params.concat(readerParams));
    },

    createdCallback: function() {
      // The body elements for the (potentially multiple) iframes we created to
      // hold HTML email content.
      this.htmlBodyNodes = [];

      this._on('msg-reply-btn', 'click', 'onReplyMenu');
      this._on('msg-delete-btn', 'click', 'onDelete');
      this._on('msg-move-btn', 'click', 'onMove');
      this._on('msg-mark-read-btn', 'click', 'onMarkRead');
      this._on('msg-reader-load-infobar', 'click', 'onLoadBarClick');

      this._emittedContentEvents = false;
      this.disableReply();

      // whether or not we've built the body DOM the first time
      this._builtBodyDom = false;

      this.focusableLinks = [];
      this.inViewFocusableLinks = [];
      this.allIframes = null;
      this.bPrevious = false;
      this.bNext = false;
      this.offSetNumber = 0;

      // Bind some methods to this so they can be used as event listeners
      this.handleBodyChange = this.handleBodyChange.bind(this);
      this.onMessageSuidNotFound = this.onMessageSuidNotFound.bind(this);
      this.onCurrentMessage = this.onCurrentMessage.bind(this);

      headerCursor.on('messageSuidNotFound', this.onMessageSuidNotFound);
      headerCursor.latest('currentMessage', this.onCurrentMessage);

      // This should handle the case where we jump right into the reader.
      headerCursor.setCurrentMessage(this.header);
    },

    onArgs: function(args) {
      this.messageSuid = args.messageSuid;
      this.currentFolder = args.curFolder;
      instance = this;
    },

    _contextMenuType: {
      VIEW_CONTACT: 1,
      CREATE_CONTACT: 2,
      ADD_TO_CONTACT: 4,
      REPLY: 8,
      NEW_MESSAGE: 16
    },

    /**
     * Inform Cards to not emit startup content events, this card will trigger
     * them once data from back end has been received and the DOM is up to date
     * with that data.
     * @type {Boolean}
     */
    skipEmitContentEvents: true,

    // Method to help bind event listeners to method names, and ensures
    // a header object before activating the method, to protect the buttons
    // from being activated while the model is still loading.
    _on: function(className, eventName, method, skipProtection) {
      this.getElementsByClassName(className)[0]
      .addEventListener(eventName, function(evt) {
        if (this.header || skipProtection) {
          return this[method](evt);
        }
      }.bind(this), false);
    },

    _setHeader: function(header) {
      this.header = header.makeCopy();
      this.hackMutationHeader = header;

      // - mark message read (if it is not already)
      if (!this.header.isRead) {
        this.header.setRead(true);
      } else {
        this.readBtn.classList.remove('unread');
        mozL10n.setAttributes(this.readBtn, 'message-mark-read-button');
      }

      if(!this.hackMutationHeader.isStarred) {
        this.starBtn.classList.add('collapsed');
      } else {
        this.starBtn.classList.remove('collapsed');
      }
      this.updateSoftKey();
      this.emit('header');
    },

    postInsert: function() {
      this._inDom = true;

      // If have a message that is waiting for the DOM, finish
      // out the display work.
      if (this._afterInDomMessage) {
        this.onCurrentMessage(this._afterInDomMessage);
        this._afterInDomMessage = null;
      }
    },

    told: function(args) {
      if (args.messageSuid) {
        this.messageSuid = args.messageSuid;
      }
    },

    handleBodyChange: function(evt) {
      if (this.className === 'card center') {
        this.buildBodyDom(evt.changeDetails);
      }
    },

    onBack: function(event) {
      var messageHeaderId = null;
      if (this.header) {
        messageHeaderId = this.header.id;
      }
      if (NavigationMap.searchMode) {
        NavigationMap.currentSearchId = messageHeaderId;
      } else {
        NavigationMap.currentMessageId = messageHeaderId;
      }

      if (this.offSetNumber !== 0) {
        if (this.offSetNumber < 0) {
          NavigationMap.scrollUp = 1;
        } else {
          NavigationMap.scrollUp = -1;
        }
        this.offSetNumber = 0;
      } else {
        NavigationMap.scrollUp = 0;
      }

      cards.removeCardAndSuccessors(this, 'delay-animate');
    },

    /**
     * Broadcast that we need to move previous if there's a previous sibling.
     *
     * @param {Event} event previous arrow click event.
     */
    onPrevious: function(event) {
      this.bPrevious = true;
      this.bNext = false;
      headerCursor.advance('previous');
    },

    /**
     * Broadcast that we need to move next if there's a next sibling.
     *
     * @param {Event} event next arrow click event.
     */
    onNext: function(event) {
      this.bNext = true;
      this.bPrevious = false;
      headerCursor.advance('next');
    },

    onMessageSuidNotFound: function(messageSuid) {
      // If no message was found, then go back. This card
      // may have been created from obsolete data, like an
      // old notification for a message that no longer exists.
      // This stops atTop since the most likely case for this
      // entry point is either clicking on a message that is
      // at the top of the inbox in the HTML cache, or from a
      // notification for a new message, which would be near
      // the top.
      if (this.messageSuid === messageSuid) {
        headerCursor.removeListener('messageSuidNotFound',
            this.onMessageSuidNotFound);
        this.onBack();
      }
    },

    /**
     * Set the message we're reading.
     *
     * @param {MessageCursor.CurrentMessage} currentMessage representation of
     * the email we're currently reading.
     */
    onCurrentMessage: function(currentMessage) {
      // If the card is not in the DOM yet, do not proceed, as
      // the iframe work needs to happen once DOM is available.
      if (!this._inDom) {
        this._afterInDomMessage = currentMessage;
        return;
      }

      let mL10nId = 'noInternetConnection';
      if (!window.navigator.onLine) {
        Toaster.showToast({
          messageL10nId: mL10nId,
          latency: 2000
        });
      }

      // Ignore doing extra work if current message is the same as the one
      // already tied to this message reader.
      if (this.header && this.header.id === currentMessage.header.id) {
        return;
      }


      if (this.canReply) {
        this.disableReply();
      }
      // Set our current message.
      this.messageSuid = null;
      this._setHeader(currentMessage.header);
      var clentHeight = document.documentElement.clientHeight;
      this._softkeyHeight =
          document.getElementById('softkeyPanel').clientHeight;
      this._topStuffHeight = clentHeight - this.scrollContainer.clientHeight -
          this._softkeyHeight;

      this.clearDom();

      // Display the header and fetch the body for display.
      this.latestOnce('header', function() {
        // iframes need to be linked into the DOM tree before their
        // contentDocument can be instantiated.
        this.buildHeaderDom(this);

        if (this.header.bytesToDownloadForBodyDisplay > MAX_ATTCHMENT_SIZE) {
          this.rootBodyNode.classList.add('collapsed');
          Toaster.showToast({
            messageL10nId: 'message-large-message-warning',
            latency: 2000
          });
          return;
        }

        this.header.getBody({ downloadBodyReps: true }, function(body) {
          // If the header has changed since the last getBody call, ignore.
          if (this.header.id !== body.id) {
            return;
          }

          this.body = body;

          // always attach the change listener.
          body.onchange = this.handleBodyChange;

          // if the body reps are downloaded show the message immediately.
          if (body.bodyRepsDownloaded) {
            this.buildBodyDom();
          }

          // XXX trigger spinner
          //
        }.bind(this));
      }.bind(this));

      // Previous.
      var hasPrevious = currentMessage.siblings.hasPrevious;
      this.previousBtn.disabled = !hasPrevious;
      this.previousIcon.classList[hasPrevious ? 'remove' : 'add'](
          'icon-disabled');

      // Next.
      var hasNext = currentMessage.siblings.hasNext;
      this.nextBtn.disabled = !hasNext;
      this.nextIcon.classList[hasNext ? 'remove' : 'add']('icon-disabled');

      if (this.bPrevious) {
        this.bPrevious = false;
        this.offSetNumber--;
      }

      if (this.bNext) {
        this.bNext = false;
        this.offSetNumber++;
      }
    },

    reply: function() {
      cards.eatEventsUntilNextCard();
      var composer = this.header.replyToMessage(null, function() {
        cards.pushCard('compose', 'animate', {
          composer: composer,
          type: 'reply',
          title: 'cmp-reply'
        });
      });
    },

    replyAll: function() {
      cards.eatEventsUntilNextCard();
      var composer = this.header.replyToMessage('all', function() {
        cards.pushCard('compose', 'animate', {
          composer: composer,
          type: 'reply',
          title: 'cmp-reply-all'
        });
      });
    },

    viewReaderAttachment: function() {
      console.log('go to viewReaderAttachment');
      cards.eatEventsUntilNextCard();
      cards.pushCard('message_reader_attachments', 'animate', {
        composer: this.body.attachments
      });
    },

    viewRecipients: function() {
      console.log('go to viewRecipients');
      cards.eatEventsUntilNextCard();
      cards.pushCard('message_reader_recipients_cc', 'animate', {
        composer: this,
        type: 'recipients'
      });
    },

    viewCc: function() {
      console.log('go to viewCc');
      cards.eatEventsUntilNextCard();
      cards.pushCard('message_reader_recipients_cc', 'animate', {
        composer: this, type: 'cc'
      });
    },

    forward: function() {
      var needToPrompt = this.header.hasAttachments ||
        this.body.embeddedImageCount > 0;

      var forwardMessage = (function() {
        cards.eatEventsUntilNextCard();
        var composer = this.header.forwardMessage('inline', function() {
          cards.pushCard('compose', 'animate', {
            composer: composer,
            title: 'cmp-forward'
          });
        });
      }.bind(this));

      if (needToPrompt) {
        readerConfirmShown = true;
        var dialogConfig = {
          title: {
            id: 'forward-attention',
            args: {}
          },
          body: {
            id: 'message-send-attachment-disabled-confirm',
            args: {}
          },
          desc: {
            id: '',
            args: {}
          },
          cancel: {
            l10nId: 'message-multiedit-cancel',
            priority: 1,
            callback: function() {
              readerConfirmShown = false;
            }
          },
          accept: {
            l10nId: 'dialog-button-ok',
            priority: 2,
            callback: function() {
              readerConfirmShown = false;
              forwardMessage();
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));
      } else {
        forwardMessage();
      }
    },

    // TODO: canReplyAll should be moved into GELAM.
    /** Returns true if Reply All should be shown as a distinct option. */
    canReplyAll: function() {
      // If any e-mail is listed as 'to' or 'cc' and doesn't match this
      // user's account, 'Reply All' should be enabled.
      var myAddresses = model.account.identities.map(function(ident) {
        return ident.address;
      });

      var otherAddresses = (this.header.to || []).concat(this.header.cc || []);
      if (this.header.replyTo && this.header.replyTo.author) {
        otherAddresses.push(this.header.replyTo.author);
      }
      for (var i = 0; i < otherAddresses.length; i++) {
        var otherAddress = otherAddresses[i];
        if (otherAddress.address &&
            myAddresses.indexOf(otherAddress.address) === -1) {
          return true;
        }
      }

      return false;
    },

    onReplyMenu: function(event) {
      var contents = msgReplyMenuNode.cloneNode(true);
      document.body.appendChild(contents);

      // reply menu selection handling
      var formSubmit = (function(evt) {
        document.body.removeChild(contents);
        switch (evt.explicitOriginalTarget.className) {
        case 'msg-reply-menu-reply':
          this.reply();
          break;
        case 'msg-reply-menu-reply-all':
          this.replyAll();
          break;
        case 'msg-reply-menu-forward':
          this.forward();
          break;
        case 'msg-reply-menu-cancel':
          break;
        }
        return false;
      }).bind(this);
      contents.addEventListener('submit', formSubmit);

      if (!this.canReplyAll()) {
        contents.querySelector('.msg-reply-menu-reply-all')
          .classList.add('collapsed');
      }
    },

    onDelete: function() {
      window.option.hide();
      readerDeleteShown = true;

      var dialogConfig = {
        title: {
          id: 'confirmation-title',
          args: {}
        },
        body: {
          id: 'message-edit-delete-confirm',
          args: {}
        },
        desc: {
          id: '',
          args: {}
        },
        cancel: {
          l10nId: 'message-multiedit-cancel',
          priority: 1,
          callback: function() {
            readerDeleteShown = false;
            window.option.stopListener();
            window.option.startListener();
            window.option.show();
          }
        },
        confirm: {
          l10nId: 'message-edit-menu-delete',
          priority: 3,
          callback: (function() {
            var op = this.header.deleteMessage();
            cards.removeCardAndSuccessors(this, 'animate');
            readerDeleteShown = false;
            window.option.stopListener();
            window.option.startListener();
            window.option.show();
            toaster.toastOperation(op);
          }).bind(this)
        }
      };

      var dialog = new ConfirmDialogHelper(dialogConfig);
      dialog.show(document.getElementById('confirm-dialog-container'));
    },

    onToggleStar: function() {
      this.hackMutationHeader.isStarred = !this.hackMutationHeader.isStarred;
      if(!this.hackMutationHeader.isStarred) {
        this.starBtn.classList.add('collapsed');
      } else {
        this.starBtn.classList.remove('collapsed');
      }
      var op = this.header.setStarred(this.hackMutationHeader.isStarred);
      var toasterText;
      if (op.operation === 'star') {
        toasterText = mozL10n.get('msg-reader-message-flagged');
      } else if (op.operation === 'unstar') {
        toasterText = mozL10n.get('msg-reader-flag-removed');
      }
      toaster.toast({
        text: toasterText
      });
      this.updateSoftKey();
    },

    onMove: function() {
      //TODO: Please verify move functionality after api landed.
      cards.folderSelector(function(folder) {
        var op = this.header.moveMessage(folder);
        cards.removeCardAndSuccessors(this, 'animate');
        toaster.toastOperation(op);
      }.bind(this), function(folder) {
        return folder.isValidMoveTarget;
      }, this.currentFolder);
    },

    onViewFolders: function() {
      cards.pushCard('folder_picker', 'animate', {
        onPushed: function() {},
        previousCard: instance
      });
    },

    onBrowse: function() {
      var iframeNode = instance.rootBodyNode.getElementsByTagName('iframe');
      var iframeHtml = iframeNode[0].contentDocument.children[0].innerHTML;
      iframeHtml = iframeHtml.replace(/overflow: hidden/g, '');
      iframeHtml = iframeHtml.replace(/ext-href=/g, 'href=');

      var win = window.open('', '', 'scrollbars=yes');
      win.document.open('text/html', 'replace');
      win.document.write(iframeHtml);
      win.document.close();
    },

    setRead: function(isRead) {
      this.hackMutationHeader.isRead = isRead;
      this.header.setRead(isRead);

      // Want the button state to reflect the current read state.
      this.readBtn.classList.toggle('unread', !isRead);
      mozL10n.setAttributes(this.readBtn,
        isRead ? 'message-mark-read-button' : 'message-mark-unread-button');
    },

    onMarkRead: function() {
      this.setRead(!this.hackMutationHeader.isRead);
    },

    /**
     * Handle peep bubble click event and trigger context menu.
     */
    onEnvelopeClick: function(event) {
      var target = event.target;
      if (!target.classList.contains('msg-peep-bubble')) {
        return;
      }
      // - peep click
      this.onPeepClick(target);
    },

    onPeepClick: function(target) {
      var contents = msgContactMenuNode.cloneNode(true);
      var peep = target.peep;
      var headerNode = contents.getElementsByTagName('header')[0];
      // Setup the marquee structure
      Marquee.setup(peep.address, headerNode);

      // Activate marquee once the contents DOM are added to document
      document.body.appendChild(contents);
      // XXX Remove 'ease' if linear animation is wanted
      Marquee.activate('alternate', 'ease');

      // -- context menu selection handling
      var formSubmit = (function(evt) {
        document.body.removeChild(contents);
        switch (evt.explicitOriginalTarget.className) {
          // All of these mutations are immediately reflected, easily observed
          // and easily undone, so we don't show them as toaster actions.
          case 'msg-contact-menu-new':
            cards.pushCard('compose', 'animate', {
              composerData: {
                message: this.header,
                onComposer: function(composer) {
                  composer.to = [{
                    address: peep.address,
                    name: peep.name
                  }];
                }
              }
            });
            break;
          case 'msg-contact-menu-view':
            sendActivity({
              name: 'open',
              data: {
                type: 'webcontacts/contact',
                params: {
                  'id': peep.contactId
                }
              }
            });
            break;
          case 'msg-contact-menu-create-contact':
            var params = {
              'email': peep.address
            };

            if (peep.name) {
              params.givenName = peep.name;
            }

            sendActivity({
              name: 'new',
              data: {
                type: 'webcontacts/contact',
                params: params
              }
            });

            // since we already have contact change listeners that are hooked up
            // to the UI, we leave it up to them to update the UI for us.
            break;
          case 'msg-contact-menu-add-to-existing-contact':
            sendActivity({
              name: 'update',
              data: {
                type: 'webcontacts/contact',
                params: {
                  'email': peep.address
                }
              }
            });

            // since we already have contact change listeners that are hooked up
            // to the UI, we leave it up to them to update the UI for us.
            break;
          case 'msg-contact-menu-reply':
            //TODO: We need to enter compose view with specific email address.
            var composer = this.header.replyToMessage(null, function() {
              cards.pushCard('compose', 'animate',
                             { composer: composer });
            });
            break;
        }
        return false;
      }).bind(this);
      contents.addEventListener('submit', formSubmit);

      // -- populate context menu
      var contextMenuOptions = this._contextMenuType.NEW_MESSAGE;
      var messageType = peep.type;

      if (messageType === 'from') {
        contextMenuOptions |= this._contextMenuType.REPLY;
      }

      if (peep.isContact) {
        contextMenuOptions |= this._contextMenuType.VIEW_CONTACT;
      } else {
        contextMenuOptions |= this._contextMenuType.CREATE_CONTACT;
        contextMenuOptions |= this._contextMenuType.ADD_TO_CONTACT;
      }

      if (contextMenuOptions & this._contextMenuType.VIEW_CONTACT) {
        contents.querySelector('.msg-contact-menu-view')
          .classList.remove('collapsed');
      }
      if (contextMenuOptions & this._contextMenuType.CREATE_CONTACT) {
        contents.querySelector('.msg-contact-menu-create-contact')
          .classList.remove('collapsed');
      }
      if (contextMenuOptions & this._contextMenuType.ADD_TO_CONTACT) {
        contents.querySelector('.msg-contact-menu-add-to-existing-contact')
          .classList.remove('collapsed');
      }
      if (contextMenuOptions & this._contextMenuType.REPLY) {
        contents.querySelector('.msg-contact-menu-reply')
          .classList.remove('collapsed');
      }
      if (contextMenuOptions & this._contextMenuType.NEW_MESSAGE) {
        contents.querySelector('.msg-contact-menu-new')
          .classList.remove('collapsed');
      }
    },

    onLoadBarClick: function(event) {
      var self = this;
      var loadBar = this.loadBar;
      if (loadBar.classList.contains('collapsed')) {
        return;
      }
      if (!this.body.embeddedImagesDownloaded) {
        mozL10n.setAttributes(loadBar, 'external-image-downloading');
        this.body.downloadEmbeddedImages(function() {
          // this gets nulled out when we get killed, so use this to bail.
          // XXX of course, this closure will cause us to potentially hold onto
          // a lot of garbage, so it would be better to add an
          // 'onimagesdownloaded' to body so that the closure would end up as
          // part of a cycle that would get collected.
          if (!self.body) {
            return;
          }

          for (var i = 0; i < self.htmlBodyNodes.length; i++) {
            self.body.showEmbeddedImages(self.htmlBodyNodes[i],
                                         self.iframeResizeHandler);
          }
        });
        // XXX really we should check for external images to display that load
        // bar, although it's a bit silly to have both in a single e-mail.
        loadBar.classList.add('collapsed');
      } else {
        for (var i = 0; i < this.htmlBodyNodes.length; i++) {
          this.body.showExternalImages(this.htmlBodyNodes[i],
                                       this.iframeResizeHandler);
        }
        loadBar.classList.add('collapsed');
      }
      loadBar.classList.remove('focusable');
      NavigationMap.navSetup('cards-message-reader',
                             ':not(.collapsed).focusable');
      NavigationMap.setFocus(NavigationMap.getCurrentControl().index);
    },

    getAttachmentBlob: function(attachment, callback) {
      try {
        // Get the file contents as a blob, so we can open the blob
        var storageType = attachment._file[0];
        var filename = attachment._file[1];
        var storage = navigator.getDeviceStorage(storageType);
        var getreq = storage.get(filename);

        getreq.onerror = function() {
          console.warn('Could not open attachment file: ', filename,
                       getreq.error.name);
        };

        getreq.onsuccess = function() {
          // Now that we have the file, return the blob within callback function
          var blob = getreq.result;
          callback(blob);
        };
      } catch (ex) {
        console.warn('Exception getting attachment from device storage:',
                     attachment._file, '\n', ex, '\n', ex.stack);
      }
    },

    onDownloadAttachmentClick: function(node, attachment) {
      node.setAttribute('state', 'downloading');
      // We register all downloads with the download manager.  Previously we had
      // thought about only registering non-media types because the media types
      // were already covered by built-in apps.  But we didn't have a good
      // reason for that; perhaps risk avoidance?  So everybody gets logged!
      // Note that POP3 also does this, but that happens in pop3/sync.js in
      // the back-end and the front-end has no control over that.
      var registerWithDownloadManager = true;
      attachment.download(function downloaded() {
        if (!attachment._file) {
          return;
        }

        node.setAttribute('state', 'downloaded');
      }, null, registerWithDownloadManager);
    },

    onViewAttachmentClick: function(node, attachment) {
      console.log('trying to open', attachment._file, 'known type:',
                  attachment.mimetype);
      if (!attachment._file) {
        return;
      }

      if (attachment.isDownloaded) {
        this.getAttachmentBlob(attachment, function(blob) {
          try {
            // Now that we have the file, use an activity to open it
            if (!blob) {
              throw new Error('Blob does not exist');
            }

            // - Figure out the best MIME type
            //
            // We have three MIME type databases at our disposal:
            //
            // - mimetypes(.js): A reasonably comprehensive database but that's
            //   not continually updated and potentially is out-of-date about
            //   some audio and video types.
            //
            // - nsIMIMEService via DeviceStorage.  Anything that was stored
            //   in DeviceStorage will have its MIME type looked-up using
            //   nsIMIMEService which in turn checks a short hard-coded list
            //   (mainly containing the core web video/audio/doc types),
            //   preferences, the OS, extensions, plugins, category manager
            //   stuff, and then a slightly longer list of hard-coded entries.
            //
            // - shared/js/mime_mapper.js: It knows about all of the media types
            //   supported by Gecko and our media apps.  It at least has
            //   historically been updated pretty promptly.
            //
            //
            // For IMAP and POP3, we get the MIME type from the composer of the
            // message.  They explicitly include a MIME type.  Assuming the
            // author of the message also created the attachment, there's a very
            // high probability they included a valid MIME type (based on a
            // local file extension mapping).  If the author forwarded a message
            // with the attachment maintained, there's also a good chance the
            // MIME type was maintained.  If the author downloaded the
            // attachment but lacked a mapping for the file extension, the
            // reported MIME type is probably application/octet-stream.  As
            // of writing this, our IMAP and POP3 implementations do not
            // second-guess the reported MIME type if it is
            // application/octet-stream.
            //
            // In the ActiveSync case, we are not given any MIME type
            // information and our ActiveSync library uses mimetypes.js to
            // map the extension type.  This may result in
            // application/octet-stream being returned.
            //
            // Given all of this, our process for determining MIME types is to:
            // - Trust the MIME type we have on file for the message if it's
            //   anything other than application/octet-stream.
            var useType = attachment.mimetype;
            // - If it was octet-stream (or somehow missing), we check if
            //   DeviceStorage has an opinion.  We use it if so.
            if (!useType || useType === OCTET_STREAM_TYPE) {
              useType = blob.type;
            }
            // - If we still think it's octet-stream (or falsey), we ask the
            //   MimeMapper to map the file extension to a MIME type.
            if (!useType || useType === OCTET_STREAM_TYPE) {
              useType = MimeMapper.guessTypeFromFileProperties(
                          attachment.filename, OCTET_STREAM_TYPE);
            }
            // - If it's falsey (MimeMapper returns an emptry string if it
            //   can't map), we set the value to application/octet-stream.
            if (!useType) {
              useType = OCTET_STREAM_TYPE;
            }
            // - At this point, we're fine with application/octet-stream.
            //   Although there are some file-types where we can just chuck
            //   "application/" on the front, there aren't enough of them.
            //   Apps can, however, use a regexp filter on the filename we
            //   provide to capture extension types that way.
            console.log('triggering open activity with MIME type:', useType);

            var activity = new MozActivity({
              name: 'open',
              data: {
                type: useType,
                filename: attachment.filename,
                blob: blob,
                // the PDF viewer really wants a "url".  download_helper.js
                // provides the local filesystem path which is sketchy and
                // non-sensical.  We just provide the filename again.
                url: attachment.filename
              }
            });
            activity.onerror = function() {
              console.warn('Problem with "open" activity', activity.error.name);
              // NO_PROVIDER is returned if there's nothing to service the
              // activity.
              if (activity.error.name === 'NO_PROVIDER') {
                var dialog = msgAttachmentDidNotOpenAlertNode.cloneNode(true);
                ConfirmDialog.show(dialog,
                  {
                    id: 'msg-attachment-did-not-open-ok',
                    handler: null
                  },
                  {
                    handler: null
                  }
                );
              }
            };
            activity.onsuccess = function() {
              console.log('"open" activity allegedly succeeded');
            };
          }
          catch (ex) {
            console.warn('Problem creating "open" activity:', ex, '\n',
                         ex.stack);
          }
        });
      }
    },

    onHyperlinkClick: function(event, linkNode, linkUrl, linkText) {
      var dialog = msgBrowseConfirmNode.cloneNode(true);
      var content = dialog.getElementsByTagName('p')[0];
      mozL10n.setAttributes(content, 'browse-to-url-prompt', { url: linkUrl });
      ConfirmDialog.show(dialog,
        { // Confirm
          id: 'msg-browse-ok',
          handler: function() {
            if (/^mailto:/i.test(linkUrl)) {
              // Fast path to compose. Works better than an activity, since
              // "canceling" the activity has freaky consequences: what does it
              // mean to cancel ourselves? What is the sound of one hand
              // clapping?
              var data = queryURI(linkUrl);
              cards.pushCard('compose', 'animate', {
                composerData: {
                  onComposer: function(composer, composeCard) {
                    // Copy the to, cc, bcc, subject, body to the compose.
                    // It is OK to do this blind key copy since queryURI
                    // explicitly only populates expected fields, does not
                    // blindly accept input from the outside, and the queryURI
                    // properties match the property names allowed on composer.
                    Object.keys(data).forEach(function(key) {
                      composer[key] = data[key];
                    });
                  }
                }
              });
            } else {
              // Pop out to what is likely the browser, or the user's preferred
              // viewer for the URL. This keeps the URL out of our cookie
              // jar/data space too.
              sendActivity({
                name: 'view',
                data: {
                  type: 'url',
                  url: linkUrl
                }
              });
            }
          }.bind(this)
        },
        { // Cancel
          id: 'msg-browse-cancel',
          handler: null
        }
      );
    },

    onHyperlinkOpen: function() {
      for (var i = 0; i < instance.focusableLinks.length; i++) {
        if (instance.focusableLinks[i].classList.contains('focus') &&
            instance.focusableLinks[i].hasAttribute('ext-href')) {
          instance.externalLinkURL =
              instance.focusableLinks[i].getAttribute('ext-href');
        }
      }
      if (instance.externalLinkURL) {
        if (/^callto:/i.test(instance.externalLinkURL)) {
          var callTag = 'callto:';
          var telNumber = instance.externalLinkURL.substring(callTag.length);
          if (telNumber) {
            sendActivity({
              name: 'dial',
              data: {
                type: 'webtelephony/number',
                number: telNumber
              }
            });
          }
        } else if (/^mailto:/i.test(instance.externalLinkURL)) {
          // Fast path to compose. Works better than an activity, since
          // "canceling" the activity has freaky consequences: what does it
          // mean to cancel ourselves? What is the sound of one hand
          // clapping?
          var data = queryURI(instance.externalLinkURL);
          cards.pushCard('compose', 'animate', {
            composerData: {
              onComposer: function(composer, composeCard) {
                // Copy the to, cc, bcc, subject, body to the compose.
                // It is OK to do this blind key copy since queryURI
                // explicitly only populates expected fields, does not
                // blindly accept input from the outside, and the queryURI
                // properties match the property names allowed on composer.
                 Object.keys(data).forEach(function(key) {
                   composer[key] = data[key];
                 });
               }
             }
           });
         } else {
           // Pop out to what is likely the browser, or the user's preferred
           // viewer for the URL. This keeps the URL out of our cookie
           // jar/data space too.
           sendActivity({
             name: 'view',
             data: {
               type: 'url',
               url: instance.externalLinkURL
             }
           });
         }
      } else {
        console.log('there is no external link url');
      }
    },

    _populatePlaintextBodyNode: function(bodyNode, rep) {
      for (var i = 0; i < rep.length; i += 2) {
        var node = document.createElement('div'), cname;

        var etype = rep[i] & 0xf;
        if (etype === 0x4) {
          var qdepth = (((rep[i] >> 8) & 0xff) + 1);
          if (qdepth > 8) {
            cname = MAX_QUOTE_CLASS_NAME;
          } else {
            cname = CONTENT_QUOTE_CLASS_NAMES[qdepth];
          }
        }
        else {
          cname = CONTENT_TYPES_TO_CLASS_NAMES[etype];
        }
        if (cname) {
          node.setAttribute('class', cname);
          node.classList.add('p');
          node.classList.add('ul');
        }

        var subnodes = model.api.utils.linkifyPlain(rep[i + 1], document);
        for (var iNode = 0; iNode < subnodes.length; iNode++) {
          node.appendChild(subnodes[iNode]);
        }

        bodyNode.appendChild(node);
      }
      var externalLink = bodyNode.querySelectorAll('.moz-external-link');
      for (var i = 0; i < externalLink.length; i++) {
        externalLink[i].classList.add('focusable');
      }
      NavigationMap.navSetup('cards-message-reader',
                             ':not(.collapsed).focusable');
    },

    buildHeaderDom: function(domNode) {
      var header = this.header;

      // -- Header
      function updatePeep(peep) {
        var nameNode = peep.element.querySelector('.msg-peep-content');

        if (peep.type === 'from') {
          // We display the sender of the message's name in the header and the
          // address in the bubble.
          nameNode.textContent = peep.address;
          nameNode.classList.add('msg-peep-address');
        } else {
          nameNode.textContent = peep.name || peep.address;
          if (!peep.name && peep.address) {
            nameNode.classList.add('msg-peep-address');
          } else {
            nameNode.classList.remove('msg-peep-address');
          }
        }
      }

      function addHeaderEmails(type, peeps) {
        var lineClass = 'msg-envelope-' + type + '-line';
        var lineNode = domNode.getElementsByClassName(lineClass)[0];

        if (!peeps || !peeps.length) {
          lineNode.classList.add('collapsed');
          return;
        }

        // Make sure it is not hidden from a next/prev action.
        lineNode.classList.remove('collapsed');

        // Because we can avoid having to do multiple selector lookups, we just
        // mutate the template in-place...
        var peepTemplate = msgPeepBubbleNode;

        for (var i = 0; i < peeps.length; i++) {
          var peep = peeps[i];
          peep.type = type;
          peep.element = peepTemplate.cloneNode(true);
          peep.element.peep = peep;
          peep.onchange = updatePeep;
          updatePeep(peep);
          lineNode.appendChild(peep.element);
        }
      }

      addHeaderEmails('from', [header.author]);
      addHeaderEmails('to', header.to);
      addHeaderEmails('cc', header.cc);
      addHeaderEmails('bcc', header.bcc);

      domNode.querySelector('#msg-reader-header-label').textContent =
          header.author.name || header.author.address;
      domNode.querySelector('#msg-reader-header-date-time').textContent =
          date. showDateTime(header.date);

      messageDisplay.subject(domNode.querySelector('.msg-envelope-subject'),
                             header);
    },

    clearDom: function() {
      // Clear header emails.
      Array.slice(this.querySelectorAll('.msg-peep-bubble')).forEach(
        function(node) {
          node.parentNode.removeChild(node);
        }
      );

      // Nuke rendered attachments.
      var attachmentsContainer =
        this.querySelector('.msg-reader-attachments-container');
      attachmentsContainer.innerHTML = '';
      attachmentsContainer.classList.add('collapsed');

      // Nuke existing body, show progress while waiting
      // for message to load.
      this.rootBodyNode.innerHTML =
        '<progress data-l10n-id="message-body-container-progress"></progress>';

      // Make sure load bar is not shown between loads too.
      this.loadBar.classList.add('collapsed');

      this.focusableLinks = [];
    },

    /**
     * Render the DOM nodes for bodyReps and the attachments container.
     * If we have information on which parts of the message changed,
     * only update those DOM nodes; otherwise, update the whole thing.
     *
     * @param {object} changeDetails
     * @param {array} changeDetails.bodyReps An array of changed item indexes.
     * @param {array} changeDetails.attachments An array of changed item
     * indexes.
     */
    buildBodyDom: function(/* optional */ changeDetails) {
      var body = this.body;

      // If the card has been destroyed (so no more body, as it is nulled in
      // die()) or header has changed since this method was scheduled to be
      // called (rapid taps of the next/previous buttons), ingore the call.
      if (!body || this.header.id !== body.id) {
        return;
      }

      var domNode = this,
          rootBodyNode = this.rootBodyNode,
          reps = body.bodyReps,
          hasExternalImages = false,
          externalImageCount = 0,
          messageChange = true,
          showEmbeddedImages = body.embeddedImageCount &&
                               body.embeddedImagesDownloaded;


      instance.currentLinkIndex = null;
      // The first time we build the body DOM, do one-time bootstrapping:
      if (!this._builtBodyDom) {
        iframeShims.bindSanitizedClickHandler(rootBodyNode,
                                              null,
                                              rootBodyNode,
                                              null);
        this._builtBodyDom = true;
        messageChange = false;
      }

      // If we have fully downloaded one body part, the user has
      // something to read so get rid of the spinner.
      // XXX: Potentially improve the UI to show if we're still
      // downloading the rest of the body even if we already have some
      // of it.
      if (reps.length && reps[0].isDownloaded) {
        // remove progress bar if we've retrieved the first rep
        var progressNode = rootBodyNode.querySelector('progress');
        if (progressNode) {
          progressNode.parentNode.removeChild(progressNode);
        }
      }

      // The logic below depends on having removed the progress node!

      for (var iRep = 0; iRep < reps.length; iRep++) {
        var rep = reps[iRep];

        // If the rep's type is fake, we should skip handle it.
        if (rep.type === 'fake') {
          continue;
        }

        // Create an element to hold this body rep. Even if we aren't
        // updating this rep right now, we need to have a placeholder.
        var repNode = rootBodyNode.childNodes[iRep];
        if (!repNode) {
          repNode = rootBodyNode.appendChild(document.createElement('div'));
        }

        // Skip updating this rep if it's not updated.
        if (changeDetails && changeDetails.bodyReps &&
            changeDetails.bodyReps.indexOf(iRep) === -1) {
          continue;
        }

        // Wipe out the existing contents of the rep node so we can
        // replace it. We can just nuke innerHTML since we add click
        // handlers on the rootBodyNode, and for text/html parts the
        // listener is a child of repNode so it will get destroyed too.
        repNode.innerHTML = '';

        var msgBodyContainer =
            document.getElementsByClassName('msg-body-container');
        if (rep.type === 'plain') {
          this._populatePlaintextBodyNode(repNode, rep.content);
          bodyRepType = 'plain';
          iframeScale = null;
          instance.allIframes = null;
          var arrayLink = msgBodyContainer[0]
                          .querySelectorAll('.moz-external-link');
          for (var i = 0; i < arrayLink.length; i++) {
            instance.focusableLinks.push(arrayLink[i]);
          }
        } else if (rep.type === 'html') {
          var iframeShim = iframeShims.createAndInsertIframeForContent(
            rep.content, this.scrollContainer, repNode, null,
            'interactive', null);
          var iframe = iframeShim.iframe;
          iframeScale = iframeShim.scale;
          var bodyNode = iframe.contentDocument.body;
          bodyRepType = 'html';
          this.iframeResizeHandler = iframeShim.resizeHandler;
          model.api.utils.linkifyHTML(iframe.contentDocument);
          this.htmlBodyNodes.push(bodyNode);

          instance.allIframes = msgBodyContainer[0].querySelectorAll('iframe');
          for (var i = 0; i < instance.allIframes.length; i++) {
            var arrayLink = instance.allIframes[i].contentDocument.body
                .querySelectorAll('.moz-external-link');
            for (var j = 0; j < arrayLink.length; j++) {
              instance.focusableLinks.push(arrayLink[j]);
            }
          }
          // BUG471-lijuanli@t2mobile.com-for image string display-begin
          externalImageCount = body.checkForExternalImages(bodyNode);
          dump('externalImageCount = ' + externalImageCount);
          if (externalImageCount > 0) {
            hasExternalImages = true;
          }
          // BUG471-lijuanli@t2mobile.com-for image string display-end
          if (showEmbeddedImages) {
            body.showEmbeddedImages(bodyNode, this.iframeResizeHandler);
          }
        }

        var handleRootBodyNodeFocus = function() {
          if (instance.className !== 'card center') {
            return;
          }

          if (instance.focusableLinks.length > 0) {
            instance.updateCurrentViewFocusLinks();
            var len = instance.inViewFocusableLinks.length;
            if (len > 0) {
              var index = instance.currentLinkIndex;
              if (index) {
                instance.setFocusRestoreElement(index);
              } else {
                instance.setFocusElement(0);
              }
            }
          }
        };

        var handRootBodyNodeKeydown = function(evt) {
          switch (evt.key) {
            // due to our Navigation engine cannot match the UI design of
            // message reader page, so we have to handle the page scroll
            // and focus set when press ArrowDown and ArrowUp in message
            // body node.
            case 'ArrowDown':
              var beforeScrollLength = instance.inViewFocusableLinks.length;
              var afterScrollLength;
              var beforeScrollEl;
              // we need distinguish the situation with the focusable links
              // length.
              //
              // 1. the focusable links length is 0.
              // if no inview focusable links, we should scroll the page
              // immediately.
              if (beforeScrollLength === 0) {
                instance.scrollByStep(instance.scrollContainer, 'down');
                afterScrollLength = instance.inViewFocusableLinks.length;
                // and if has focusable links after scroll, we should set
                // focus on the first one.
                if (afterScrollLength > 0) {
                  instance.setFocusElement(0);
                }
              // 2. the focusable links length is 1.
              // if has one inview focusable link, we should scroll the
              // page immediately.
              } else if (beforeScrollLength === 1) {
                beforeScrollEl = instance.inViewFocusableLinks[0];
                instance.scrollByStep(instance.scrollContainer, 'down');
                afterScrollLength = instance.inViewFocusableLinks.length;
                // and if has no focusable link after scroll, we must remove all
                // focus.
                if (afterScrollLength === 0) {
                  instance.removeAllFocus();
                  instance.updateSoftKey();
                // and if has focusbale links after scroll, we must set the
                // focus to the next one.
                } else if (afterScrollLength > 0) {
                  var index =
                      instance.inViewFocusableLinks.indexOf(beforeScrollEl);
                  if (index === -1) {
                    instance.setFocusElement(0);
                  } else if (index === 0 && afterScrollLength > 1) {
                    instance.setFocusElement(1);
                  }
                }
              // 3. the focusable links length > 1
              } else if (beforeScrollLength > 1) {
                var focusedElIndex;
                // we need to know the focused link's index
                for (var i = 0; i < instance.inViewFocusableLinks.length; i++) {
                  var inViewFocusableLink = instance.inViewFocusableLinks[i];
                  if (inViewFocusableLink.classList.contains('focus')) {
                    focusedElIndex = i;
                    beforeScrollEl = inViewFocusableLink;
                  }
                }

                // if focused link is not the last one, we only need set the
                // focus to the next one.
                if (focusedElIndex < (beforeScrollLength - 1)) {
                  instance.setFocusElement(focusedElIndex + 1);
                  // and if focused link is the last one, we need to scroll the
                  // page firstly, then check the focusable links' length after
                  // scroll to determine which link should be focused.
                } else if (focusedElIndex === (beforeScrollLength - 1)) {
                  instance.scrollByStep(instance.scrollContainer, 'down');
                  afterScrollLength = instance.inViewFocusableLinks.length;
                  if (afterScrollLength === 0) {
                    instance.removeAllFocus();
                  } else if (afterScrollLength > 0) {
                    var index =
                        instance.inViewFocusableLinks.indexOf(beforeScrollEl);
                    if (index === -1) {
                      instance.setFocusElement(0);
                    } else if (index > -1 && index < (afterScrollLength - 1)) {
                      instance.setFocusElement(index + 1);
                    }
                  }
                }
              }
              evt.preventDefault();
              evt.stopPropagation();
              break;
            case 'ArrowUp':
              // we need to check if we should move the focus out of message
              // body node or not.
              if (instance.isVisible(instance.rootBodyNode, false)
                  && (instance.inViewFocusableLinks.length === 0
                  || (instance.inViewFocusableLinks.length > 0
                  && instance.focusableLinks[0].classList.contains('focus')))) {
                instance.removeAllFocus();
                return;
              }
              var beforeScrollLength = instance.inViewFocusableLinks.length;
              var afterScrollLength;
              var beforeScrollEl;
              // we need distinguish the situation with the focusable links
              // length.
              //
              // 1. the focusable links length is 0.
              // if no inview focusable links, we should scroll the page
              // immediately.
              if (beforeScrollLength === 0) {
                instance.scrollByStep(instance.scrollContainer, 'up');
                afterScrollLength = instance.inViewFocusableLinks.length;
                // and if has focusable links after scroll, should set focus
                // on the last one.
                if (afterScrollLength > 0) {
                  instance.setFocusElement(afterScrollLength - 1);
                }
              // 2. the focusable links length is 1.
              // if has one inview focusable link, we should scroll the page
              // immediately.
              } else if (beforeScrollLength === 1) {
                beforeScrollEl = instance.inViewFocusableLinks[0];
                instance.scrollByStep(instance.scrollContainer, 'up');
                afterScrollLength = instance.inViewFocusableLinks.length;
                // and if has no focusable link after scroll, we must remove
                // all focus.
                if (afterScrollLength === 0) {
                  instance.removeAllFocus();
                  instance.updateSoftKey();
                  // and if has focusbale links after scroll, we must set the
                  // focus to the previous one.
                } else if (afterScrollLength > 0) {
                  var index =
                      instance.inViewFocusableLinks.indexOf(beforeScrollEl);
                  if (index === -1) {
                    instance.setFocusElement(afterScrollLength - 1);
                  } else if (index === (afterScrollLength - 1)
                      && afterScrollLength > 1) {
                    instance.setFocusElement(index - 1);
                  }
                }
              // 3. the focusable links length > 1
              } else if (beforeScrollLength > 1) {
                var focusedElIndex;
                // we need to know the focused link's index
                for (var i = 0; i < instance.inViewFocusableLinks.length; i++) {
                  var inViewFocusableLink = instance.inViewFocusableLinks[i];
                  if (inViewFocusableLink.classList.contains('focus')) {
                    focusedElIndex = i;
                    beforeScrollEl = inViewFocusableLink;
                  }
                }

                // if focused link is not the first one, we only need set the
                // focus to the previous one.
                if (focusedElIndex > 0) {
                  instance.setFocusElement(focusedElIndex - 1);
                  // and if focused link is the first one, we need to scroll
                  // the page firstly, then check the focusable links' length
                  // after scroll to determine which link should be focused.
                } else if (focusedElIndex === 0) {
                  instance.scrollByStep(instance.scrollContainer, 'up');
                  afterScrollLength = instance.inViewFocusableLinks.length;
                  if (afterScrollLength === 0) {
                    instance.removeAllFocus();
                  } else if (afterScrollLength > 0) {
                    var index =
                        instance.inViewFocusableLinks.indexOf(beforeScrollEl);
                    if (index === -1) {
                      instance.setFocusElement(afterScrollLength - 1);
                    } else if (index > 0) {
                      instance.setFocusElement(index - 1);
                    }
                  }
                }
              }
              evt.preventDefault();
              evt.stopPropagation();
              break;
          }
        };

        if (messageChange) {
          instance.onCardVisible('forward');
        } else {
          rootBodyNode.addEventListener('focus', handleRootBodyNodeFocus);
          rootBodyNode.addEventListener('keydown', handRootBodyNodeKeydown);
        }
      }

      // The image logic checks embedded image counts, so this should be
      // able to run every time:
      // -- HTML-referenced Images
      var loadBar = this.loadBar;
      if (body.embeddedImageCount && !body.embeddedImagesDownloaded) {
        loadBar.classList.remove('collapsed');
        loadBar.classList.add('focusable');
        NavigationMap.navSetup('cards-message-reader',
                               ':not(.collapsed).focusable');
        var size = (body.allEmbeddedImagesSize / (1024 * 1024)).toFixed(2);
        // BUG471-lijuanli@t2mobile.com-for image string display-begin
        dump('body.embeddedImageCount = ' + body.embeddedImageCount);
        if (body.embeddedImageCount == 1) {
            mozL10n.setAttributes(this.loadBarText,
                'msg-reader-download-external-image',
                {n: size});
        } else {
          mozL10n.setAttributes(this.loadBarText,
              'msg-reader-download-external-images',
              {n: size});
        }
        // BUG471-lijuanli@t2mobile.com-for image string display-end
      } else if (hasExternalImages) {
        loadBar.classList.remove('collapsed');
        loadBar.classList.add('focusable');
        NavigationMap.navSetup('cards-message-reader',
                               ':not(.collapsed).focusable');
        // BUG471-lijuanli@t2mobile.com-for image string display-begin
        if (externalImageCount == 1) {
            mozL10n.setAttributes(this.loadBarText, 'message-show-external-image');
        } else {
            mozL10n.setAttributes(this.loadBarText, 'message-show-external-images');
        }
        // BUG471-lijuanli@t2mobile.com-for image string display-end
      } else {
        loadBar.classList.add('collapsed');
        if (loadBar.classList.contains('focusable')) {
          loadBar.classList.remove('focusable');
        }
      }

      // -- Attachments (footer)
      // An attachment can be in 1 of 3 possible states for UI purposes:
      // - Not downloadable: We can't download this message because we wouldn't
      //   be able to do anything with it if we downloaded it.  Anything that's
      //   not a supported image type falls in this category.
      // - Downloadable, not downloaded: The user can trigger download of the
      //   attachment to DeviceStorage.
      // - Downloadable, downloaded: The attachment is already fully downloaded
      //   to DeviceStorage and we can trigger its display.
      var attachmentsContainer =
          domNode.querySelector('.msg-reader-attachments-container');
      if (body.attachments && body.attachments.length) {
        // If buildBodyDom is called multiple times, the attachment
        // state might change, so we must ensure the attachment list is
        // not collapsed if we now have attachments.
        console.log('attachmentsContainer.childNodes.length is ' +
                    attachmentsContainer.childNodes.length);
        if (attachmentsContainer.childNodes.length > 0) {
          console.log('no longer to update attachments in msg reader as it' +
                      ' had been updated');
          return;
        }
        attachmentsContainer.classList.remove('collapsed');

        // create element under attachmentsContainer as attachmentsContainer
        // will be cleared in clearDom
        var divElement = document.createElement('div');
        divElement.classList.add('msg-reader-attachment-info');
        attachmentsContainer.appendChild(divElement);
        var spanElementTitle = document.createElement('span');
        var spanElementName = document.createElement('span');
        var spanElementCount = document.createElement('span');

        spanElementTitle.classList.add('msg-envelope-key');
        if (navigator.largeTextEnabled) {
          spanElementTitle.classList.add('p-pri');
        }
        if (body.attachments.length === 1) {
          spanElementTitle.setAttribute('data-l10n-id',
              'msg-reader-display-attachment');
        } else if (body.attachments.length > 1) {
          spanElementTitle.setAttribute('data-l10n-id',
              'msg-reader-display-attachments');
        }
        spanElementName.classList.add('msg-reader-attachments-filename');
        spanElementName.classList.add('p-pri');
        spanElementCount.classList.add('msg-reader-attachments-count');
        spanElementCount.classList.add('p-pri');

        divElement.appendChild(spanElementTitle);
        divElement.appendChild(spanElementName);
        divElement.appendChild(spanElementCount);

        attachmentsContainer.classList.add('focusable');
        NavigationMap.navSetup('cards-message-reader',
            ':not(.collapsed).focusable');
        // We need MimeMapper to help us determining the downloadable
        // attachments but it might not be loaded yet, so load before use it.
        require(['shared/js/mime_mapper'], function(mapper) {
          if (!MimeMapper) {
            MimeMapper = mapper;
          }

          var messageReaderAttachmentFileName =
              domNode.querySelector('.msg-reader-attachments-filename');
          messageReaderAttachmentFileName.innerHTML = '';
          for (var iAttach = 0; iAttach < body.attachments.length; iAttach++) {
            // Skip updating this attachment if it's not updated.
            if (changeDetails && changeDetails.attachments &&
                changeDetails.attachments.indexOf(iAttach) === -1) {
              continue;
            }

            var attachment = body.attachments[iAttach];
            messageReaderAttachmentFileName.innerHTML += attachment.filename + ' ';
          }
          domNode.querySelector('.msg-reader-attachments-count').innerHTML =
              ' (' + body.attachments.length + ')';
          instance.enableReply();
        }.bind(this));
      }
      else {
        attachmentsContainer.classList.add('collapsed');
        if (attachmentsContainer.classList.contains('focusable')) {
          attachmentsContainer.classList.remove('focusable');
        }
        instance.enableReply();
      }
    },

    disableReply: function() {
      this.canReply = false;
    },

    enableReply: function() {
      this.canReply = true;
      this.updateSoftKey();
      // Inform that content is ready. Done here because reply is only enabled
      // once the full body is available.
      if (!this._emittedContentEvents) {
        evt.emit('metrics:contentDone');
        this._emittedContentEvents = true;
      }
    },

    /**
     * Called by Cards when the instance of this card type is the
     * visible card.
     */
    onCardVisible: function(navDirection) {
      console.log(this.localName + '.onCardVisible, navDirection=' +
                  navDirection);
      const CARD_NAME = this.localName;
      const QUERY_CHILD = ':not(.collapsed).focusable';
      const CONTROL_ID = CARD_NAME + ' ' + QUERY_CHILD;

      // forward: new card pushed
      if (navDirection === 'forward') {
        NavigationMap.navSetup(CARD_NAME, QUERY_CHILD);
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('first');
        NavigationMap.observeChild(CARD_NAME, QUERY_CHILD);
      }
      // back: hidden card is restored
      else if (navDirection === 'back') {
        NavigationMap.setCurrentControl(CONTROL_ID);
        NavigationMap.setFocus('restore');
      }

      window.addEventListener('keydown', this.handleKeyEvent);
      this.updateCurrentViewFocusLinks();
      this.updateSoftKey(softkeyType);
    },

    onHidden: function() {
      console.log(this.localName + ' onHidden');
      window.removeEventListener('keydown', this.handleKeyEvent);
    },

    onFocusChanged: function(queryChild, index, item) {
      if (this.className !== 'card center') {
        return;
      }

      let focused = document.querySelector('.focus');
      if (!focused.classList.contains('msg-body-container')) {
        if (item.classList.contains('msg-reader-attachments-container') ||
            item.classList.contains('msg-reader-load-infobar')) {
          this.updateSoftKey('select');
          softkeyType = 'select';
        } else {
          this.updateSoftKey();
          softkeyType = '';
        }
      }
    },

    goReaderAttachmentsList: function() {
      console.log("go to reader attachments list");
      instance.viewReaderAttachment();
    },

    handleKeyEvent: function(event) {
      console.log('message_reader ---> handleKeyEvent(keydown: ' +
                  event.key + ')');
      if (window.option.menuVisible) {
        return;
      }
      switch (event.key) {
        case 'ArrowLeft':
          instance.onPrevious();
          break;
        case 'ArrowRight':
          instance.onNext();
          break;
        case 'BrowserBack':
        case 'Backspace':
          if (readerDeleteShown) {
            event.preventDefault();
            CustomDialog.hide();
            option.show();
            readerDeleteShown = false;
            return;
          }
          if (readerConfirmShown) {
            event.preventDefault();
            readerConfirmShown = false;
            return;
          }
          if (!window.option.menuVisible) {
            event.preventDefault();
            instance.onBack();
          }
          break;
      }
    },

    setFocusElement: function(index) {
      instance.removeAllFocus();
      if (instance.inViewFocusableLinks.length > 0) {
        var toFocused = instance.inViewFocusableLinks[index];
        toFocused.classList.add('focus');
        toFocused.focus();

        instance.updateSoftKey('open');
        softkeyType = 'open';

        // update currentLinkIndex
        for (var i = 0; i < instance.focusableLinks.length; i++) {
          if (instance.focusableLinks[i] === toFocused) {
            instance.currentLinkIndex = i;
            break;
          }
        }
      }
    },

    setFocusRestoreElement: function(index) {
      var toFocused = instance.focusableLinks[index];
      toFocused.classList.add('focus');
      toFocused.focus();
      if (!this.isVisible(toFocused, true)) {
        toFocused.scrollIntoView();
      }
    },

    scrollByStep: function(el, dir) {
      var sHeight = el.scrollHeight;
      var cHeight = el.clientHeight;
      var moveHeight = sHeight - cHeight;
      var sTop = el.scrollTop;
      if (dir === 'down') {
        if (sTop < moveHeight) {
          if (sTop + stepHeight >= moveHeight) {
             el.scrollTop = moveHeight;
          } else {
             el.scrollTop = sTop + stepHeight;
          }
        }
      } else if (dir === 'up') {
        if (sTop - stepHeight >= 0) {
          el.scrollTop = sTop - stepHeight;
        } else {
          el.scrollTop = 0;
        }
      }

      if (this.focusableLinks.length > 0) {
        this.updateCurrentViewFocusLinks();
      }
    },

    updateCurrentViewFocusLinks: function() {
      this.inViewFocusableLinks = [];

      for (var i = 0; i < this.focusableLinks.length; i++) {
        if (this.isVisible(this.focusableLinks[i], true)) {
          this.inViewFocusableLinks.push(this.focusableLinks[i]);
        }
      }
    },

    isVisible: function(el, isLinks) {
      if (el.offsetWidth === 0 || el.offsetHeight === 0) {
        return false;
      }
      var height = document.documentElement.clientHeight -
              this._softkeyHeight,
          rects = el.getClientRects(),
          msgBodyRects = this.rootBodyNode.getClientRects()[0],
          topTarget = this._topStuffHeight;

      if (!iframeScale && isLinks) {
        iframeScale = 1;
      }

      for (var i = 0, l = rects.length; i < l; i++) {
        var r = rects[i];
        var originalTop = (bodyRepType === 'html' ? msgBodyRects.top : 0);
        var compareBottom = isLinks ?
            r.bottom * iframeScale + originalTop : r.bottom;
        var compareTop = isLinks ?
            r.top * iframeScale + originalTop : r.top;
        if ((el.classList.contains('msg-body-container')
             && compareTop < height && compareTop >= topTarget)
             || (!el.classList.contains('msg-body-container')
                  && (compareBottom > 0 && compareBottom <= height)
                  && (compareTop >= topTarget))) {
          return true;
        }
      }
      return false;
    },

    removeAllFocus: function() {
      for (var i = 0; i < this.focusableLinks.length; i++) {
        if (this.focusableLinks[i].classList.contains('focus')) {
          this.focusableLinks[i].classList.remove('focus');
        }
      }
    },

    die: function() {
      headerCursor.removeListener('messageSuidNotFound',
                                  this.onMessageSuidNotFound);
      headerCursor.removeListener('currentMessage', this.onCurrentMessage);
      window.removeEventListener('keydown', this.handleKeyEvent);

      // Our header was makeCopy()d from the message-list and so needs to be
      // explicitly removed since it is not part of a slice.
      if (this.header) {
        this.header.__die();
        this.header = null;
      }
      if (this.body) {
        this.body.die();
        this.body = null;
      }
    }
  }
];
});
