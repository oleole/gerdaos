define([ "require", "l10n!", "cards", "./base", "template!./settings_account_servers.html" ], function(e) {
    var t, n = e("l10n!"), o = e("cards");
    return [ e("./base")(e("template!./settings_account_servers.html")), {
        onArgs: function(e) {
            this.account = e.account, this.server = e.account.servers[e.index], this.headerLabel.textContent = this.account.name, 
            n.setAttributes(this.serverLabel, "settings-" + this.server.type + "-label"), this.hostnameNodeSpan.innerHTML = this.server.connInfo.hostname || this.server.connInfo.server, 
            this.portNodeSpan.innerHTML = this.server.connInfo.port || "", "activesync" === this.server.type && this.portNode.classList.add("hidden"), 
            t = this;
        },
        handleKeyDown: function(e) {
            switch (e.key) {
              case "Backspace":
                e.preventDefault(), t.onBack();
            }
        },
        onCardVisible: function() {
            var e = [ {
                name: "Cancel",
                l10nId: "cancel",
                priority: 1,
                method: function() {
                    t.onBack();
                }
            } ];
            NavigationMap.setSoftKeyBar(e), window.addEventListener("keydown", t.handleKeyDown);
            var n = document.getElementsByTagName("cards-settings-account-servers")[0];
            n.setAttribute("role", "heading"), n.setAttribute("aria-labelledby", "settings-account-servers-header"), 
            this.formNode.focus();
        },
        onBack: function() {
            o.removeCardAndSuccessors(this, "animate", 1);
        },
        die: function() {
            window.removeEventListener("keydown", t.handleKeyDown);
        }
    } ];
});