define([ "require", "query_string", "services", "cards" ], function(e) {
    function t(e) {
        window.removeEventListener("message", t);
        var o = e.data;
        if ("oauth2Complete" === e.type) {
            if (!o.code) return r("reject", new Error("no code returned")), void 0;
            console.log("oauth redirect returned with code"), f = !0, n(o.code).then(function(e) {
                var t = 1e3 * parseInt(e.expires_in, 10), n = Date.now() + Math.max(0, t - m), o = {
                    status: "success",
                    tokens: {
                        accessToken: e.access_token,
                        refreshToken: e.refresh_token,
                        expireTimeMS: n
                    },
                    secrets: a
                };
                r("resolve", o);
            }, function(e) {
                r("reject", e);
            });
        } else r("resolve", {
            status: "cancel"
        });
    }
    function n(e) {
        return console.log("redeeming oauth code"), new Promise(function(t, n) {
            var o = new XMLHttpRequest({
                mozSystem: !0
            });
            o.open("POST", u.tokenEndpoint, !0), o.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"), 
            o.timeout = m, o.onload = function() {
                if (o.status < 200 || o.status >= 300) console.error("token redemption failed", o.status, o.responseText), 
                n("status" + o.status); else try {
                    var e = JSON.parse(o.responseText);
                    console.log("oauth code redeemed. access_token? " + !!e.access_token + ", refresh_token? " + !!e.refresh_token), 
                    t(e);
                } catch (r) {
                    console.error("badly formed JSON response for token redemption:", o.responseText), 
                    n(r);
                }
            }, o.onerror = function(e) {
                console.error("token redemption weird error:", e), n(e);
            }, o.ontimeout = function() {
                console.error("token redemption timeout"), n("timeout");
            };
            var r = {
                code: e,
                client_id: a.clientId,
                client_secret: a.clientSecret,
                redirect_uri: g,
                grant_type: "authorization_code"
            };
            o.send(i.fromObject(r));
        });
    }
    function o() {
        l && !l.closed || f || r("resolve", {
            status: "cancel"
        });
    }
    function r(e, t) {
        console.log("oauth2 fetch reset with action: " + e);
        var n = c[e];
        h && (clearInterval(h), h = null), l && !l.closed && l.close(), c = d = l = null, 
        f = !1, n(t);
    }
    var i = e("query_string"), s = e("services");
    e("cards");
    var a, c, d, u, l, h, p = s.oauth2, f = !1, m = 3e4, g = "http://localhost";
    const y = 300;
    return function(e, n) {
        if (c && r("reject", new Error("Multiple oauth calls, starting new one")), d = new Promise(function(e, t) {
            c = {
                resolve: e,
                reject: t
            };
        }), u = e, a = void 0, e.clientId && e.clientSecret ? a = {
            clientId: e.clientId,
            clientSecret: e.clientSecret
        } : e.secretGroup && (a = p[e.secretGroup]), !a) return r("reject", new Error("no secrets for group: " + e.secretGroup)), 
        d;
        var s = {
            client_id: a.clientId,
            redirect_uri: g,
            response_type: "code",
            scope: e.scope,
            max_auth_age: 0
        }, f = e.authEndpoint + "?" + i.fromObject(s);
        if (n) {
            var m = i.fromObject(n);
            m && (f += "&" + m);
        }
        return window.addEventListener("message", function(e) {
            t(e.data);
        }), l = window.open(f, "", "dialog"), h = setInterval(o, y), d;
    };
});