/* exported GestureDetector */

'use strict';

/**
 * hardkey.js: generate hard key event for feature phone.
 */
var keyCmdATT = ['lmcmLSK', 'lmcmRSK', 'lmcmFn', 'lmcmBack', 'lmcmNotify',
  'lmcmLeft', 'lmcmRight', 'lmcmUp', 'lmcmDn', 'lmcmOk',
  'lmcmLeft', 'lmcmRight', 'lmcmUp', 'lmcmDn',
  'lmcm1', 'lmcm2', 'lmcm3', 'lmcm4', 'lmcm5',
  'lmcm6', 'lmcm7', 'lmcm8', 'lmcm9', 'lmcm0',
  'lmcmPhone', 'lmcmHangUp', 'lmcmPound', 'lmcmMulti', 'lmcmClr'
];

var keyCmdGC = ['', '', 'lmcm5Star', 'lmcmNo', '',
  '', '', 'lmcmUp', 'lmcmDn', 'lmcmYes',
  '', '', 'lmcmUp', 'lmcmDn',
  'lmcm1', 'lmcm2', 'lmcm3', 'lmcm4', 'lmcm5',
  'lmcm6', 'lmcm7', 'lmcm8', 'lmcm9', 'lmcm0',
  '', '', 'lmcmPound', 'lmcmMulti', ''
];


var keyCode = ['F1', 'F2', '5Star', 'BrowserBack', 'F3',
  'ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown', 'Enter',
  'Left', 'Right', 'Up', 'Down',
  '1', '2', '3', '4', '5',
  '6', '7', '8', '9', '0',
  'F5', 'End', '*', '#', 'F8'
];
var keyStyle;
var iframeFocusNode = null;
var DEBUG = 1;

function debug(s) {
  if (DEBUG) {
    dump('<LM_LOG> -*- logmanager -*-: ' + s + '\n');
  }
}

function ConvertToCmd(keycode) {
  for (var i in keyCode) {
    if (keyCode[i] == keycode) {
      if (keyStyle != 'GC') {
        debug('ConvertToCmd ' + keyCmdATT[i]);
        return keyCmdATT[i];
      } else {
        debug('ConvertToCmd ' + keyCmdGC[i]);
        return keyCmdGC[i];
      }
    }
  }
  return '';
}

navigator.mozSettings.addObserver('keyboard.style', function getState(e) {
  keyStyle = e.settingValue;
});

var req = window.navigator.mozSettings.createLock().get('keyboard.style');
req.onsuccess = function bt_EnabledSuccess() {
  keyStyle = req.result['keyboard.style'];
};

window.addEventListener('keydown', function(event) {
  debug('addEventListener ' + event.key);
  var e = document.activeElement;
  var allNodes = document.getElementsByTagName('*');
  var length = allNodes.length;
  var i;
  var node;
  var flag = false;
  var style;

  switch (event.key) {
    case 'Up':
    case 'ArrowUp':
      // Do something for "VolumeUp" key press.
      for (i= length - 1; i >= 0; i--) {
        node = allNodes[i];
        style = window.getComputedStyle(node);
        if (flag && isFocusAvailable(node) && !isElementHide(node)) {
          break;
        }
        if (!iframeFocusNode && node == e || iframeFocusNode == node) {
          if (flag) {
            break;
          }
          flag = true;
          iframeFocusNode = null;
        }
        if (i == 0) {
          i = length;
        }
      }
      debug('changefocus pre ' + node.id + ' is focus');
      debug('changefocus tabIndex ' + node.tabIndex);
      if (node.tagName == 'IFRAME') {
        if (node.contentDocument) {
          iframeFocusNode = node;
          node.contentDocument.body.focus();
        }
      } else {
        node.focus();
      }

      event.preventDefault();
      break;
    case 'Down':
    case 'ArrowDown':
      // Do something for "VolumeDown" key press.
      for (i = 0; i < length; i++) {
        node = allNodes[i];
        style = window.getComputedStyle(node);
        if (flag && isFocusAvailable(node) && !isElementHide(node)) {
          break;
        }
        if (!iframeFocusNode && node == e || iframeFocusNode == node) {
          if (flag) {
            break;
          }
          flag = true;
          iframeFocusNode = null;
        }
        if (i == length - 1) {
          i = -1;
        }
      }
      debug('changefocus next  ' + node.id + ' is focus');
      debug('changefocus tabIndex ' + node.tabIndex);
      if (node.tagName == 'IFRAME') {
        if (node.contentDocument) {
          iframeFocusNode = node;
          node.contentDocument.body.focus();
        }
      } else {
        node.focus();
      }

      event.preventDefault();
      break;
  }
});

function isElementHide(e) {
  var style;
  while (e && e.style) {
    style = window.getComputedStyle(e);
    if (style.display == 'none' || style.visibility == 'hidden') {
      return true;
    }
    e = e.parentNode;
  }
  return false;
}

function isFocusAvailable(e) {
  if (e.tabIndex != -1 && e.lmFocus == true && e.disabled != true) {
    return true;
  }
  return false;
}

