'use strict';

var PhotoResize = {
  resize: function(blob) {
    if (blob instanceof Array) {
      blob = blob[0];
    }
    return new Promise(resolve => {
      let desWidth = 32;
      let desHeight = 32; // Maybe should * window.devicePixelRatio in future
      ImageUtils.resizeAndCropToCover(blob, desWidth, desHeight, ImageUtils.PNG)
        .then(resizedBlob => {
          resolve(resizedBlob);
      });
    });
  }
};
