/* globals PerformanceTestingHelper, Contacts, CallLogDBManager, LazyLoader,
           Utils, StickyHeader, KeypadManager, SimSettingsHelper,
           CallHandler, AccessibilityHelper, Service, CallLogApp,
           NavbarManager, OptionHelper, NavigationMap, CallInfo, MozActivity,
           ConfirmDialog, Toaster, fb, CallGroupMenu */

'use strict';
var CallLog = {
  _initialized: false,
  _headersInterval: null,
  _empty: true,
  _dbupgrading: false,
  _contactCache: false,
  _removeCacheNode: false,
  _tabIndex: [
    'all',
    'missed',
    'dialed',
    'received'
  ],
  tab: document.querySelector('gaia-tabs'),
  callLogReady: false,
  getCurrentTabName: function cl_getCurrentTabName() {
    return this._tabIndex[this.tab.selected];
  },

  init: function cl_init() {
    if (this._initialized) {
      this.becameVisible();
      return;
    }

    window.performance.mark('callLogStart');
    PerformanceTestingHelper.dispatch('start-call-log');
    document.addEventListener('focusChanged', function (e) {
      var activeEl = e.detail.focusedElement;
      var editMode = document.body.classList.contains('recents-edit');
      if (!editMode && !OptionHelper.CallInfoVisible && activeEl) {
        self.renderOptionMenu(activeEl);
      } else if (editMode) {
        var noResultContainer = document.getElementById('no-result-container');
        if (!noResultContainer.hidden) {
          return;
        }
        self.updateSoftKeyState();
      }
    });

    this._initialized = true;

    var lazyFiles = [
      '/shared/style/confirm.css',
      '/shared/style/switches.css',
      '/shared/style/lists.css',
      '/shared/js/confirm.js',
      '/dialer/js/utils.js',
      '/shared/js/sticky_header.js',
      '/shared/js/sim_settings_helper.js',
      '/shared/js/date_time_helper.js'
    ];
    var self = this;

    var validContactsCachePromise = this._validateContactsCache();

    LazyLoader.load(lazyFiles, function resourcesLoaded() {
      var mainNodes = [
        'all-filter',
        'call-log-container',
        'call-log-edit-mode',
        'call-log-filter',
        'call-log-view',
        'deselect-all-threads',
        'delete-button',
        'edit-mode-header',
        'header-edit-mode-text',
        'missed-filter',
        'dialed-filter',
        'received-filter',
        'select-all-threads',
        'call-log-upgrading',
        'call-log-upgrade-progress',
        'call-log-upgrade-percent'
      ];

      mainNodes.forEach(function (id) {
        this[Utils.toCamelCase(id)] = document.getElementById(id);
      }, self);

      validContactsCachePromise.then(function () {
        self.sticky = new StickyHeader(self.callLogContainer, document.getElementById('sticky'));
        self.render();

        self._tabIndex.forEach((tabName) => {
          self[tabName + 'Filter'].addEventListener('click', () => {
            self.selectTab(tabName);
          });
        });
        window.addEventListener('timeformatchange',
          self._updateCallTimes.bind(self));
        self.editModeHeader && self.editModeHeader.addEventListener('action',
          self.hideEditMode.bind(self));
        self.callLogContainer.addEventListener('contextmenu', self);
        self.selectAllThreads.addEventListener('click',
          self.selectAll.bind(self));
        self.deselectAllThreads.addEventListener('click',
          self.deselectAll.bind(self));
        self.deleteButton.addEventListener('click',
          self.deleteLogGroups.bind(self));
        window.addEventListener('largetextenabledchanged', () => {
          document.body.classList.toggle('large-text', navigator.largeTextEnabled);
        });
        window.navigator.mozSetMessageHandler('activity', (activity) => {
          if (activity.source.data && activity.source.data.type == 'dialer_open_calllog') {
            NavbarManager.dialerOpenCalllog = true;
          } else {
            NavbarManager.dialerOpenCalllog = false;
          }
        });
        document.addEventListener('visibilitychange', function () {
          if (document.hidden) {
            self.pauseHeaders();
          } else {
            self.updateHeadersContinuously();
            if (window.location.hash === '#call-log-view') {
              self.becameVisible();
              var editMode = document.body.classList.contains('recents-edit');
              if (!editMode && !OptionHelper.CallInfoVisible) {
                self.renderOptionMenu();
              }
            }
          }
        });

        // workaround patch, ConfimationDialogue is no focus cause it....
        window.addEventListener('keydown', (evt) => {
          if ((evt.key === 'ArrowUp' || evt.key === 'ArrowDown') &&
            self.isConfimationDialogueShown()) {
            evt.stopPropagation();
            evt.preventDefault();
          }
        }, true);

        window.addEventListener('keydown', function (evt) {
          console.log('call_log keydown. evt.key: ' + evt.key);
          if (evt.key == 'ArrowLeft' || evt.key == 'ArrowRight') {
            evt.preventDefault();
          }
          if (self.isConfirmDialogHelperShown()) {
            evt.preventDefault();
            return;
          }
          if (NavigationMap.menuIsActive) {
            return;
          }

          if (CallMenu.optionMenu && CallMenu.optionMenu.form &&
            CallMenu.optionMenu.form.classList.contains('visible')) {
            return;
          }

          if (self.isConfimationDialogueShown() && (evt.key != 'Backspace')) {
            return;
          }
          var editMode = document.body.classList.contains('recents-edit');
          var noResultContainer = document.getElementById('no-result-container');

          switch (evt.key) {
            case 'ArrowLeft':
            case 'ArrowRight':
              if (OptionHelper.CallInfoVisible) {
                break;
              }
              self.navigateTab(evt.key);
              if (editMode) {
                if (!noResultContainer.hidden) {
                  return;
                }
                self.updateHeaderCount();
                self.updateSoftKeyState();
              }
              break;
              //todo remove on release
            case 'g':
              CallLog.generateCallLog();
              break;
            case 'BrowserBack':
            case 'Backspace':
              if (self.isConfimationDialogueShown()) {
                self.closeConfirmationDialog();
                break;
              }
              if (typeof CustomDialog !== 'undefined' &&
                CustomDialog.isVisible) {
                CustomDialog.runCancel();
                break;
              }
              if (editMode) {
                if (!noResultContainer.hidden) {
                  window.close();
                  return;
                }
                self.renderOptionMenu();
                self.hideEditMode();
              } else if (!OptionHelper.CallInfoVisible) {
                if (NavbarManager.dialerOpenCalllog) {
                  new MozActivity({
                    name: 'dial',
                    data: {
                      number: ''
                    }
                  });
                } else {
                  window.close();
                }
              } else {
                CallInfo.close(evt);
                var header = document.getElementById('call-info-gaia-header');
                var config = {
                  detail: {
                    type: 'back'
                  }
                };
                var e = new CustomEvent('action', config);
                header.dispatchEvent(e);
                OptionHelper.CallInfoVisible = false;
                OptionHelper.show(NavigationMap.latestClassName);
                NavigationMap.reset(NavigationMap.latestClassName, NavigationMap.latestNavId);
              }
              break;
            case 'Call':
              if (editMode || OptionHelper.CallInfoVisible) {
                return;
              }
              if (!noResultContainer.hidden) {
                return;
              }
              // invoke react sim chooser
              navigator.hasFeature('device.capability.vilte').then((hasVT) => {
                CallMenu.dial(OptionHelper.defaultServiceId, null, !hasVT);
              });
              break;
              // handle "Send" button
            case 'F4': // Pixi
            case 'Shift': // Emulator
              self.onSendClick();
              break;
            case 'Enter':
              if (!noResultContainer.hidden) {
                return;
              }
              if (document.body.classList.contains('recents-edit')) {
                const logItem = evt.target;
                // Landed on the logItem (when using the screen reader).
                const checkbox = logItem.getElementsByTagName('input')[0];
                const toggleChecked = !checkbox.checked;
                checkbox.checked = toggleChecked;
                logItem.setAttribute('aria-selected', toggleChecked);

                self.updateHeaderCount();
                self.updateSoftKeyState();
                return;
              }
              break;
              // handle "Delete" button
            case 'Clear': // on device
              if (editMode) {
                self.deleteSelectedItems();
                break;
              }
              if (OptionHelper.CallInfoVisible) {
                break;
              }
              self.deleteLogEntry();
              break;
          }
        });

        self.becameVisible();
      });
    });

    // Listen for database upgrade events.
    CallLogDBManager.onupgradeneeded = function onupgradeneeded() {
      // Show a progress bar letting the user know that the database is being
      // upgraded.
      self.showUpgrading();
      self._dbupgrading = true;
    };

    CallLogDBManager.onupgradeprogress = function onupgradeprogress(progress) {
      self.updateUpgradeProgress(progress);
    };

    CallLogDBManager.onupgradedone = function onupgradedone() {
      self.hideUpgrading();
      self._dbupgrading = false;
      self.render();
    };

  },
  //send click event handler
  onSendClick: function (evt) {
    var focusedEls = document.querySelectorAll('.focus');
    (focusedEls.length > 0) && focusedEls[0].click();
  },
  //check confirmation dialogue visible state
  isConfimationDialogueShown: function () {
    var confDialog = document.querySelector('#confirmation-message');
    return !confDialog.classList.contains('hide');
  },
  //hide confirmation dialogue
  closeConfirmationDialog: function () {
    var b = document.querySelector('#confirmation-message button[data-l10n-id="cancel"]');
    b = b || document.querySelector('#confirmation-message .full');
    if (b) {
      b.click();
    }
  },
  /**
   * Returns a promise that is fullfilled once the contact cache has been
   * validated. Note that the contents of the contact cache may still be stale
   * after the promise is fullfilled. This only guarantees that
   * this._contactCache contains a valid value.
   *
   * @return {Promise} A promise that is fullfilled once we've validated the
   *                   contact cache.
   */
  _validateContactsCache: function cl_validateContactsCache() {
    var self = this;

    return new Promise(function (resolve, reject) {
      /* Get the latest contact cache revision and the actual Contacts API
       * db revision. If both values differ, we need to update the contact cache
       * and its revision and directly query the Contacts API to render the
       * appropriate information while the cache is being rebuilt. */
      window.asyncStorage.getItem('contactCacheRevision',
        function onItem(cacheRevision) {
          Contacts.getRevision(function (contactsRevision) {
            /* We don't need to sync if this is the first time that we use the
             * call log. */
            if (!cacheRevision || cacheRevision > contactsRevision) {
              window.asyncStorage.setItem('contactCacheRevision',
                contactsRevision);
              self._contactCache = true;
              resolve();
              return;
            }

            self._contactCache = (cacheRevision >= contactsRevision);
            if (self._contactCache) {
              resolve();
              return;
            }
            if (CallLogCacheRestore.restored) {
              CallLogCacheRestore.restored = false;
              self._removeCacheNode = true;
            }
            CallLogDBManager.invalidateContactsCache(function (error) {
              if (!error) {
                self._contactCache = true;
                resolve();
              }
            });
          });
        });
    });
  },

  _updateCallTimes: function cl_updateCallTimes() {
    var logItemElts = this.callLogContainer.querySelectorAll('.log-item');
    for (var i = 0; i < logItemElts.length; i++) {
      var logItemElt = logItemElts[i];
      var timestamp = logItemElt.getAttribute('data-timestamp');
      var formattedTime = Utils.prettyDate(parseInt(timestamp, 10)) + ' ';
      var callTimeElt = logItemElt.querySelector('.call-time');
      callTimeElt.textContent = formattedTime;
    }
  },

  // Helper to update UI and clean notifications when we got visibility
  becameVisible: function cl_becameVisible() {
    this.updateHeaders();
    this.cleanNotifications();
    this.showFilterTitle();
    NavigationMap.update();
  },

  // Method for updating the time in headers based on device time
  updateHeaders: function cl_updateHeaders() {
    var headers = this.callLogContainer.getElementsByTagName('header');
    for (var i = 0, l = headers.length; i < l; i++) {
      var current = headers[i];
      var parsedInfo = Utils.headerDate(parseInt(current.dataset.timestamp));
      if (parsedInfo !== current.textContent) {
        current.textContent = parsedInfo;
      }
    }
  },

  // Method that starts updating headers every minute, based on device time
  updateHeadersContinuously: function cl_updateHeaders() {
    var updater = this.updateHeaders.bind(this);
    this._headersInterval = setInterval(updater, 60000);
  },

  pauseHeaders: function cl_pauseHeaders() {
    clearInterval(this._headersInterval);
  },

  // Method for rendering the whole list of logs for first time,
  // getting the groups from the DB and rendering them in order.
  render: function cl_render() {
    var self = this;
    var daysToRender = [];
    var chunk = [];
    var prevDate;
    var screenRendered = false;
    var MAX_CALLLOG_ITEMS_FOR_FIRST_RENDER = 8;
    var MAX_GROUPS_TO_BATCH_RENDER = 20;
    var batchGroupCounter = 0;
    var firstRender = true;
    document.getElementById('call-log-filter').classList.remove('hide');
    if (!CallLogCacheRestore.restored) {
      self.showUpgrading(); //before get information from callLogDbManager we should notice user about data getting process
    }
    CallLogDBManager.getGroupList(function logGroupsRetrieved(cursor) {
      self.hideUpgrading(); //hide upgrading after callLogDBManager return result
      if (!cursor.value) {
        if (self._dbupgrading) {
          return;
        }
        if (chunk.length === 0) {
          self.renderEmptyCallLog();
        } else {
          daysToRender.push(chunk);
          self.renderSeveralDays(daysToRender, firstRender);
          firstRender = false;
          if (!screenRendered) {
            window.performance.mark('firstChunkReady');
            PerformanceTestingHelper.dispatch('first-chunk-ready');
          }
          self.sticky.refresh();
          self.updateHeadersContinuously();
        }
        window.performance.measure('callLogReady', 'callLogStart');
        PerformanceTestingHelper.dispatch('call-log-ready');

        self.callLogReady = true;
        window.dispatchEvent(new CustomEvent('first-call-logs-ready'));
        window.dispatchEvent(new CustomEvent('call-log-ready'));
        CallLogCache.saveFromNode('cache-list', self.callLogContainer);
        return;
      }

      self._empty = false;
      var currDate = new Date(cursor.value.date);
      batchGroupCounter++;
      if (!prevDate || (currDate.getTime() == prevDate.getTime())) {
        if (batchGroupCounter >= MAX_CALLLOG_ITEMS_FOR_FIRST_RENDER &&
          !screenRendered) {
          if (chunk.length) {
            daysToRender.push(chunk);
          }
          chunk = [cursor.value];
          renderNow = true;
          screenRendered = true;
          window.performance.mark('firstChunkReady');
          PerformanceTestingHelper.dispatch('first-chunk-ready');
        } else {
          chunk.push(cursor.value);
        }
      } else {
        if (chunk.length) {
          daysToRender.push(chunk);
        }
        chunk = [cursor.value];
        var renderNow = false;
        if (batchGroupCounter >= MAX_CALLLOG_ITEMS_FOR_FIRST_RENDER &&
          !screenRendered) {
          renderNow = true;
          screenRendered = true;
          window.performance.mark('firstChunkReady');
          PerformanceTestingHelper.dispatch('first-chunk-ready');
        } else if (batchGroupCounter >= MAX_GROUPS_TO_BATCH_RENDER) {
          renderNow = true;
        }
      }
      if (renderNow) {
        self.renderSeveralDays(daysToRender, firstRender);
        firstRender = false;
        daysToRender = [];
        batchGroupCounter = 0;
      }
      prevDate = currDate;
      cursor.continue();
    }, 'lastEntryDate', true, true);

  },

  renderSeveralDays: function cl_renderSeveralDays(chunks, firstRender) {
    var i, j;
    var callLogContainer = document.getElementById('call-log-container');
    if (CallLogCacheRestore.restored && firstRender) {
      var chunk;
      var callLogItem;
      for (i = 0; i < chunks.length; i++) {
        for (j = 0; j < chunks[i].length; j++) {
          chunk = chunks[i][j];
          if (!chunk.emergency) {
            callLogItem = document.getElementById(chunk.id);
            if (!callLogItem) {
              continue;
            }
            if (Voicemail.check(chunk.number)) {
              this.updateContactInfo(callLogItem);
            } else if (chunk.contact) {
              this.updateContactInfo(callLogItem, chunk.contact,
                chunk.contact.matchingTel);
            } else {
              this.updateContactInfo(callLogItem);
            }
          }
        }
      }
      NavigationMap.update();
      window.dispatchEvent(new CustomEvent('first-call-logs-ready'));
      return;
    }
    if (this._removeCacheNode) {
      var sections = callLogContainer.querySelectorAll('section');
      for (i = 0; i < sections.length; i++) {
        callLogContainer.removeChild(sections[i]);
      };
      this._removeCacheNode = false;
    }
    var checkSectionExist = CallLogCacheRestore.restored;
    for (var i = 0, l = chunks.length; i < l; i++) {
      checkSectionExist = this.renderChunk(chunks[i], checkSectionExist);
    }
    window.performance.mark('visuallyLoaded');
  },

  renderChunk: function cl_renderChunk(chunk, checkSectionExist) {
    var callLogSection;
    var header;
    if (checkSectionExist) {
      header = document.getElementById('header-' + chunk[0].date);
      if (header) {
        callLogSection = header.parentNode;
      }
    }
    if (!callLogSection) {
      callLogSection = this.createSectionForGroup(chunk[0]);
      checkSectionExist = false;
    }
    var phoneNumbers = [];
    var logGroupContainer = callLogSection.getElementsByTagName('ol')[0];
    for (var i = 0; i < chunk.length; i++) {
      var current = chunk[i];
      var logGroupDOM = this.createGroup(current);
      logGroupContainer.appendChild(logGroupDOM);
      phoneNumbers.push(current.number);
    }
    if (!checkSectionExist) {
      this.callLogContainer.appendChild(callLogSection);
    }

    NavigationMap.update();
    window.dispatchEvent(new CustomEvent('first-call-logs-ready'));
    return checkSectionExist;
  },

  renderEmptyCallLog: function cl_renderEmptyCallLog(isFilteredGroup) {
    isFilteredGroup = !!isFilteredGroup;
    OptionHelper.show('empty-call');
    // If rendering the empty call log for all calls (i.e. the
    // isFilteredGroup not set), set the _empty parameter to true
    if (!isFilteredGroup) {
      this._empty = true;
    }

    var noResultContainer = document.getElementById('no-result-container');

    noResultContainer.hidden = false;
    document.getElementById('no-result-msg1').hidden = isFilteredGroup;
    document.getElementById('no-result-msg2').hidden = isFilteredGroup;
    document.getElementById('no-result-msg3').hidden = !isFilteredGroup;
    document.getElementById('no-result-msg4').hidden = !isFilteredGroup;
    // If the whole call log is cleared, remove any stale call log nodes from
    // the call log container
    if (!isFilteredGroup) {
      var noResultContainerClone = noResultContainer.cloneNode(true);
      this.callLogContainer.innerHTML = '';
      this.callLogContainer.appendChild(noResultContainerClone);
    }
  },

  //todo remove in release
  deleteCallLog: function cl_deleteCallLog() {
    console.log('CallLog deleteCallLog');
    var self = this;
    CallLogDBManager.deleteAll(function onDeleteAll() {
      self.renderEmptyCallLog();
      console.log('CallLog onDeleteAll');
      NavigationMap.reset();
      CallLogCache.saveFromNode('cache-list', CallLog.callLogContainer);
    });
  },

  //todo remove in release
  generateCallLog: function cl_generateCallLog() {
    var self = this;
    var number = '8910123450';
    for (var i = 0; i < 6; i++) {
      var d = new Date();
      d.setDate(d.getDate() - i);
      var entry = {
        date: d.getTime(),
        duration: i % 2 === 0 ? 60000 : 0,
        type: i === 0 ? 'dialing' : 'incoming',
        number: number + i,
        serviceId: 0,
        emergency: false,
        voicemail: false,
        status: i % 2 === 0 ? 'connected' : null
      };
      CallLogDBManager.add(entry, function (logEntry) {
        self.appendGroupAndSaveCache(logEntry);
        NavigationMap.reset();
      });
    }
  },

  renderOptionMenu: function (focusedEl) {
    var phoneNumber;
    focusedEl = focusedEl || document.activeElement;
    phoneNumber = focusedEl.getAttribute('data-phone-number');
    if (phoneNumber ===  null) {
      return;
    }
    Contacts.findByNumber(phoneNumber, function (contact, matchingTel) {
      // Donot re-render softkey when CustomDialog is visible
      if (typeof CustomDialog !== 'undefined' && CustomDialog.isVisible) {
        return;
      }
      
      var optionMenu = document.getElementById('option-menu');
      if (optionMenu) {
        return;
      }

      if (!contact) {
        OptionHelper.show('log-item-new');
      } else {
        OptionHelper.show('log-item');
      }
    });
  },

  appendGroupAndSaveCache: function cl_appendGroupAndSaveCache(group) {
    var logGroupDOM = this.appendGroup(group);
    if (logGroupDOM) {
      CallLogCache.saveFromNode('cache-list', CallLog.callLogContainer);
    }
    return logGroupDOM;
  },
  // Method for appending a new group in the list.
  // We need to ensure where to put the group, taking into account
  // that the user might have changed date and time in his device.
  appendGroup: function cl_appendGroup(group) {
    if (!this._initialized) {
      return;
    }

    // Switch to all calls tab to avoid erroneous call filtering
    this.selectTab('all');

    // Create element of logGroup
    var logGroupDOM = this.createGroup(group);
    var dayIndex = group.date;
    var container = this.callLogContainer;
    var previousLogGroup = document.getElementById(group.id);

    if (previousLogGroup) {
      // We already have a group with the same id, so just remove it
      // and re-insert it in the right place
      var parent = previousLogGroup.parentNode;
      parent.removeChild(previousLogGroup);
      this.insertInSection(logGroupDOM, parent);
      return logGroupDOM;
    }

    var groupSelector = '[data-timestamp="' + dayIndex + '"]';
    var sectionExists = container.querySelector(groupSelector);

    if (sectionExists) {
      // We found a section to place the group, so just insert it
      // in the right position.
      var section = sectionExists.getElementsByTagName('ol')[0];
      this.insertInSection(logGroupDOM, section);
      return logGroupDOM;
    }

    // We don't have any call for that day, so creating a new section
    var callLogSection = this.createSectionForGroup(group);
    var logGroupContainer = callLogSection.getElementsByTagName('ol')[0];
    logGroupContainer.appendChild(logGroupDOM);

    var previousSections = container.getElementsByTagName('section');
    var referenceSection;
    var previousSectionsLength = previousSections.length;
    for (var i = 0; i < previousSectionsLength; i++) {
      var current = previousSections[i];
      if (dayIndex > current.dataset.timestamp) {
        referenceSection = current;
        break;
      }
    }

    if (referenceSection) {
      container.insertBefore(callLogSection, referenceSection);
    } else {
      if (this._empty) {
        // Create a copy of the empty call log message div in order to add it
        // back in the empty container
        var noResultContainer =
          document.getElementById('no-result-container').cloneNode(true);
        container.innerHTML = '';
        noResultContainer.hidden = true;
        container.appendChild(noResultContainer);
        this._empty = false;
      }
      container.appendChild(callLogSection);
    }

    this.sticky.refresh();
    if (!this.groupMenuShown()) {
      NavigationMap.reset('log-item');
    }
    return logGroupDOM;
  },

  // Method that places a log group in the right place inside a section
  insertInSection: function cl_insertInSect(group, section) {
    var groups = section.getElementsByTagName('li');
    var groupsLength = groups.length;
    var referenceGroup;
    var date = group.dataset.timestamp;
    for (var i = 0; i < groupsLength; i++) {
      var current = groups[i];
      if (parseInt(date) > parseInt(current.dataset.timestamp)) {
        referenceGroup = current;
        break;
      }
    }

    if (referenceGroup) {
      section.insertBefore(group, referenceGroup);
    } else {
      section.appendChild(group);
    }
    if (!this.groupMenuShown()) {
      NavigationMap.reset('log-item');
    }
  },

  // Method that generates the markup for each of the rows in the call log.
  // Example:
  // <li data-contact-id="4bfa5f07c5584d48a1af7931b976a223"
  //  id="1369695600000-6136112351-dialing" data-type="dialing"
  //  data-phone-number="6136112351" data-timestamp="1369731559902"
  //  class="log-item" role="option" aria-selected="false">
  //    <label class="pack-checkbox call-log-selection danger"
  //     aria-hidden="true">
  //      <input value="1369695600000-6136112351-dialing" type="checkbox">
  //      <span></span>
  //    </label>
  //    <a role="presentation">
  //      <aside class="icon call-type-icon icon icon-outgoing">
  //      </aside>
  //      <p class="primary-info">
  //        <span class="primary-info-main">David R. Chichester</span>
  //        <span class="retry-count">(1)</span>
  //      </p>
  //      <p aria-hidden="true" class="additional-info">
  //        <span class="type-carrier">Mobile, O2</span>
  //        <span class="call-time">9:59 AM</span>
  //      </p>
  //    </a>
  // </li>
  createGroup: function cl_createGroup(group) {
    var date = group.lastEntryDate;
    var number = group.number;
    var type = group.type;
    var emergency = group.emergency;
    var voicemail = Voicemail.check(number);
    var status = group.status || '';
    var contact = group.contact;
    var groupDOM = document.createElement('li');
    groupDOM.classList.add('log-item');
    groupDOM.dataset.timestamp = date;
    groupDOM.dataset.phoneNumber = number;
    groupDOM.dataset.type = type;
    groupDOM.setAttribute('role', 'option');
    groupDOM.setAttribute('aria-selected', false);
    groupDOM.setAttribute('tabindex', 1);
    if (contact && contact.id) {
      groupDOM.dataset.contactId = contact.id;
    }
    groupDOM.id = group.id;
    let callType = '';
    switch (type) {
      case 'dialing':
      case 'alerting':
      case 'videoCallDialing':
        callType = 'outgoing';
        groupDOM.classList.add('dialed-call');
        break;
      case 'incoming':
      case 'videoCallIncoming':
        groupDOM.dataset.status = status;
        if (status === 'connected') {
          callType = 'incoming';
          groupDOM.classList.add('received-call');
        } else {
          callType = 'missed';
          groupDOM.classList.add('missed-call');
        }
        break;
    }

    let simNum = '';
    if (navigator.mozMobileConnections &&
      navigator.mozMobileConnections.length > 1 &&
      typeof group.serviceId !== 'undefined') {
      simNum = `-sim${parseInt(group.serviceId) + 1}`;
    }
    var iconData = '';
    if (type === 'videoCallDialing' || type === 'videoCallIncoming') {
      iconData = `video-call-${callType}${simNum}`;
    } else {
      iconData = `call-${callType}${simNum}`;
    }
    var label = document.createElement('label');
    label.setAttribute('aria-hidden', true);
    label.className = 'pack-checkbox-large call-log-selection danger';
    var input = document.createElement('input');
    input.setAttribute('type', 'checkbox');
    input.value = group.id;
    var div = document.createElement('div');

    label.appendChild(input);
    label.appendChild(div);

    var main = document.createElement('a');
    main.setAttribute('role', 'presentation');
    var iconContact = document.createElement('aside');
    iconContact.className = 'icon contact-type-icon default';
    if (!voicemail && !emergency && group.contact && group.contact.photo) {
      PhotoResize.resize(group.contact.photo).then(blob => {
        iconContact.style.backgroundImage = 'url(' + URL.createObjectURL(blob) + ')';
        iconContact.classList.remove('default');
      });
    }
    var icon = document.createElement('aside');
    icon.className = 'icon call-type-icon';
    icon.dataset.icon = iconData;

    var primInfo = document.createElement('p');
    primInfo.className = 'primary-info';

    var primInfoMain = document.createElement('span');
    primInfoMain.className = 'primary-info-main';
    var bdi = document.createElement('bdi');
    if (contact && contact.primaryInfo) {
      bdi.textContent = contact.primaryInfo;
    } else {
      if (number) {
        bdi.textContent = number;
      } else {
        bdi.setAttribute('data-l10n-id', 'withheld-number');
      }
    }
    primInfoMain.appendChild(bdi);
    primInfo.appendChild(primInfoMain);

    if (group.retryCount && group.retryCount > 1) {
      var retryCount = document.createElement('span');
      retryCount.className = 'retry-count';
      retryCount.textContent = '(' + group.retryCount + ')';
      primInfo.appendChild(retryCount);
    }

    var phoneNumberAdditionalInfo = '';
    var phoneNumberTypeL10nId = null;
    if (voicemail || emergency) {
      phoneNumberAdditionalInfo = number;
      phoneNumberTypeL10nId =
        emergency ? 'emergencyNumber' :
        (voicemail ? 'voiceMail' : null);
    } else if (contact && contact.matchingTel) {
      phoneNumberAdditionalInfo =
        Utils.getPhoneNumberAdditionalInfo(contact.matchingTel);
    } else {
      phoneNumberAdditionalInfo = {
        id: 'unknown'
      };
    }


    var addInfo = document.createElement('p');
    addInfo.className = 'additional-info';
    addInfo.setAttribute('aria-hidden', 'true');

    var typeAndCarrier = document.createElement('span');
    typeAndCarrier.className = 'type-carrier';
    if (phoneNumberAdditionalInfo) {
      if (typeof (phoneNumberAdditionalInfo) === 'string') {
        typeAndCarrier.removeAttribute('data-l10n-id');
        typeAndCarrier.textContent = phoneNumberAdditionalInfo;
        groupDOM.classList.toggle('min', false);
      } else {
        groupDOM.classList.toggle('min', true);
      }
    }
    addInfo.appendChild(typeAndCarrier);

    var callTimeInfo = document.createElement('p');
    callTimeInfo.className = 'call-time-info';
    callTimeInfo.setAttribute('aria-hidden', 'true');

    var callTime = document.createElement('span');
    callTime.className = 'call-time';
    callTime.textContent = Utils.prettyDate(date) + ' ';
    callTimeInfo.appendChild(callTime);

    main.appendChild(iconContact);
    main.appendChild(icon);
    main.appendChild(primInfo);
    main.appendChild(callTimeInfo);
    main.appendChild(addInfo);

    if (phoneNumberTypeL10nId) {
      // Check if this element has a `bdi` child, and remove it if it does.
      var bdiPrim = primInfoMain.querySelector('bdi');
      primInfoMain.removeChild(bdiPrim);

      primInfoMain.setAttribute('data-l10n-id', phoneNumberTypeL10nId);
      var primElem = primInfoMain.parentNode;
      var parent = primElem.parentNode;
      parent.insertBefore(addInfo, primElem.nextElementSibling);
    }

    groupDOM.appendChild(label);
    groupDOM.appendChild(main);

    return groupDOM;
  },

  // Method that creates a new section for placing groups
  createSectionForGroup: function cl_createSectionForGroup(group) {
    var referenceTimestamp = group.date;
    var groupContainer = document.createElement('section');
    groupContainer.dataset.timestamp = referenceTimestamp;
    var header = document.createElement('header');
    header.textContent = Utils.headerDate(referenceTimestamp);
    header.dataset.timestamp = referenceTimestamp;
    header.id = 'header-' + referenceTimestamp;
    header.dataset.update = true;
    var ol = document.createElement('ol');
    ol.classList.add('log-group');
    ol.setAttribute('role', 'listbox');
    ol.setAttribute('aria-multiselectable', true);
    ol.id = 'group-container-' + referenceTimestamp;

    groupContainer.appendChild(header);
    groupContainer.appendChild(ol);

    return groupContainer;
  },

  showEditMode: function cl_showEditMode(event) {
    // we don't have callLogIconEdit
    /*
    if (this.callLogIconEdit.hasAttribute('disabled')) {
      // Disabled does not have effect on an anchor.
      event.preventDefault();
      event.stopPropagation();
      return;
    }
    */
    this.headerEditModeText.setAttribute('data-l10n-id', 'tcl-edit-not-selected');
    this.deleteButton.setAttribute('disabled', 'disabled');
    this.selectAllThreads.removeAttribute('disabled');
    this.selectAllThreads.setAttribute('data-l10n-id', 'selectAll');
    this.deselectAllThreads.setAttribute('disabled', 'disabled');
    this.updateSoftKeyState(); //we need to hide right soft 'delete' button if nothing is selected
    document.body.classList.add('recents-edit');
    var tempElement = document.getElementById('call-log-container').parentNode;
    tempElement.setAttribute('aria-labelledby','header-edit-mode-text');
  },

  hideEditMode: function cl_hideEditMode() {
    document.body.classList.remove('recents-edit');
    var cont = this.callLogContainer;
    var inputs = cont.querySelectorAll('input[type="checkbox"]');
    var logItems = cont.querySelectorAll('.log-item');
    var i, l;
    for (i = 0, l = inputs.length; i < l; i++) {
      inputs[i].checked = false;
    }
    for (i = 0, l = logItems.length; i < l; i++) {
      logItems[i].setAttribute('aria-selected', false);
    }
    var tempElement = document.getElementById('call-log-container').parentNode;
    tempElement.setAttribute('aria-labelledby','calllog');
    document.activeElement.blur();
  },

  showUpgrading: function cl_showUpgrading() {
    this.callLogUpgrading.classList.remove('hide');
  },

  hideUpgrading: function cl_hideUpgrading() {
    this.callLogUpgrading.classList.add('hide');
  },

  updateUpgradeProgress: function cl_updateUpgradeProgress(progress) {
    this.callLogUpgradeProgress.setAttribute('value', progress);
    this.callLogUpgradePercent.textContent = progress + '%';
  },

  showFilterTitle: function cl_showFilterTitle() {
    var itemsFilter = document.querySelectorAll('ul[aria-controls="call-log-container"] > li');
    if (itemsFilter.length > 1) {
      for (var j = 0; j < itemsFilter.length; j++) {
        var itemFilter = itemsFilter.item(j);
        if (itemFilter.querySelector('[aria-selected="true"]')) {
          //itemFilter.hidden = false;
          continue;
        }
        if (itemFilter.querySelector('[aria-selected="false"]')) {
          //itemFilter.hidden = true;
          continue;
        }
      }
    }
  },

  navigateTab: function cl_navigateTab(eventKey) {
    let nextTabName;
    let isRtl = document.documentElement.dir === 'rtl';
    let arrowKeys = isRtl ? ['ArrowRight', 'ArrowLeft'] : ['ArrowLeft', 'ArrowRight'];
    switch (eventKey) {
      case arrowKeys[0]:
        if (this.tab.selected <= 0) {
          return;
        }
        nextTabName = this._tabIndex[this.tab.selected - 1];
        break;
      case arrowKeys[1]:
        if (this.tab.selected >= this.tab.querySelectorAll('a').length - 1) {
          return;
        }
        nextTabName = this._tabIndex[this.tab.selected + 1];
        break;
      default:
        return;
    }

    this.selectTab(nextTabName);
  },

  selectTab: function cl_selectTab(tabName, selectedItem) {
    const index = this._tabIndex.indexOf(tabName);

    this.tab.select(index);
    const currentTabName = this.getCurrentTabName();

    // set aria
    const ariaElements = this._tabIndex.map(tabName => {
      return this[tabName + 'Filter'].firstElementChild;
    });
    this.callLogContainer.setAttribute('aria-labelledby', currentTabName + '-filter-tab');

    this.updateGroupFiltered(currentTabName);
    this.showFilterTitle();

    if (typeof SoftkeyHelper !== 'undefined' && NavigationMap.menuIsActive) {
      SoftkeyHelper.getSoftkey().hideMenu();
    }

    if (currentTabName == 'all') {
      NavigationMap.reset('log-item', selectedItem);
    } else {
      NavigationMap.reset(currentTabName + '-call', selectedItem);
    }

  },

  selectTabByClassName: function cl_selectTabClassName(className, selectedItem) {
    let tabName = ({
      'log-item': 'all',
      'missed-call': 'missed',
      'dialed-call': 'dialed',
      'received-call': 'received'
    })[className];

    if (!tabName) {
      tabName = 'all';
    }
    this.selectTab(tabName, selectedItem);
  },

  updateGroupFiltered: function cl_updateGroupFiltered(tabName) {

    // reset then determine elements to show
    for (let section of this.callLogContainer.getElementsByTagName('section')) {
      section.classList.remove('groupFiltered');
      for (let call of section.getElementsByClassName('log-item')) {
        call.classList.remove('filtered');
      }
    }
    switch (tabName) {
      case 'all':
        if (this._empty) {
          this.renderEmptyCallLog();
        } else {
          var noResultContainer = document.getElementById('no-result-container');
          noResultContainer.hidden = true;
        }

        var shownContainers = document.querySelector('section[data-timestamp]');
        if (!shownContainers) {
          this.renderEmptyCallLog(true);
        }
        break;
      case 'missed':
      case 'dialed':
      case 'received':
        const targetClassName = tabName + '-call';

        var hasTargetCalls = false;
        for (let section of this.callLogContainer.getElementsByTagName('section')) {
          for (let call of section.getElementsByClassName('log-item')) {
            if (!call.classList.contains(targetClassName)) {
              call.classList.add('filtered');
            }
          }

          if (section.getElementsByClassName(targetClassName).length !== 0) {
            hasTargetCalls = true;
          } else {
            section.classList.add('groupFiltered');
          }
        }

        // If there are no target calls to display, show appropriate empty call log,
        // otherwise hide the empty call log message and enable edit mode
        if (!hasTargetCalls) {
          this.renderEmptyCallLog(true);
        } else {
          const noResultContainer = document.getElementById('no-result-container');
          noResultContainer.hidden = true;
        }
        break;
    }
  },

  getSelected: function () {
    return this.callLogContainer.querySelectorAll('.log-item:not(.filtered) input[type="checkbox"]:checked');
  },

  getAllInputs: function () {
    return this.callLogContainer.querySelectorAll('.log-item:not(.filtered) input[type="checkbox"]');
  },

  getSelectedCount: function () {
    return this.getSelected().length;
  },

  updateHeaderCount: function cl_updateHeaderCount() {
    var selected = this.getSelected().length;
    var allInputs = this.getAllInputs().length;

    if (selected === 0) {
      this.headerEditModeText.setAttribute('data-l10n-id', 'tcl-edit-not-selected');
      this.selectAllThreads.removeAttribute('disabled');
      this.selectAllThreads.setAttribute('data-l10n-id', 'selectAll');
      this.deselectAllThreads.setAttribute('disabled', 'disabled');
      //this.deleteButton.setAttribute('disabled', 'disabled');
      this.deleteButton.style.visibility = 'hidden';
      return;
    }
    navigator.mozL10n.setAttributes(
      this.headerEditModeText,
      'edit-selected', {
        n: selected
      });

    this.deleteButton.removeAttribute('disabled');
    if (selected === allInputs) {
      this.deselectAllThreads.removeAttribute('disabled');
      this.selectAllThreads.setAttribute('disabled', 'disabled');
    } else {
      this.selectAllThreads.removeAttribute('disabled');
      this.selectAllThreads.setAttribute('data-l10n-id', 'selectAll');
      this.deselectAllThreads.removeAttribute('disabled');
    }
  },

  selectAll: function cl_selectAll() {
    var inputs = this.getAllInputs();
    for (var i = 0, l = inputs.length; i < l; i++) {
      inputs[i].checked = true;
      inputs[i].parentElement.parentElement.setAttribute("aria-selected", "true");
    }
    this.updateHeaderCount();
    this.updateSoftKeyState();
  },

  deselectAll: function cl_selectAll() {
    var inputs = this.getAllInputs();
    for (var i = 0, l = inputs.length; i < l; i++) {
      inputs[i].checked = false;
      inputs[i].parentElement.parentElement.setAttribute("aria-selected", "false");
    }
    this.updateHeaderCount();
    this.updateSoftKeyState();
  },
  //update soft right key button state in edit mode {delete button}
  updateSoftKeyState: function cl_updateSoftKeyState() {
    var focused = document.querySelector('.log-item.focus:not(.filtered)');
    var selected = this.getSelectedCount(),
      allInputs = this.getAllInputs().length;
    if (selected === 0) {
      OptionHelper.show('edit-select-none');
    } else if (selected !== allInputs) {
      if (focused.getAttribute("aria-selected") === "true") {
        OptionHelper.show('edit-select');
      } else {
        OptionHelper.show('edit-no-select-focus');
      }
    } else if (selected === allInputs) {
      OptionHelper.show('edit-deselect');
    }
  },
  deleteSelectedItems: function () {
    console.log('delete selected items was called for ' + this.getSelectedCount());
    if (this.getSelectedCount() > 0) {
      this.deleteLogGroups();
    }
  },
  deleteLogGroups: function cl_deleteLogGroups() {
    var inputsSelected = this.getSelected();
    var self = this;
    var msg = null;
    var title = 'tcl-confirmation';
    var yesObject = {
      title: 'delete',
      isDanger: true,
      callback: function deleteLogGroup() {
        ConfirmDialog.hide();
        const toast = {
          messageL10nId: 'tcl-call-log-delete-group-notification',
          messageL10nArgs: {
            n: inputsSelected.length
          },
          latency: 2000,
          useTransition: true
        };
        var allInputs = self.getAllInputs();
        if (inputsSelected.length === allInputs.length &&
           self.getCurrentTabName() === 'all') {
          CallLogDBManager.deleteAll(function onDeleteAll() {
            self.renderEmptyCallLog();
            document.body.classList.remove('recents-edit');
            NavigationMap.reset();
            CallLogCache.saveFromNode('cache-list', CallLog.callLogContainer);
          });
          Toaster.showToast(toast);
          return;
        }
        let logItemsToDelete = [];
        for (let i = 0, l = inputsSelected.length; i < l; i++) {
          const logItem = inputsSelected[i].parentNode.parentNode;
          const olContainer = logItem.parentNode;
          const section = olContainer.parentNode;
          olContainer.removeChild(logItem);
          const currentTabClass = self.getCurrentTabName() + '-call';
          if (self.getCurrentTabName() !== 'all' &&
            olContainer.getElementsByClassName(currentTabClass).length === 0) {
            if (olContainer.children.length === 0) {
              self.callLogContainer.removeChild(section);
            } else {
              olContainer.parentNode.classList.add('groupFiltered');
            }
          } else if (olContainer.children.length === 0) {
            self.callLogContainer.removeChild(section);
          }
          var dataset = logItem.dataset;
          var toDelete = {
            date: parseInt(dataset.timestamp),
            number: dataset.phoneNumber === null ? '' : dataset.phoneNumber,
            type: dataset.type
          };
          if (dataset.status) {
            toDelete.status = dataset.status;
          }
          logItemsToDelete.push(toDelete);
        }

        CallLogDBManager.deleteGroupList(logItemsToDelete, function () {
          document.body.classList.remove('recents-edit');
          self.renderOptionMenu();
          CallLogCache.saveFromNode('cache-list', CallLog.callLogContainer);
        });
        OptionHelper.show(NavigationMap.latestClassName);
        NavigationMap.reset();

        var nothingText = document.getElementById('no-result-container');
        if (!nothingText.parentNode.querySelectorAll('section:not(.groupFiltered)').length) {
          nothingText.removeAttribute('hidden');
          OptionHelper.show('empty-call');
        }

        Toaster.showToast(toast);
      }
    };

    var noObject = {
      title: 'cancel',
      callback: function onCancel() {
        ConfirmDialog.hide();
        var selected = CallLog.getSelectedCount(),
          allInputs = CallLog.getAllInputs().length;
        if (selected && selected !== allInputs) {
          OptionHelper.show('edit-select');
        } else if (selected === allInputs) {
          OptionHelper.show('edit-deselect');
        }
        NavigationMap.reset(NavigationMap.latestClassName, NavigationMap.latestNavId);
      }
    };

    if (1 === inputsSelected.length) {
      const logItem = inputsSelected[0].parentNode.parentNode;
      var tempNum = logItem.dataset.phoneNumber;
      msg = { id: 'tcl-delete-single-log',
        args: { num: tempNum } };
      Contacts.findByNumber(tempNum, function (contact, matchingTel) {
        if (contact && matchingTel) {
          var primaryInfo = Utils.getPhoneNumberPrimaryInfo(matchingTel,
            contact);
          tempNum = primaryInfo;
          msg = { id: 'tcl-delete-single-log',
            args: { num: tempNum[0] } };
        }
        ConfirmDialog.show(title, msg, noObject, yesObject);
      });
    } else {
      msg = 'tcl-delete-log';
      ConfirmDialog.show(title, msg, noObject, yesObject);
    }
    OptionHelper.show('confirmation');
  },


  /**************************
   * Contacts related methods.
   **************************/

  // We need _updateContact and _removeContact aux functions to keep the
  // correct references to the log DOM element.
  _updateContact: function _updateContact(log, phoneNumber, contactId,
    updateDb) {
    var self = this;
    Contacts.findByNumber(phoneNumber,
      function (contact, matchingTel) {
        if (!contact || !matchingTel) {
          self._removeContact(log, contactId, updateDb);
          return;
        }

        // Update contact info.
        if (self._contactCache && updateDb) {
          CallLogDBManager.updateGroupContactInfo(contact, matchingTel,
            function (result) {
              if (typeof result === 'number' && result > 0) {
                self.updateContactInfo(log, contact, matchingTel);
              }
            });
        } else {
          self.updateContactInfo(log, contact, matchingTel);
        }
      });
  },

  _removeContact: function _removeContact(log, contactId, updateDb) {
    // If the cache is valid, we also need to remove the contact from the
    // cache
    if (this._contactCache && updateDb && (log.dataset.contactId === contactId)) {
      var self = this;

      CallLogDBManager.removeGroupContactInfo(contactId, null,
        function (result) {
          if (typeof result === 'number' && result > 0) {
            self.updateContactInfo(log);
          }
        });
    } else {
      this.updateContactInfo(log);
    }
  },

  /**
   * Updates the whole list of groups or part of it with the appropriate
   * contact information.
   *
   * This function will be triggered after receiving a 'oncontactchange' event
   * with 'create', 'remove' or 'update' reasons or during the initial rendering
   * for each chunk of data *only* if we detect that the contacts cache is not
   * valid.
   *
   * param reason
   *        String containing the reason of the 'oncontactchange' event or null
   *        if the function was triggered because of an invalid contacts cache.
   * param contactId
   *        Contact identifier if any. Only 'oncontactchange' events with
   *        'update' or 'remove' reasons will provide a contactId parameter.
   * param target
   *        DOM element to be updated. We default to the whole log if no
   *        'target' param is provided.
   */
  updateListWithContactInfo: function cl_updateList(reason, contactId, target) {
    var container = target || this.callLogContainer;

    if (!container) {
      return;
    }

    // Get the list of logs to be updated.
    var logs = [];
    switch (reason) {
      case 'remove':
        logs = container.querySelectorAll('li[data-contact-id="' + contactId +
          '"]');
        break;
        /*
        case 'create':
        case 'update':
        */
      default:
        logs = container.querySelectorAll('.log-item');
        break;
    }

    for (var i = 0, l = logs.length; i < l; i++) {
      var log = logs[i];
      var logInfo = log.dataset;
      this._updateContact(log, logInfo.phoneNumber, contactId, true);
    }
  },

  /**
   * Visually update the contact information of a group in the DOM.
   *
   * param element
   *        DOM element containing the group information.
   * param contact
   *        Object containing the contact information associated to the group
   *        of calls.
   * param matchingTel
   *        Object containing the phone number associated with the group of
   *        calls. It contains 'type', 'carrier' and 'value' parameters.
   *
   */
  updateContactInfo: function cl_updateContactInfo(element, contact,
    matchingTel) {
    var primInfoCont = element.querySelector('.primary-info-main');
    var addInfo = element.getElementsByClassName('additional-info')[0];
    var typeAndCarrier = addInfo.querySelector('.type-carrier');
    var contactIcon = element.querySelector('.contact-type-icon');
    var callTimeInfo = element.querySelector('.call-time-info');
    var number = element.dataset.phoneNumber;
    var voicemail = Voicemail.check(number);
    var emergency = !!element.querySelector('[data-l10n-id="emergencyNumber"]');
    var primElem = primInfoCont.parentNode;
    var parent = primElem.parentNode;
    if (emergency) {
      return;
    }
    if (voicemail) {
      parent.insertBefore(addInfo, primElem.nextElementSibling);
    } else {
      parent.insertBefore(callTimeInfo, primElem.nextElementSibling);
    }
    if (!matchingTel || voicemail) {
      if (element.dataset.contactId) {
        // Remove contact info.
        primInfoCont.textContent = '';
        contactIcon.style.backgroundImage = '';
        contactIcon.classList.add('default');
        delete element.dataset.contactId;
      }
      if (voicemail) {
        primInfoCont.setAttribute('data-l10n-id', 'voiceMail');
        typeAndCarrier.removeAttribute('data-l10n-id');
        typeAndCarrier.textContent = number;
      } else {
        primInfoCont.removeAttribute('data-l10n-id');
        primInfoCont.textContent = '';
        addInfo = number;
        var bdi = document.createElement('bdi');
        bdi.textContent = number;
        primInfoCont.appendChild(bdi);

        typeAndCarrier.removeAttribute('data-l10n-id');
        typeAndCarrier.textContent = '';
      }

      CallLogCache.saveFromNode('cache-list', CallLog.callLogContainer);
      return;
    }
    primInfoCont.removeAttribute('data-l10n-id');
    var primaryInfo =
      Utils.getPhoneNumberPrimaryInfo(matchingTel, contact);
    if (!primaryInfo) {
      primaryInfo = contact.primaryInfo;
    }
    if (primaryInfo) {
      primInfoCont.textContent = '';
      var bdiPrim = document.createElement('bdi');
      bdiPrim.textContent = primaryInfo;
      primInfoCont.appendChild(bdiPrim);
    }

    var phoneNumberAdditionalInfo =
      Utils.getPhoneNumberAdditionalInfo(matchingTel);
    if (phoneNumberAdditionalInfo) {
      if (typeof (phoneNumberAdditionalInfo) === 'string') {
        typeAndCarrier.removeAttribute('data-l10n-id');
        typeAndCarrier.textContent = phoneNumberAdditionalInfo;
        element.classList.toggle('min', false);
      } else {
        element.classList.toggle('min', true);
      }
    }
    if (contact) {
      element.dataset.contactId = contact.id;
      if (contact.photo) {
        PhotoResize.resize(contact.photo).then(blob => {
          contactIcon.style.backgroundImage =
            'url(' + URL.createObjectURL(blob) + ')';
          contactIcon.classList.remove('default');
        });
      } else {
        contactIcon.classList.add('default');
        contactIcon.style.backgroundImage = '';
      }
    }
    CallLogCache.saveFromNode('cache-list', CallLog.callLogContainer);
  },

  cleanNotifications: function cl_cleanNotifcations() {
    /* On startup of call log, we clear all dialer notification except for USSD
     * ones as those are closed only when the user taps them. */
    if (document.hidden) {
      return;
    }
    Notification.get()
      .then(
        function onSuccess(notifications) {
          for (var i = 0; i < notifications.length; i++) {
            if (!notifications[i].tag) {
              notifications[i].close();
            }
          }
        },
        function onError(reason) {
          console.debug('Call log Notification.get() promise error: ' + reason);
        }
    );
  },

  _getGroupFromLog: function cl_getGroupFromLog(log) {
    if (!log) {
      return;
    }
    var data = log.dataset;
    if (!data || !data.id) {
      return;
    }
    var groupId = data.id.split('-');
    var group = {
      date: groupId[0],
      number: groupId[1],
      type: groupId[2]
    };
    if (data.status) {
      group.status = data.status;
    }
    return group;
  },

  deleteLogEntry: function cl_deleteLogEntry() {
    var self = this;
    var itemForDelete = this.callLogContainer.querySelector('.focus');
    var dataset = itemForDelete.dataset;
    var toDelete = {
      date: parseInt(dataset.timestamp),
      number: dataset.phoneNumber === null ? '' : dataset.phoneNumber,
      type: dataset.type
    };
    if (dataset.status) {
      toDelete.status = dataset.status;
    }
    var ondeleted = function ondeleted(result) {
      // We expect a number. Otherwise that means that we got an error
      // message.
      var nfText = '';
      if (typeof result !== 'number') {
        nfText = navigator.mozL10n.get('tcl-calllog-entry-removed-err-notification');
      } else {
        var itemStyle = itemForDelete.style;
        var currentItemId = itemForDelete.getAttribute('data-nav-id');
        var nextItemID = itemStyle.getPropertyValue('--nav-down');
        if (nextItemID < currentItemId) {
          nextItemID = itemStyle.getPropertyValue('--nav-up');
        }
        var groupContainer = itemForDelete.parentNode;
        groupContainer.removeChild(itemForDelete);

        const currentTabClass = self.getCurrentTabName() + '-call';
        if (self.getCurrentTabName() !== 'all' &&
          groupContainer.getElementsByClassName(currentTabClass).length === 0) {
          groupContainer.parentNode.classList.add('groupFiltered');
        } else if (groupContainer.children.length === 0) {
          var section = groupContainer.parentNode;
          self.callLogContainer.removeChild(section);
        }
        if (currentItemId == nextItemID) {
          if (NavigationMap.latestClassName !== 'log-item') {
            self.renderEmptyCallLog(true);
          } else {
            self.renderEmptyCallLog();
          }
        } else {
          NavigationMap.reset(NavigationMap.latestClassName, nextItemID);
        }
      }
      var toast = {
        messageL10nId: 'tcl-calllog-entry-removed-succ-notification',
        latency: 2000,
        useTransition: true
      };
      Toaster.showToast(toast);
      CallLogCache.saveFromNode('cache-list', CallLog.callLogContainer);
    };
    CallLogDBManager.deleteGroup(toDelete, null, ondeleted);
  },

  groupMenuShown: function cl_groupMenuShown() {
    var groupMenu = document.querySelector('.group-menu.visible');
    if (groupMenu) {
      return true;
    } else {
      return false;
    }
  },

  isConfirmDialogHelperShown: function cl_isConfirmDialogHelperShown() {
    var confirmDialogHelper = document.querySelector('gaia-confirm');
    return confirmDialogHelper ? true : false;
  }
};

navigator.mozContacts.oncontactchange = function oncontactchange(event) {
  var reason = event.reason;
  var options = {
    filterBy: ['id'],
    filterOp: 'equals',
    filterValue: event.contactID
  };

  /* FIXME: We should use the contact information (id, phone number, etc...) to
   * reduce the number of elements we try to update. */

  if (reason === 'remove') {
    CallLog.updateListWithContactInfo('remove', event.contactID);
    return;
  }

  var request = navigator.mozContacts.find(options);
  request.onsuccess = function contactRetrieved(e) {
    if (!e.target.result || e.target.result.length === 0) {
      console.warn('Call log: No Contact Found: ', event.contactID);
      return;
    }

    var contact = e.target.result[0];
    if (!fb.isFbContact(contact)) {
      CallLog.updateListWithContactInfo(reason, event.contactID);
      return;
    }

    var fbReq = fb.getData(contact);
    fbReq.onsuccess = function fbContactSuccess() {
      CallLog.updateListWithContactInfo(reason, event.contactID);
    };
    fbReq.onerror = function fbContactError() {
      console.error('Error while querying FB: ', fbReq.error.name);
      CallLog.updateListWithContactInfo(reason, event.contactID);
    };
  };

  request.onerror = function errorHandler(e) {
    console.error('Error retrieving contact by ID ' + event.contactID);
  };
};
