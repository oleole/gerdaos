'use strict';
(function(exports) {

  const bMsgObjHeader = 'BEGIN:BMSG\n';
  const bMsgObjTailer = 'END:BMSG\n';
  const bInBoxFolder = 'TELECOM/MSG/INBOX\n';
  const bOutBoxFolder = 'TELECOM/MSG/OUTBOX\n';
  const vCardHeader = 'BEGIN:VCARD\n';
  const vCardTailer = 'END:VCARD\n';

  const MAP_LISTING_HEAD = '<MAP-msg-listing version = "1.0">\n';
  const MAP_LISTING_FOOT = '</MAP-msg-listing>\n';

  const MAP_REPORT_HEAD = '<MAP-event-report version = "1.0">\n';
  const MAP_REPORT_FOOT = '</MAP-event-report>\n';
  const MAP_REPORT_TYPE = {
    newMessage: '"NewMessage"',
    deliverySuccess: '"DeliverySuccess"',
    sendingSuccess: '"SendingSuccess"',
    deliveryFailure: '"DeliveryFailure"',
    sendingFailure: '"SendingFailure"',
    memoryFull: '"MemoryFull"',
    memoryAvailable: '"MemoryAvailable"',
    messageDeleted: '"MessageDeleted"',
    messageShift: '"MessageShift"'
  };
  const MAP_REPORT_FOLDER = {
    inbox: '"TELECOM/MSG/INBOX"',
    outbox: '"TELECOM/MSG/OUTBOX"'
  };
  const MAP_REPORT_MSG_TYPE_GSM = 'SMS_GSM';
  const MAP_REPORT_MSG_TYPE_CDMA = 'SMS_CDMA';

  function convert2Template (msg) {
    return `<msg handle = "${msg.id}" subject = "${msg.subject}" ` +
           `datetime = "${msg.datetime}" ` +
           `sender_name = "${msg.sender_name}" ` +
           `sender_addressing = "${msg.sender_addressing}" ` +
           `recipient_name = "${msg.recipient_name}" ` +
           `recipient_addressing = "${msg.recipient_addressing}" ` +
           `type = "${msg.type}" ` +
           `size = "${msg.size}" ` +
           `text = "${msg.text}" ` +
           `attachment_size = "${msg.attachment_size}" ` +
           `priority = "${msg.priority}" ` +
           `read = "${msg.read}" ` +
           `sent = "${msg.sent}" ` +
           `protected = "${msg.protected}"/>\n`;
  }

  function reformatXMLMsg(messages) {
    var data = '';
    var unreadFlag = false;

    function generateDate(ts) {
      return new Date(ts).toISOString().replace(/[-:]/ig, '').split('.')[0];
    }

    function getOwnerInfo() {
      return {
        name: 'local',
        addressing: '000000123'
      };
    }

    var currentMsgType = currentNetworkType();
    messages.forEach((item) => {
      var msgRecord = item;
      var msgTemplate = {
        id: msgRecord.id,
        priority: 'no',
        datetime: generateDate(msgRecord.timestamp),
        text: 'yes',
        read: msgRecord.read ? 'yes' : 'no',
        protected: 'no'
      };

      if (msgRecord.type === 'sms') {
        msgTemplate.type = currentMsgType;
        msgTemplate.subject = msgRecord.body.substring(0);
        msgTemplate.size = msgRecord.body.length;
        msgTemplate.attachment_size = 0;
      } else if (msgRecord.type === 'mms') {
        msgTemplate.type = 'MMS';
        msgTemplate.subject = '',
        msgTemplate.size = 0;
        msgTemplate.attachment_size = 0;
      }

      var ownInfo = getOwnerInfo();

      switch(msgRecord.delivery) {
        case 'sent':
          msgTemplate.sender_name = ownInfo.name;
          msgTemplate.sender_addressing = ownInfo.addressing;
          msgTemplate.recipient_name = msgRecord.receiver;
          msgTemplate.recipient_addressing = msgRecord.receiver;
          msgTemplate.sent = 'yes';
          break;
        case 'received':
          msgTemplate.sender_name = msgRecord.sender;
          msgTemplate.sender_addressing = msgRecord.sender;
          msgTemplate.recipient_name = ownInfo.name;
          msgTemplate.recipient_addressing = ownInfo.addressing;
          msgTemplate.sent = 'no';
          break;
        case 'sending':
          msgTemplate.sender_name = ownInfo.name;
          msgTemplate.sender_addressing = ownInfo.addressing;
          msgTemplate.recipient_name = msgRecord.receiver;
          msgTemplate.recipient_addressing = msgRecord.receiver;
          msgTemplate.sent = 'no';
          break;
        case 'error':
          console.error('Error when deliverting.');
          return;
        default:
          console.error('Unknown delivery status.');
          return;
      }
      data += convert2Template(msgTemplate);
      if (!msgRecord.read) {
         unreadFlag = true;
      }
    });

    return {
      size: messages.length,
      unreadFlag: unreadFlag,
      xml: MAP_LISTING_HEAD + data + MAP_LISTING_FOOT
    };
  }

  function reformatBMessage(item) {
    var bVerProp = 'VERSION:1.0\n';
    var bTypeProp = 'TYPE:' + currentNetworkType() + '\n';
    var bStatusProp = (item.read === true) ?
      'STATUS:READ\n' : 'STATUS:UNREAD\n';
    var bFolderPropContent = (item.delivery === 'received') ?
      bInBoxFolder : bOutBoxFolder;
    var bFolderProp = 'FOLDER:' + bFolderPropContent;
    var bMsgProp = bVerProp + bStatusProp + bTypeProp + bFolderProp;

    var vCardVer = 'VERSION:2.1\n';
    var vCardNameContent = (!item.vCard) ?
      'UNKNOW\n' : (item.vCard[0].name + '\n');
    var vCardTelContent = (item.delivery === 'received') ?
      (item.sender + '\n') : (item.receiver + '\n');
    var vCardName = 'N:' + vCardNameContent;
    var vCardTel = 'TEL:' + vCardTelContent;
    var vCard = vCardHeader + vCardVer + vCardName + vCardTel + vCardTailer;

    var bMsgBodyContent = 'BEGIN:MSG\n' + item.body + '\n' + 'END:MSG\n';
    var bMsgBodyProp = 'ENCODING:C-8BIT\n' + 'LENGTH:' +
      (item.body.length + '\n');
    var bMsgContent = 'BEGIN:BBODY\n' + bMsgBodyProp +
      bMsgBodyContent + 'END:BBODY\n';

    var bMsgEnv = 'BEGIN:BENV\n' + bMsgContent + 'END:BENV\n';
    var bMsgObj = bMsgObjHeader + bMsgProp + vCard + bMsgEnv + bMsgObjTailer;

    return bMsgObj;
  }

  function reformatEventObj(item) {
    var objHeader = '<event ';
    var objTail = '/>\n';
    var objType  = 'type = ' + MAP_REPORT_TYPE[item.type] + ' ';
    var objHandle = 'handle = ' + '"' + item.handle + '" ';
    var objFolder = 'folder = ' + MAP_REPORT_FOLDER[item.folder] + ' ';
    var objMsgType = 'msg_type = ' + currentNetworkType() + ' ';
    var eventObj = objHeader + objType + objHandle + objFolder +
      objMsgType + objTail;

    return MAP_REPORT_HEAD + eventObj + MAP_REPORT_FOOT;
  }

  function currentNetworkType() {
    var conns = window.navigator.mozMobileConnections;
    if (!conns || conns.length < 1) {
      return MAP_REPORT_MSG_TYPE_GSM;
    } else {
      var type = conns[0].voice.type;
      return isGSMFamily(type) ? MAP_REPORT_MSG_TYPE_GSM : MAP_REPORT_MSG_TYPE_CDMA;
    }
  }

  function isGSMFamily(type) {
    switch (type) {
      case 'is95a':
      case 'is95b':
      case '1xrtt':
      case 'evdo0':
      case 'evdoa':
      case 'evdob':
      case 'ehrpd':
        return false;
      default:
        return true;
    }
  }

  exports.reformatEventObj = reformatEventObj;
  exports.reformatXMLMsg = reformatXMLMsg;
  exports.reformatBMessage = reformatBMessage;
  exports._convert2Template = convert2Template;
}(window));
