/**
 * Created by tclxa on 4/13/15.
 */
'use strict';
function addfocuslist(){
  window.setTimeout(function() {
    var focusElements = document.querySelectorAll('[data-type="focusEnabled"]');
    debug('gaolu addfocuslist focusElements = ' + focusElements.toString());
    for(var item in focusElements) {
      if('object' == typeof(focusElements[item])) {
        focusElements[item].jrdFocus = true;
        focusElements[item].tabIndex = 1;
        debug('gaolu addfocuslist ' + item + ' = ' + focusElements[item].id);
        //focusElements[0].focus();
      }
    }
      $('menuItem-wifi').focus();
      selectBackFocus();
  }, 200);
}


var selectBackFocus = function() {
    var selectElements = document.getElementsByTagName("select");
    for (var i = 0, paragraph; paragraph = selectElements[i]; i++) {
        paragraph.addEventListener('blur' , function() {
            this.parentNode.focus();
        });
    }
}

var focusTopBottom = function(event) {
    if('toppanel' == document.activeElement.id){
        switch (event.key) {
            case 'Up':
            case 'ArrowUp':
                $('menuItem-attachInfomation').focus();
                break;
            case "Down":
            case "ArrowDown":
                $('menuItem-wifi').focus();
                break;
        }
    }
}


/*add start by TCL_cuisx*/
var removeFocusNodeSelectedStyle = function() {
  var arr = document.querySelectorAll('.inputSelected');
  if (arr.length && arr.length > 0) {
    for(var i = 0; i < arr.length; i++) {
      arr[i].classList.remove('inputSelected');
    }
  }
};
var addFocusNodeSelectedStyle = function(currentNode, isArrowDownKey) {
  if(currentNode.tagName != 'INPUT') {
    removeFocusNodeSelectedStyle();
    return;
  }
  removeFocusNodeSelectedStyle();
  if (isArrowDownKey) {
    currentNode = currentNode.nextSibling;
  } else {
    currentNode = currentNode.previousSibling;
  }
  currentNode.parentNode.classList.add('inputSelected');
};

var JudgeCurrentGetFocusNode = function(event) {
  var currentNode = document.activeElement;
  switch (event.key) {
    case 'Up':
    case 'ArrowUp':
      addFocusNodeSelectedStyle(currentNode, false);
      break;
    case "Down":
    case "ArrowDown":
      addFocusNodeSelectedStyle(currentNode, true);
      break;
  }
    focusTopBottom(event);
};
/*add end by TCL_cuisx*/

window.addEventListener('keydown',
  function initaddEventListerner(e){
    /*add start by TCL_cuisx*/
    setTimeout(function() {
      JudgeCurrentGetFocusNode(e);
    });
    /*add end by TCL_cuisx*/
    var cmd = ConvertToCmd(e.key);
    debug('gaolu jrdlog e.key = ' + e.key);
    if (e.key && ('Enter' == e.key)) {
      debug('gaolu 1 cmd = ' + cmd);
      if ('jrdcmOk' == cmd || 'jrdcmYes' == cmd) {
        var el = document.activeElement;
        if (el) {
          el.click();
          el.focus();
          debug('gaolu jrdlog el.name = ' + el.id);
            e.preventDefault();
        }
      }
    }

    else if (e.key && ('AcaSoftLeft' == e.key)) {
      pageBack();
      e.preventDefault();
    }

    else if(e.key && ('BrowserBack' == e.key || 'Backspace' == e.key)) {
    /*  debug('gaolu 2 cmd = ' + cmd);
      if ('jrdcmNo' == cmd) {
        var rootsection = document.getElementById('root');
        var ifroot = rootsection.style.display;
        debug('gaolu 2 ifroot = ' + ifroot);
        if ('none' != ifroot) {
          debug('gaolu 2 click home button ');
          window.close();
        }
      } */
      var el = document.activeElement;
      dump("gaoguang, el.id = " + el.id + "  el.tagName =" + el.tagName + "el.type = " + el.type );
      if(el != null && el.tagName == 'INPUT' && el.type != 'checkbox') {
        var str = el.value;
        dump("gaoguang str = " + str);
        if(str != "") {
          el.value = str.substr(0, str.length -1);
          return;
        }
      }

      navigator.mozApps.getSelf().onsuccess = function () {
        var app = this.result;
        if (!app.connect) {
          // in such case we can't use IAC
          debug('Can not initialise IAC');
          return;
        }
        app.connect('EndKeyHandled').then(function (ports) {
          ports.forEach(function (port) {
            port.postMessage('GoHome');
          });
        }, function _onConnectReject(data) {
          debug('Connection reject' + data);
        });
      }; 
   
    }
    else if (e.key && ('EndCall' == e.key)) {

    }
  });
addfocuslist();
