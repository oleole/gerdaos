'use strict';
function debug(msg) {
 dump('-*-*engmode-hwtest.js -*-* ' + msg);
}
function $(id) {
  return document.getElementById(id);
}

//add by qinghao.wu for PR1040684 start
var _ssidData = "";
var _ssidIntervalLoop;
var _sdcard;
//add by qinghao.wu for PR1040684 end

//BEGIN t2m_zzh PR1043289 GPSOTA
var _gpsData = '';
var _gpsDataInterval;
var _geolocationId;
var _getlocationFlag;
//END t2m_zzh GPSOTA
var toStopGPS = false;
//add by qinghao.wu for pr1086827 start
var _acceX = document.getElementById('gsensor_x');
var _acceY = document.getElementById('gsensor_y');
var _acceZ = document.getElementById('gsensor_z');
//add by qinghao.wu for pr1086827 end

var EngmodeExHwtest = {
  init: function engmodeExHwTestInit() {
    var nfcStop = navigator.engmodeExtension.fileReadLE('nfc_stop');
    dump('lx:nfcStop ' + nfcStop + '\n');
    var listItem = document.querySelector('#root ul');
    var nfcItem = $('nfcItem');
    if (nfcStop == '' || nfcStop == null) {
      listItem.removeChild(nfcItem);
    }

    var bgsvStop =
      navigator.engmodeExtension.checkIsFileExist('/storage/sdcard1/bgservice1');
    dump('lx:bgsvStop ' + bgsvStop + '\n');
    bgsvStop.onsuccess = function(e){
      if ('NO_EXIST' == e.target.result){
        var listItem = document.querySelector('#root ul');
        var bgsvItem = $('logcatItem');
        listItem.removeChild(bgsvItem);
      }
    }
      this.initWifiTxRateOptions();
    //this.pageChangeHandler();
    this.fireEvent();
    _sdcard = navigator.getDeviceStorage('sdcard');//add by qinghao.wu for PR1040684
  },
  pageChangeHandler: function engmodeExHwTestPageChangeHandler() {
    this.initWifiTxRateOptions();
    var pageLinks = document.getElementsByTagName('a');
    for (var i = 0; i < pageLinks.length; i++) {
      var link = pageLinks[i];
      link.addEventListener('click', function(event) { //modify start by ENGMODE_cuisx
        var url = this.href;
        var index = url.indexOf('#');
        var sectionId = url.substr(index + 1);
        debug('lx: sectionId ' + sectionId + '\n');
        //if(sectionId=="wifiTx") {
        switch (sectionId) {
          case 'wifiTx':
          case 'wifiRx':
            if (navigator.engmodeExtension) {
              var engmodeEx = navigator.engmodeExtension;
              var parmArray = new Array();
              parmArray.push('wifitest');
              parmArray.push('power');
              parmArray.push('/data/testbox_log/wifitext.txt');
              var initRequest = engmodeEx.execCmdLE(parmArray, 3);
            }
            break;
          case 'bluetoothTx':
            var initCommand = '/system/bin/btTx_start' +
              ' > /data/testbox_log/btTx_start.txt';
	          if (navigator.engmodeExtension) {
              var engmodeEx = navigator.engmodeExtension;
              debug('lx: bluetoothTx  initCommand ' + initCommand + '\n');
              var parmArray = new Array();
              parmArray.push('btTx_start');
              parmArray.push('');
              parmArray.push('/data/testbox_log/btTx_start.txt');
              var initRequest = engmodeEx.execCmdLE(parmArray, 3);
              }
            break;

          case 'nfc':
            if (navigator.engmodeExtension) {
              var engmodeEx = navigator.engmodeExtension;
              var parmArray = new Array();
              parmArray.push('nfc_stop');
              parmArray.push('');
              parmArray.push('/data/testbox_log/nfc_stop.txt');
              var initRequest = engmodeEx.execCmdLE(parmArray, 3);
            }
            break;

          default :
            break;
        }
        //}
        var sections = document.getElementsByTagName('section');
        for (var j = 0; j < sections.length; j++) {
          var section = sections[j];
          /*add start by ENGMODE_cuisx*/
          var previousWillGetFocusNode;
          var nextWillGetFocusNode;
          var nodeStyle = window.getComputedStyle(section);
          if (nodeStyle.display === 'block'){
            var nodeHash = '#' + section.id;
            if (event.target.firstChild.nodeType === Node.ELEMENT_NODE && link.childNodes[0].classList.contains('icon-back')) {
              var previousWillGetFocusParentNode = document.querySelector('[href="' + nodeHash + '"]');
              var previousFocusElements = previousWillGetFocusParentNode.querySelectorAll('[data-type="focusEnabled"]');
              if (previousFocusElements && previousFocusElements.length > 0) {
                previousWillGetFocusNode = previousFocusElements[0];
              } else {
                previousWillGetFocusNode = previousWillGetFocusParentNode;
              }
              if (previousWillGetFocusNode) {
                window.setTimeout(function() {
                  previousWillGetFocusNode.focus();
                }, 200);
              }
            } else {
              if (sectionId === 'root') {
                return;
              }
              var nextWillGetFocusParentNode = document.querySelector('#' + sectionId);
              var nextFocusElements = nextWillGetFocusParentNode.querySelectorAll('[data-type="focusEnabled"]');
              if (nextFocusElements && nextFocusElements.length > 0) {
                nextWillGetFocusNode = nextFocusElements[0];
              } else {
                nextWillGetFocusNode = nextWillGetFocusParentNode;
              }

              if (nextWillGetFocusNode) {
                window.setTimeout(function() {
                  nextWillGetFocusNode.focus();
                }, 200);
              }
            }
          }
          /*add end by ENGMODE_cuisx*/

          if (section.id != sectionId) {
            section.style.display = 'none';
          } else {
            section.style.display = 'block';
          }
        }
      });
    }
  },

  //add by qinghao.wu for PR1040684 start
  getSsidData: function getSsidData() {
    var time = new Date();
    var networkReqSsid = navigator.mozWifiManager.connection.network.ssid;
    var networkStrength = navigator.mozWifiManager.connectionInformation.signalStrength;
    _ssidData += time.getMonth() + "-" + time.getDay() + "\t"
              + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds()
              + "\t" +networkReqSsid+ "\tff:ff:ff:ff:ff:ff\t" +networkStrength + '\n';
    dump("get data is "+_ssidData);
  },

  ssidInterval: function ssidInterval() {
    _ssidIntervalLoop = setInterval(this.getSsidData, 1000);
  },
  //add by qinghao.wu for PR1040684 end

  //add by qinghao.wu for pr1086827 start
  startGsensorTest: function startGsensorTest(e) {
    dump("qinghao.wu the xxxxx is "+e.accelerationIncludingGravity.x);
    _acceX.innerHTML = e.accelerationIncludingGravity.x;
    _acceY.innerHTML = e.accelerationIncludingGravity.y;
    _acceZ.innerHTML = e.accelerationIncludingGravity.z;
  },
  //add by qinghao.wu for pr1086827 end

  //BEGIN t2m_zzh PR1043289 GPSOTA
  getGpsLocation: function getGpsLocation() {
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    function success(pos) {
      var crd = pos.coords;
      dump('zzh Latitude : ' + crd.latitude);
      dump('zzh Longitude: ' + crd.longitude);
      _getlocationFlag = true;
      var location ='Lat:'+ (crd.latitude + '').substr(0, 7) + '\t'
                   + 'Long:'+ (crd.longitude + '').substr(0, 7) ;
      $('gpsOtaLocacionInfo').innerHTML = location;
    }

    function error(err) {
      dump('gpsOta ERROR(' + err.code + '): ' + err.message);
    }

    function testGPSLocation() {
      _geolocationId = navigator.geolocation.watchPosition(success, error, options);
    }

    function switchGeolocationStatus() {
      var request = window.navigator.mozSettings.createLock().set({'geolocation.enabled' : true});
      request.onsuccess = function() {
        dump('gpsOta switchGeolocationStatus success');
        window.setTimeout(testGPSLocation, 2000);
      };
    }

    var request = window.navigator.mozSettings.createLock().get('geolocation.enabled');
    request.onsuccess = function() {
      if (true == request.result['geolocation.enabled']) {
        testGPSLocation();
        dump('gpsOta geolocation.enabled');
      }else {
        switchGeolocationStatus();
      }
    };
    request.onerror = function() {
      dump('gpsOta Failed to read geolocation enabled value',req.error.name);
    };
  },

  getGpsData: function getGpsData() {
    var svString = null;
    var svInfo = null;
    var prnAndsnr = "";
    var locationString = null;

    if(navigator.engmodeExtension){
      svString = navigator.engmodeExtension.fileReadLE('GPSif');
    } else {
      svString = '';
    }

    if (!svString || svString === '') {
      return;
    }

    dump('gpsOta svString:' + svString);
    svInfo = JSON.parse(svString);

    dump('gpsOta sv num:' + svInfo.num);

    if (svInfo.num === 0) {
      $('gpsOtaSatelliteInfo').textContent = svInfo.num + ' satellite(s)';
    } else if (svInfo.num > 0) {
      $('gpsOtaSatelliteInfo').textContent = svInfo.num + ' satellite(s)';
      for (var i in svInfo.gps) {
        var prn = svInfo.gps[i].prn;
        var snr = svInfo.gps[i].snr;
         if (!isNaN(prn) && !isNaN(snr)) {
           $('gpsOtaSatelliteInfo').innerHTML += '</br>' +
                                       'prn: ' + prn +
                                       ', snr: ' + snr;
        }
        prnAndsnr += "\t" + prn + "#" + snr;
      }
    }

    function pad(num,n){
      var len = num.toString().length;
      while(len<n){
        num = '0' + num;
        len++;
      }
      return num;
    }

    var date = new Date();
    var systemTime = pad(date.getHours(),2) + ':' + pad(date.getMinutes(),2) + ':' + pad(date.getSeconds(),2);
    var utcTime = pad(date.getUTCHours(),2) + ':' + pad(date.getUTCMinutes(),2) + ':' + pad(date.getUTCSeconds(),2)+ ":" + "0";
    var smsInfo = 'NO SMS';
    var callInfo = 'NO CALL';
    _gpsData += systemTime + '\t' + utcTime + '\t' + svInfo.num + '\t' + smsInfo + '\t' + callInfo + prnAndsnr + '\n';
    dump("gpsOta data is "+_gpsData);
  },
  //END t2m_zzh GPSOTA

  fireEvent: function engmodeExHwTestFireEvent() {
    //window.navigator.mozSettings.createLock().set({
      //'screen.timeout' : 9999999
    //});
    if (navigator.requestWakeLock)
    {
      navigator.requestWakeLock('screen');
    }

    window.navigator.mozSettings.createLock().set({
      'devtools.debugger.remote-enabled' : true
    });
    //Delete by dapeng.wang@tcl.com for defect1216 begin
    //window.navigator.mozSettings.createLock().set({
    //  'ril.data.enabled' : true
    //});
    //Delete by dapeng.wang@tcl.com for defect1216 end

    var _self = this;
//    $('wifi-back').onclick = function() {
//      var stopCommand = '/system/bin/wifitest tx stop' +
//        ' > /data/testbox_log/wifitext.txt';
//      if (navigator.engmodeExExtension) {
//        var engmodeEx = navigator.engmodeExtension;
//        debug('lx: wifi-back ' + stopCommand + '\n');
//
//        var parmArray = new Array();
//        parmArray.push('wifitest');
//        parmArray.push('tx stop ');
//        parmArray.push('/data/testbox_log/wifitextstop.txt');
//        var request = engmodeEx.execCmdLE(parmArray, 3);
//      }
//    };
    $('wifiRateType').addEventListener('change', function() {
      _self.initWifiTxRateOptions();
    });
    $('wifiTxRun').addEventListener('click', function() {
      this.disabled = true;
      var channelIndex = $('channelSelect').selectedIndex;
      var channel = $('channelSelect').options[channelIndex].value;
      var powderIndex = $('txPowerSelect').selectedIndex;
      var txPower = $('txPowerSelect').options[powderIndex].value;
      var protocolIndex = $('wifiRateType').selectedIndex;
      var protocolType = $('wifiRateType').options[protocolIndex].value;
      var rateIndex = $('wifiRate').selectedIndex;
      var wifiRate = $('wifiRate').options[rateIndex].value;
      debug('lx:wifiRate ' + wifiRate);
      var runCommand = _self.getTxWifiRunCommand(protocolType,
        channel, txPower, wifiRate);
      $('wifiTxStop').disabled = false;
      window.setTimeout(function() {$('wifiTxStop').focus(); }, 100);

      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        var parmArray = new Array();
        parmArray.push('wifitest');
        parmArray.push('tx cmd ' + runCommand);
        parmArray.push('/data/testbox_log/wifitest.txt');
        var request = engmodeEx.execCmdLE(parmArray, 3);
      }
    });
    $('wifiTxStop').addEventListener('click', function() {
      this.disabled = true;
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        var parmArray = new Array();
        parmArray.push('wifitest');
        parmArray.push('tx stop');
        parmArray.push('/data/testbox_log/wifitext_stop.txt');
        var request = engmodeEx.execCmdLE(parmArray, 3);
      }
      $('wifiTxRun').disabled = false;
      window.setTimeout(function() {$('wifiTxRun').focus(); }, 100);
    });

    $('wifiRxRun').addEventListener('click', function() {
      this.disabled = true;
      var channelIndex = $('rxChannelSelect').selectedIndex;
      var channel = $('rxChannelSelect').options[channelIndex].value;
      var rxRun = '/system/bin/wifitest tx cmd ' + channel;
      $('wifiRxStop').disabled = false;
      window.setTimeout(function() {$('wifiRxStop').focus(); }, 100);
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        debug('lx: runCommand ' + rxRun + '\n');

        var parmArray = new Array();
        parmArray.push('wifitest');
        parmArray.push('tx cmd ' + channel);
        var request = engmodeEx.execCmdLE(parmArray, 2);

      }
    });
    $('wifiRxStop').addEventListener('click', function() {
      this.disabled = true;
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        var parmArray = new Array();
        parmArray.push('wifitest');
        parmArray.push('tx stop');
        parmArray.push('/data/testbox_log/wifitext.txt');
        var request = engmodeEx.execCmdLE(parmArray, 3);
      }
      $('wifiRxRun').disabled = false;
      window.setTimeout(function() {$('wifiRxRun').focus(); }, 100);
    });

     //add by qinghao.wu for PR1040684 start
    $('wifiSsidRun').addEventListener('click', function() {
      this.disabled = true;
      dump('wifiSsidRun start');

      if(_sdcard) {
        var deleteChamberReq = _sdcard.delete('wifiOTA/wifiota.log');
        deleteChamberReq.onsuccess = function() {
           dump('File deleted success');
          console.log('chamberTest File deleted');
        };

        deleteChamberReq.onerror = function() {
          dump('File deleted error'+ this.error);
          console.log('Unable to delete the file: ' + this.error);
        };
      } else {
        dump('_sdcard is error ');
      }

      EngmodeExHwtest.ssidInterval();

      $('wifiSsidStop').disabled = false;
      window.setTimeout(function() {$('wifiSsidStop').focus(); }, 100);
    });

     $('wifiSsidStop').addEventListener('click', function() {
      this.disabled = true;
      clearInterval(_ssidIntervalLoop);
      dump('wifiSsidStop start');
      var chamberFile = new Blob([_ssidData.toString()], {type: 'text/plain'});
      var addChamberReq = _sdcard.addNamed(chamberFile, 'wifiOTA/wifiota.log');

      addChamberReq.onsuccess = function() {
          var chamberFileName = this.result;
          dump('addFile success  '+ chamberFileName);
          console.log('File "' + chamberFileName + '" successfully wrote on the sdcard storage area');
      };

      // An error typically occur if a file with the same name already exist
      addChamberReq.onerror = function() {
        dump('addFile onerror  '+ this.error);
          console.warn('Unable to write the chamberFile: ' + this.error);
      };

      $('wifiSsidRun').disabled = false;
      $('wifiSsidContent').innerHTML = _ssidData.split("\n").join("</br>");
      window.setTimeout(function() {$('wifiSsidRun').focus(); }, 100);
    });
    //add by qinghao.wu for PR1040684 end

    //add by qinghao.wu for PR1086827 start
    $('gsensorRun').addEventListener('click', function gsensorstart() {
        this.disabled = true;
        dump('gsensorRun start');
        window.addEventListener('devicemotion', EngmodeExHwtest.startGsensorTest);

        $('gsensorStop').disabled = false;
        window.setTimeout(function() {$('gsensorStop').focus(); }, 100);
    });

    $('gsensorStop').addEventListener('click', function gsensorend() {
        this.disabled = true;
        dump('gsensorStop start');
        window.removeEventListener('devicemotion', EngmodeExHwtest.startGsensorTest);

        $('gsensorRun').disabled = false;
        window.setTimeout(function() {$('gsensorRun').focus(); }, 100);
    });
    //add by qinghao.wu for PR1086827 end

    $('btTxHop').addEventListener('change', function() {
      var btTxHopIndex = $('btTxHop').selectedIndex;
      var btTxHop = $('btTxHop').options[btTxHopIndex].value;
      dump('lx: btTxHopIndex ' + btTxHopIndex + 'btTxHop value ' + btTxHop + '\n');
      if (btTxHop == '0x01') {
        dump('lx:add disable');
        $('btChannelItem').classList.add('disable');
       // $('btChannel').classList.add('disable');
        $('btTxChannel').selectedIndex = 0;
      } else {
        if ($('btChannelItem').classList.contains('disable'))
          $('btChannelItem').classList.remove('disable');
        dump('lx:remove disable');
//        if($('btChannel').classList.contains('disable'))
//          $('btChannel').classList.remove('disable');
      }
    });
    $('btTxRun').addEventListener('click', function() {
      var patternIndex = $('btTxPattern').selectedIndex;
      var btTxPattern = $('btTxPattern').options[patternIndex].value;
      var channelIndex = $('btTxChannel').selectedIndex;
      var btTxChannel = $('btTxChannel').options[channelIndex].value;
      var packet = $('btTxPacketType').selectedIndex;
      var btTxPacketType = $('btTxPacketType').options[packet].value;
      var whiteningIndex = $('btTxWhitening').selectedIndex;
      var btTxWhitening = $('btTxWhitening').options[whiteningIndex].value;
      var powerIndex = $('btTxPower').selectedIndex;
      var btTxPower = $('btTxPower').options[powerIndex].value;
      var rxGainIndex = $('btTxRxGain').selectedIndex;
      var btTxRxGain = $('btTxRxGain').options[rxGainIndex].value;
      var btTxHopIndex = $('btTxHop').selectedIndex;
      var btTxHop = $('btTxHop').options[btTxHopIndex].value;

      var txRunCommand =  btTxChannel + ' ' + btTxPattern + ' '
                          + btTxPacketType + ' ' + btTxWhitening + ' '
                          + btTxPower + ' ' + btTxRxGain + ' '
                          + btTxHop;
      this.disabled = true;
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        debug('lx: btTxRun runCommand ' + txRunCommand + '\n');

        var parmArray = new Array();
        parmArray.push('btTx_run');
        parmArray.push(txRunCommand);
        var request = engmodeEx.execCmdLE(parmArray, 2);

        request.onsuccess = function(e) {
          $('btTxRun').disabled = false;
          window.setTimeout(function() {$('btTxRun').focus(); }, 100);
          alert('success!');
        };
        request.onerror = function(e) {
          alert('error!');
          $('btTxRun').disabled = false;
          window.setTimeout(function() {$('btTxRun').focus(); }, 100);
        };
      }
    });

    // guang.gao@tcl.com  for code check , no work
    $('btRxRun').addEventListener('click', function() {
      this.disabled = true;
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;

        var parmArray = new Array();
        parmArray.push('btTx_run');
        var request = engmodeEx.execCmdLE(parmArray, 1);
        request.onsuccess = function(e) {
          $('btRxRun').disabled = false;
          window.setTimeout(function() {$('btRxRun').focus(); }, 100);
          alert('success!');
        };
        request.onerror = function(e) {
          $('btRxRun').disabled = false;
          window.setTimeout(function() {$('btRxRun').focus(); }, 100);
          alert('error!');
        };
      }
    });
// guang.gao@tcl.com  for code check , no work end
    $('btTx-back').onclick = function() {
       var btDisable = 'bttest disable';
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        debug('lx: bluetooth-back ' + btDisable + '\n');
        var parmArray = new Array();
        parmArray.push('bttestdisable');
        var request = engmodeEx.execCmdLE(parmArray, 1);
      }

    };
    $('btVTA-input').addEventListener('change', function(evt) {
      var value = evt.target.checked;
      debug(' wifiSleep value  ' + value);
      setBluetoothDiscoverable(value);

      function setBluetoothDiscoverable(visible) {
        var bluetooth = window.navigator.mozBluetooth;
        var defaultAdapter = null;
        var req = bluetooth.getDefaultAdapter();
        req.onsuccess = function bt_getAdapterSuccess() {
          defaultAdapter = req.result;
          if (defaultAdapter == null) {
            // we can do nothing without DefaultAdapter
            return;
          }
          defaultAdapter.setDiscoverable(visible);
          defaultAdapter.setDiscoverableTimeout(1000 * 60 * 60 * 2);
        };
      }
    });
		//BT RADIO TEST BEGIN
		$('btRadioTestRun').addEventListener('click', function() {
			this.disabled = true;
			$('btRadioTestStop').disabled = false;
      $('btRadioTestStop').focus();
			var bttestCommand = '/system/bin/bt_radio_run '
				+ ' > /data/testbox_log/bt_radio_run.txt';
			if (navigator.engmodeExtension) {
				var engmodeEx = navigator.engmodeExtension;
				debug('lx:btRadioTestRun runCommand ' + bttestCommand + '\n');
        var parmArray = new Array();
        parmArray.push('bt_radio_run');
        parmArray.push('');
        parmArray.push('/data/testbox_log/bt_radio_run.txt');
        var request = engmodeEx.execCmdLE(parmArray, 3);

			}
		});
		$('btRadioTestStop').addEventListener('click', function() {
			this.disabled = true;
      $('btRadioTestRun').disabled = false;
      $('btRadioTestRun').focus();
			var stopCommand = '/system/bin/bt_radio_stop';
			if (navigator.engmodeExtension) {
				var engmodeEx = navigator.engmodeExtension;
				debug('lx:btRadioTestStop stopCommand ' + stopCommand + '\n');
        var parmArray = new Array();
        parmArray.push('bt_radio_stop');
        var request = engmodeEx.execCmdLE(parmArray, 1);
			}
		});
		//BT RADIO TEST ENE

		$('gpsTest1Run').addEventListener('click', function() {
      this.disabled = true;
      var runCommand = '/system/bin/gps_test 1 0';
      $('gpsTest1Stop').disabled = false;
      window.setTimeout(function() {$('gpsTest1Stop').focus(); }, 100);
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        debug('lx:gpsTest1Run runCommand ' + runCommand + '\n');
        var parmArray = new Array();
        parmArray.push('gps_test_1_0');
        var request = engmodeEx.execCmdLE(parmArray, 1);
      }
    });
    $('gpsTest1Stop').addEventListener('click', function() {
      this.disabled = true;
      var stopCommand = '/system/bin/gps_test stop';
      if (navigator.engmodeExtension) {
        debug('lx:gpsTest1Stop stopCommand ' + stopCommand + '\n');
        //navigator.engmodeExtension.stopUniversalCommand();
        var parmArray = new Array();
        parmArray.push('gps_test');
        parmArray.push('stop');
        var request = engmodeEx.execCmdLE(parmArray, 2);
      }
      $('gpsTest1Run').disabled = false;
      window.setTimeout(function() {$('gpsTest1Run').focus(); }, 100);
    });

    var count, type;
    $('gpsTest2Cold').addEventListener('click', function() {

      count = 0;
      type = 'cold';
      console.log('total count =='+$('input_gps_test_count').value);
      if($('input_gps_test_count').value != 0 && $('input_gps_test_count').value != '')
         total = $('input_gps_test_count').value;
      runTest();
      disableButtonGPSTest2();
    });
    $('gpsTest2Hot').addEventListener('click', function() {

      count = 0;
      type = 'hot';
      console.log('total count =='+$('input_gps_test_count').value);
      if($('input_gps_test_count').value != 0 && $('input_gps_test_count').value != '')
        total = $('input_gps_test_count').value;
      runTest();
      disableButtonGPSTest2();
    });
    $('gpsTest2Warm').addEventListener('click', function() {
      count = 0;
      type = 'warm';
      console.log('total count =='+$('input_gps_test_count').value);
      if($('input_gps_test_count').value != 0 && $('input_gps_test_count').value != '')
        total = $('input_gps_test_count').value;
      runTest();
      disableButtonGPSTest2();
    });

		var AGPSwatchId;
		var AGPSStatus = $('AGPSStatus');
		var AGPSDisplay_input = $('AGPSDisplay-input');
    $('AGPSDisplay-input').addEventListener('change', function(evt) {
      var input = evt.target;
      var type = input.type;
      var id = input.id;
      var key = input.name;
      var value;

      value = input.checked;
      debug('AGPSDisplay value  ' + value);
      if (value) {
				AGPSStatus.textContent = 'AGPS Openning...!';
        if (navigator.geolocation) {
          var options = {
            enableHighAccuracy: true,
            timeout: 0,
            maximumAge: 0
          };
					var request = window.navigator.mozSettings.createLock().get('geolocation.enabled');
					request.onsuccess = function() {
						if (true == request.result['geolocation.enabled']) {
						  AGPSwatchId = navigator.geolocation.watchPosition(AGPSReqSuccess, AGPSReqerror, options);
              debug(' display AGPS \n');
						}else {
							AGPSStatus.textContent = 'failed!Check your settings!';
							AGPSDisplay_input.checked = false;
						}
					};
					request.onerror = function() {
						AGPSStatus.textContent = 'failed!Check your SW-Verison!';
						AGPSDisplay_input.checked = false;
					};
        }
      } else {
				AGPSStatus.textContent = 'AGPS Closed!';
        navigator.geolocation.clearWatch(AGPSwatchId);
      }

      function AGPSReqSuccess(pos) {
				AGPSStatus.textContent = 'AGPS Opened!';
      }
      function AGPSReqerror() {
				AGPSStatus.textContent = 'failed!Check your settings!';
				AGPSDisplay_input.checked = false;
      }

      var settings = window.navigator.mozSettings;

      if (!key || !settings || evt.type != 'change')
        return;

      var cset = {};
      cset[key] = value;
      settings.createLock().set(cset);
    });

    //BEGIN t2m_zzh PR1043289 GPSOTA
    $('gpsOtaRun').addEventListener('click', function() {
      this.disabled = true;
      dump('gpsOta start');

      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        dump('gpsOta startGpsTest');
        var parmArray = new Array();
        parmArray.push('gps_test_0_1');
        var request = engmodeEx.execCmdLE(parmArray, 1);
      }

      if(_sdcard) {
        var deleteChamberReq = _sdcard.delete('GPSOTA/gpsota.log');
        deleteChamberReq.onsuccess = function() {
          dump('File deleted success');
          console.log('chamberTest File deleted');
        };

        deleteChamberReq.onerror = function() {
          dump('File deleted error'+ this.error);
          console.log('Unable to delete the file: ' + this.error);
        };
      } else {
        dump('_sdcard is error ');
      }

      _getlocationFlag = false;
      EngmodeExHwtest.getGpsLocation();

      _gpsData = '';
      _gpsDataInterval = setInterval(EngmodeExHwtest.getGpsData, 1000);

      $('gpsOtaStop').disabled = false;
      $('gpsOtaLocacionInfo').innerHTML = "Please wait for position info...";
      $('gpsOtaSatelliteInfo').innerHTML = "Please wait for satellite info...";
      window.setTimeout(function() {$('gpsOtaStop').focus(); }, 100);
    });

    $('gpsOtaStop').addEventListener('click', function() {
      this.disabled = true;
      dump('gpsOta stop');
      if (_gpsDataInterval) {
        clearInterval(_gpsDataInterval);
        _gpsDataInterval = null;
      }
      navigator.geolocation.clearWatch(_geolocationId);

      if (navigator.engmodeExtension) {
        var parmArray = new Array();
        parmArray.push('gps_test');
        parmArray.push('stop');
        var request = engmodeEx.execCmdLE(parmArray, 2);
      }

      if (_gpsData != '') {
        var title = "SystemTime" + "\t" + "UTC" + "\t" + "sates_in_view" + "\t" + "sms" + "\t" + "call" + "\t" + "PRN#SNR" + '\n';
        var gpsInfo = title + _gpsData;
        var chamberFile = new Blob([gpsInfo.toString()], {type: 'text/plain'});
        var addChamberReq = _sdcard.addNamed(chamberFile, 'GPSOTA/gpsota.log');

        addChamberReq.onsuccess = function() {
            var chamberFileName = this.result;
            dump('gpsOta addFile success  '+ chamberFileName);
        };

        addChamberReq.onerror = function() {
          dump('gpsOta addFile onerror  '+ this.error);
        };
      } else {
        $('gpsOtaSatelliteInfo').innerHTML = "No satellite info";
      }

      if(_getlocationFlag == false){
        $('gpsOtaLocacionInfo').innerHTML = "No position info";
      }
      $('gpsOtaRun').disabled = false;
      window.setTimeout(function() {$('gpsOtaRun').focus(); }, 100);
    });
    // END t2m_zzh GPSOTA

    // BEGIN t2m_zzh PR1133022 GPSOTA test app for NB lab
    $('gpsOtaNbRun').addEventListener('click', function() {
      this.disabled = true;
      dump('gpsNbOta start');
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        dump('gpsNbOta startGpsTest');
        var parmArray = new Array();
        parmArray.push('gps_test_1_0_1');
        var request = engmodeEx.execCmdLE(parmArray, 1);
      }
      $('gpsOtaNbStop').disabled = false;
      window.setTimeout(function() {$('gpsOtaNbStop').focus(); }, 100);
    });

    $('gpsOtaNbStop').addEventListener('click', function() {
      this.disabled = true;
      dump('gpsNbOta stop');
      if (navigator.engmodeExtension) {
        var parmArray = new Array();
        parmArray.push('gps_test');
        parmArray.push('stop');
        var request = engmodeEx.execCmdLE(parmArray, 2);
      }
      $('gpsOtaNbRun').disabled = false;
      window.setTimeout(function() {$('gpsOtaNbRun').focus(); }, 100);
    });
    // END t2m_zzh GPSOTA test app for NB lab

    $('nfcEUTRun').addEventListener('click', function() {
      var index = $('nfcRunTest').selectedIndex;
      var value = $('nfcRunTest').options[index].value;
      var nfcRunCommand = '/system/bin/test_pn547  ' + value;

      this.disabled = true;
      window.setTimeout(function() {$('nfcEUTStop').focus(); }, 100);
      if (navigator.engmodeExtension) {
        var engmodeEx = navigator.engmodeExtension;
        debug('lx:nfc Run  ' + nfcRunCommand + '\n');
        var parmArray = new Array();
        parmArray.push('test_pn547');
        parmArray.push(value);
        var request = engmodeEx.execCmdLE(parmArray, 2);
      }
    });

    $('nfcEUTStop').addEventListener('click', function() {
      if (navigator.engmodeExtension) {
        debug('lx:nfcEUTStop \n');
        //navigator.engmodeExtension.stopUniversalCommand();
      }
      this.disabled = true;
      $('nfcEUTRun').disabled = false;
      window.setTimeout(function() {$('nfcEUTRun').focus(); }, 100);
    });

    function disableButtonGPSTest2() {
      $('rst_gpstest2').innerHTML = type + '  testing.....(0)';
      $('gpsTest2Cold').disabled = true;
      $('gpsTest2Warm').disabled = true;
      $('gpsTest2Hot').disabled = true;
      $('gpsTest2stop').disabled = false;
      window.setTimeout(function() {$('gps-test2-back').focus(); }, 100);
    }

    function enableButtonGPSTest2() {
      $('gpsTest2Cold').disabled = false;
      $('gpsTest2Warm').disabled = false;
      $('gpsTest2Hot').disabled = false;
      $('gpsTest2stop').disabled = true;
      window.setTimeout(function() {$('gps-test2-back').focus(); }, 100);
    }

    function closeGPS() {
      window.navigator.mozSettings.createLock().set({
        'geolocation.enabled' : false
      });
    }

    function openGPS() {
      window.navigator.mozSettings.createLock().set({
        'geolocation.enabled' : true
      });
    }

    function closeShell() {
      try {
        navigator.engmodeExtension.stopUniversalCommand();
      } catch (e) {
      }
    }

    var result = '';
    var result2 = '';
    var filename = '';
    var total = 60;
    var id;
    var geolocation = navigator.geolocation;
    var timeBegin = 0;
    var timeEnd = 0;
    var options = {
      enableHighAccuracy: true,
      timeout: 50000,
      maximumAge: 0
    };
    $('input_gps_test_count').value = '';

    function success(pos) {
      if (id != undefined) {
        if (30 <= pos.coords.accuracy) {
          return;
        }
        geolocation.clearWatch(id);
        $('gps2ResultPanel').innerHTML = '[success]clear watch:[' + id + ']</br>'
                                         + $('gps2ResultPanel').innerHTML;
      }
      var crd = pos.coords;
      timeEnd = Date.now();
      dump('lx: timeEnd ' + timeEnd + '\n');
      result = parseInt((timeEnd - timeBegin) / 1000);
      result2 = result;
      result = result + '\t' + crd.latitude + '\t' + crd.longitude + ';\n';
      dump('lx: result2 ' + result2 + '\n');
      result2 = '[' + id + ']' + result2 + '\t'
                + (crd.latitude + '').substr(0, 7) + '\t'
                + (crd.longitude + '').substr(0, 7);
      {
        var filename = '/data/testbox_log/testbox-gps2-' + type + '.log';
        var wirteCommand = 'echo \"' + result + '\" >> ' + filename;
        var request = navigator.engmodeExtension.fileWriteLE(result, filename, 'a');
      }
      $('gps2ResultPanel').innerHTML = result2 + '</br>'
                                       + $('gps2ResultPanel').innerHTML;

      Log('geolocation.watchPosition success id = ' + id
                                                  + ' result: ' + result);

      if ((count > total) || (true == toStopGPS)) {

        $('rst_gpstest2').innerHTML = 'success';
        toStopGPS = false;
        enableButtonGPSTest2();
        result += '\nTest ' + count + ' times,\nThe GPS TTFF test completed!!!!\n';
      } else {
        $('rst_gpstest2').innerHTML = type + '  testing.....(' + count + ')';
        runTest();
      }

    }

    function error(err) {
      if (id != undefined) {
        geolocation.clearWatch(id);
        $('gps2ResultPanel').innerHTML = '[error]clear watch:[' + id + ']</br>'
                                         + $('gps2ResultPanel').innerHTML;
      }
      var msg = 'undefined error';
      if (typeof err == 'object') {
        if (err.code == 1) {
          msg = 'Error:Permission denied';
        }
        if (err.code == 2) {
          msg = 'Error:Position unavailable';
        }
        if (err.code == 3) {
          msg = 'Error:Timed out';
        }
      }
      $('gps2ResultPanel').innerHTML = '[' + id + ']' + msg + '</br>'
                                       + $('gps2ResultPanel').innerHTML;

      Log('geolocation.watchPosition error id = ' + id + ' msg: ' + msg);

      if ((count > total) || (true == toStopGPS)) {

        $('rst_gpstest2').innerHTML = 'success';
        toStopGPS = false;
        enableButtonGPSTest2();

        result += '\nTest ' + count + ' times,\nThe GPS TTFF test completed!!!!\n';
      } else {
        $('rst_gpstest2').innerHTML = type + '  testing.....(' + count + ')';
        runTest();
      }
    }

    function runTest() {
      Log('runTest :' + type);
       debug('lx: Type ' + type + '\n');
      if ((count > total) || (true == toStopGPS)) {
        toStopGPS = false;
        return;
      }
      var request = window.navigator.mozSettings.createLock().set({'geolocation.enabled' : false});
      DomRequestResult(request, 'geolocation.enabled: false', function(){window.setTimeout(_settingCallback, 10000)});

      function _settingCallback() {
        if (type != 'hot') {
          var parmArray = new Array();
          parmArray.push('gps_test');
          parmArray.push(type);
          var request = navigator.engmodeExtension.execCmdLE(parmArray, 2);
          DomRequestResult(request, 'gps_test ' + type, function(){window.setTimeout(_callback, 10000)});
        }
        else {
          _callback();
        }
      }

      function _callback() {
        var request = window.navigator.mozSettings.createLock().set({'geolocation.enabled' : true});
        DomRequestResult(request, 'geolocation.enabled: true', function(){window.setTimeout(testGPS, 10000)});
      }

      function testGPS() {
        count++;
        timeBegin = Date.now();

        id = geolocation.watchPosition(success, error, options);
        Log('geolocation.watchPosition id = ' + id);
      }
    }
  },
  initWifiTxRateOptions : function initWifiTxOptions() {
    var rateIndex = $('wifiRateType').selectedIndex;
    var option = $('wifiRateType').options[rateIndex].innerHTML;
    if (option == '11b' || !option) {
      $('wifiRate').innerHTML = "<option value='0'>1Mbps</option>"
                                + "<option value='1'>2Mbps</option>"
                                + "<option value='2'>5.5Mbps</option>"
                                + "<option value='3'>11Mbps</option>";
    } else if (option == '11g') {
      $('wifiRate').innerHTML = "<option value='4'>6Mbps</option>"
                                + "<option value='5'>9Mbps</option>"
                                + "<option value='6'>12Mbps</option>"
                                + "<option value='7'>18Mbps</option>"
                                + "<option value='8'>24Mbps</option>"
                                + "<option value='9'>36Mbps</option>"
                                + "<option value='10'>48Mbps</option>"
                                + "<option value='11'>54Mbps</option>";
    } else if (option == '11n') {
      $('wifiRate').innerHTML = "<option value='12'>6.5Mbps</option>"
                                + "<option value='13'>13Mbps</option>"
                                + "<option value='14'>19.5Mbps</option>"
                                + "<option value='15'>26Mbps</option>"
                                + "<option value='16'>39Mbps</option>"
                                + "<option value='17'>52Mbps</option>"
                                + "<option value='18'>58.5Mbps</option>"
                                + "<option value='19'>65Mbps</option>";
    }
  },
  getTxWifiRunCommand : function getTxWifiRunCommand(txpro, txfreq, txpwr, txrate) {
    return txpro + ' ' + txfreq + ' ' + txpwr + ' ' + txrate ;
  },
  getTxBTRunCommand : function getTxBTRunCommand(pattern, channel, packet, whitening, power, rxGain) {
    return 'hcitool cmd 0x3F 0x0004 0x04 '
           + this.getTxBTChannel(channel)
           + pattern + ' ' + packet + ' '
           + whitening + ' ' + power + ' '
           + rxGain + ' 0x9c 0x35 0xBD 0x9c 0x35 0xBD 0x00 0x1B 0x00 0x00';
  },
  getTxBTChannel : function getTxBTChannel(channel) {
    var result = '';
    for (var i = 0; i < 5; i++) {
      result += channel;
      result += ' ';
    }
    return result;
  }
};
EngmodeExHwtest.init();


function DomRequestResult(request, data, callback) {
  request.onsuccess = function(e) {
    Log('DomRequest success: ' + data);
    if(callback){
      callback();
    }
  };
  request.onerror = function(e) {
    Log('DomRequest error: ' + data);
  }
};

var ALL_LOGS = "";
window.addEventListener('load', function onLoad() {
  Log('load');

  window.removeEventListener('load', onLoad);
  document.getElementById('gpsTest2stop').addEventListener('click', function(){
  //  var filename = '/data/testbox_log/engmodeExhwtest-gps2-' + new Date().getTime() + '.log';
  //  var wirteCommand = "echo \"" + ALL_LOGS + "\" >> " + filename;
  //
  //  ALL_LOGS = "";
  //  var request = navigator.engmodeExtension.fileWriteLE(ALL_LOGS, filename, 'a');
	//	request.onsuccess = function(e) {
	//		alert('save success');
	//	};
	//	request.onerror = function(e) {
	//		alert('save faile');
	//	}
    toStopGPS = true;
  });
});

$('menuItem-getlog').addEventListener('click', function() {

  var parmArray = new Array();
  parmArray.push('data_kaioslog_upper');
  var gLogrequest = engmodeEx.execCmdLE(parmArray, 1);
  gLogrequest.onsuccess = function() {

    var parmArray = new Array();
    parmArray.push('data_testbox_upper');
    var gengmodeLogrequest = engmodeEx.execCmdLE(parmArray, 1);
    gengmodeLogrequest.onsuccess = function() {

      var parmArray = new Array();
      parmArray.push('data_pidpptt_upper');
      var gmiscLogrequest = engmodeEx.execCmdLE(parmArray, 1);
      gmiscLogrequest.onsuccess = function() {
       // alert('you can find the log at /data/testbox_log or /data/jrdlog');
       document.querySelector('#makelog_infoPanel').innerHTML = 'you can find the log at /data/testbox_log or /data/kaioslog';
       dump('yjli kaioslog alert');
        window.setTimeout(function() {$('menuItem-getlog').focus(); }, 100);
      };
    };
  };
});
