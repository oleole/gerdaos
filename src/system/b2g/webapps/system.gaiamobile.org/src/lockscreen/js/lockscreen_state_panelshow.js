/* global Promise, LockScreenBaseState */

'use strict';

/**
 * This state would guarantee the LockScreen shows the screen.
 */
(function(exports) {

  var LockScreenStatePanelShow = function() {
    LockScreenBaseState.apply(this, arguments);
  };
  LockScreenStatePanelShow.prototype =
    Object.create(LockScreenBaseState.prototype);

  LockScreenStatePanelShow.prototype.start = function(lockScreen) {
    this.type = 'panelShow';
    this.lockScreen = lockScreen;
    return this;
  };

  LockScreenStatePanelShow.prototype.transferTo =
  function lssss_transferTo(inputs) {
    return new Promise((resolve, reject) => {
      // Copy from the original switching method.
      this.lockScreen.overlay.classList.add('no-transition');
      this.lockScreen.overlay.dataset.panel = 'main';
      this.lockScreen.overlay.dataset.passcodeStatus = '';
      this.lockScreen.overlay.classList.add('passcode-unlocking');
      resolve();
    });
  };
  exports.LockScreenStatePanelShow = LockScreenStatePanelShow;
})(window);
