define(['require','shared/toaster','modules/settings_panel','modules/settings_service','shared/settings_listener','shared/airplane_mode_helper'],function(require) {
  
  var Toaster = require('shared/toaster');
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');
  var SettingsListener = require('shared/settings_listener');
  var AirplaneModeHelper = require('shared/airplane_mode_helper');

  return function airplane_mode_settings_panel() {
    var AIRPLANEMODE_KEY = 'airplaneMode.enabled';
    var _settings = window.navigator.mozSettings;
    var _currentSettingsValue = false;
    var _airplaneModeSwitchOn;
    var _airplaneModeSwitchOff;
    var airplaneModeDesc;

    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method: function() {
            SettingsService.navigate('root');
          }
        }, {
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };
      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _updateAPMInfo(enabled) {
      _currentSettingsValue = enabled;
      _airplaneModeSwitchOn.checked = enabled;
      _airplaneModeSwitchOff.checked = !enabled;

      AirplaneModeHelper.setEnabled(enabled);
      airplaneModeDesc.setAttribute('data-l10n-id', enabled ? 'on' : 'off');
    }

    function _setAPM(evt) {
      var enabled = (evt.target.value === 'true') || false;
      if (_currentSettingsValue === enabled) {
        return;
      }

      var lock = _settings.createLock();
      var option = {};
      option[AIRPLANEMODE_KEY] = enabled;
      var req = lock.set(option);

      req.onsuccess = () => {
        var toast = {
          messageL10nId: 'changessaved',
          latency: 2000,
          useTransition: true
        };
        Toaster.showToast(toast);
        SettingsService.navigate('root');
      };

      _airplaneModeSwitchOn.checked = enabled;
      _airplaneModeSwitchOff.checked = !enabled;
      airplaneModeDesc.setAttribute('data-l10n-id', enabled ? 'on' : 'off');
      window.dispatchEvent(new CustomEvent('airplaneModeChange', {
        detail: {
          status: enabled ? 'enabling': 'disabling'
        }
      }));
    }

    return SettingsPanel({
      onInit: function(panel) {
        _airplaneModeSwitchOn =
          document.getElementById('airplane_switch_on');
        _airplaneModeSwitchOff =
          document.getElementById('airplane_switch_off');
        airplaneModeDesc = document.getElementById('airplaneMode-desc');
      },

      onBeforeShow: function() {
        _initSoftKey();

        _airplaneModeSwitchOn.addEventListener('click', _setAPM);

        _airplaneModeSwitchOff.addEventListener('click', _setAPM);

        SettingsListener.observe(AIRPLANEMODE_KEY, false, _updateAPMInfo);
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();

        _airplaneModeSwitchOn.removeEventListener('click', _setAPM);

        _airplaneModeSwitchOff.removeEventListener('click', _setAPM);
        SettingsListener.unobserve(AIRPLANEMODE_KEY, _updateAPMInfo);
      }
    });
  };
});
