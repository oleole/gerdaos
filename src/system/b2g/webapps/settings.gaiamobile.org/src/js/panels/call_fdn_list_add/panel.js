define(['require','modules/dialog_panel','modules/settings_service','modules/fdn_context','modules/fdn_dialog'],function(require) {
  

  var DialogPanel = require('modules/dialog_panel');
  var SettingsService = require('modules/settings_service');
  var FdnContext = require('modules/fdn_context');
  var FdnDialog = require('modules/fdn_dialog');

  return function ctor_call_fdn_list_add() {
    return DialogPanel({
      onInit: function(panel) {
        this._submitable = false;
        this._mode = 'add';
        this._panel = panel;

        this._elements = {};
        this._elements.fdnNameInput = panel.querySelector('.fdnContact-name');
        this._elements.fdnNumberInput =
          panel.querySelector('.fdnContact-number');
        this._elements.fdnContactTitle =
          panel.querySelector('.fdnContact-title');
        this.gaiaHeader = document.querySelector('#simpin2-dialog gaia-header');
        this._boundInputsChange = this._checkContactInputs.bind(this);
      },

      _initSoftkey: function(saveSoftkeyEnable) {
        var self = this;
        if (saveSoftkeyEnable) {
          var params = {
            menuClassName: 'menu-button',
            header: {
              l10nId: 'message'
            },
            items: [{
              name: 'Cancel',
              l10nId: 'cancel',
              priority: 1,
              method: function() {
                SettingsService.navigate('call-fdnList');
              }
            }, {
              name: 'Save',
              l10nId: 'save',
              priority: 2,
              method: function() {
                self.gaiaHeader.dataset.href = '#call-fdnList';
                self._updateContact('add', self._elements.fdnNameInput.value,
                  self._elements.fdnNumberInput.value);
              }
            }, {
              name: 'Contact',
              l10nId: 'fdnContact',
              priority: 3,
              method: function() {
                var activity = new MozActivity({
                  name: 'pick',
                  data: {
                    type: 'webcontacts/contact',
                    params: { }
                  }
                });
                activity.onsuccess = function() {
                  var result = activity.result;
                  var name = activity.result.contact.name;
                  var number = activity.result.contact.tel[0].value;
                  self._elements.fdnNameInput.value = name;
                  self._elements.fdnNumberInput.value = number;
                  self._checkContactInputs();
                };
              }
            }]
          };
        } else {
          var params = {
            menuClassName: 'menu-button',
            header: {
              l10nId: 'message'
            },
            items: [{
              name: 'Cancel',
              l10nId: 'cancel',
              priority: 1,
              method: function() {
                SettingsService.navigate('call-fdnList');
              }
            }, {
              name: 'Contact',
              l10nId: 'fdnContact',
              priority: 3,
              method: function() {
                var activity = new MozActivity({
                  name: 'pick',
                  data: {
                    type: 'webcontacts/contact',
                    params: { }
                  }
                });
                activity.onsuccess = function() {
                  var result = activity.result;
                  var name = activity.result.contact.name;
                  var number = activity.result.contact.tel[0].value;
                  self._elements.fdnNameInput.value = name;
                  self._elements.fdnNumberInput.value = number;
                  self._checkContactInputs();
                };
              }
            }]
          };
        }
        SettingsSoftkey.init(params);
        SettingsSoftkey.show();
      },

      onBeforeShow: function(panel, options) {
        this._currentContact = options.contact;
        this._mode = options.mode || 'add';
        this._elements.fdnNameInput.value = options.name || '';
        this._elements.fdnNumberInput.value = options.number || '';
        if (this._mode === 'add') {
          this._elements.fdnContactTitle.setAttribute('data-l10n-id',
            'fdnAction-add');
        } else {
          this._elements.fdnContactTitle.setAttribute('data-l10n-id',
            'fdnAction-edit-header');
        }

        window.addEventListener('keydown', this._keydownHandler);
        this._elements.fdnNameInput.addEventListener('input', this._boundInputsChange);
        this._elements.fdnNumberInput.addEventListener('input', this._boundInputsChange);
        this._checkContactInputs();
      },

      onShow: function() {
        this._elements.fdnNameInput.focus();
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
        window.removeEventListener('keydown', this._keydownHandler);
        this._elements.fdnNameInput.removeEventListener('input', this._boundInputsChange);
        this._elements.fdnNumberInput.removeEventListener('input', this._boundInputsChange);
      },

      _keydownHandler: function(evt) {
        switch (evt.key) {
          case 'ArrowUp':
          case 'ArrowDown':
          case 'Enter':
            var input = document.querySelector('#call-fdnList-add li.focus input');
            if (input) {
              input.focus();
            }
            break;
          default:
            break;
        }
      },

      onSubmit: function() {
        if (this._submitable) {
          return Promise.resolve({
            name: this._elements.fdnNameInput.value,
            number: this._elements.fdnNumberInput.value
          });
        } else {
          return Promise.reject();
        }
      },

      /**
       * update information on each contact item based on passed in parameters.
       *
       * @type {Function}
       * @param {String} action
       * @param {Object} options
       * @return {Promise}
       */
      _updateContact: function(action, name, number) {
        var self = this;
        var cardIndex = DsdsSettings.getIccCardIndexForCallSettings();

        var contact = FdnContext.createAction(action, {
          cardIndex: cardIndex,
          contact: {
            id: this._currentContact && this._currentContact.id,
            name: name,
            number: number
          }
        });

        FdnDialog.show('get_pin2', {
          fdnContact: contact,
          cardIndex: cardIndex,
          onsuccess: () => {
            SettingsService.navigate('call-fdnList', {
              name: self._elements.fdnNameInput.value,
              number: self._elements.fdnNumberInput.value
            });
          },
          oncancel: () => {
            SettingsService.navigate('call-fdnList', {
              name: self._elements.fdnNameInput.value,
              number: self._elements.fdnNumberInput.value
            });
          }
        });
      },

      _isPhoneNumberValid: function(number) {
        if (number) {
          var re = /^([\+]*[0-9])+$/;
          //According to 3Gpp TS 22.101, the modem need check '*99#' before
          //active PDP, so we should allow the user add the '*99#' at FDN list.
          if (re.test(number) || number === '*99#') {
            return true;
          }
        }
        return false;
      },

      _checkContactInputs: function() {
        if (this._elements.fdnNameInput.value === '' ||
          this._elements.fdnNumberInput.value === '' ||
          !this._isPhoneNumberValid(this._elements.fdnNumberInput.value)) {
          this._submitable = false;
        } else {
          this._submitable = true;
        }
        this._initSoftkey(this._submitable);
      }
    });
  };
});
