/* global DsdsSettings */
/**
 * The apn settings panel
 */
define(['require','shared/toaster','modules/settings_panel','modules/settings_service','modules/apn/apn_settings_manager'],function(require) {
  
  var Toaster = require('shared/toaster');
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');
  var ApnSettingsManager = require('modules/apn/apn_settings_manager');

  return function ctor_apn_settings_panel() {
    var _serviceId = 0;
    var _apnSettingsList;

    function _initSoftKey() {
      var softkeyParams = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }, {
          name: 'Reset to Default',
          l10nId: 'reset-apn',
          priority: 3,
          method: function() {
            _resetApn();
          }
        }]
      };

      SettingsSoftkey.init(softkeyParams);
      SettingsSoftkey.show();
    }

    function _resetApnWarningDialog() {
      return new Promise(resolve => {
        var dialogConfig = {
          title: {id: 'apnSettings-reset', args: {}},
          body: {id: 'reset-apn-warning-message', args: {}},
          cancel: {
            name: 'Cancel',
            l10nId: 'cancel',
            priority: 1,
            callback: function() {
              resolve(false);
              dialog.destroy();
            }
          },
          confirm: {
            name: 'Rest',
            l10nId: 'reset-apn',
            priority: 3,
            callback: function() {
              resolve(true);
              dialog.destroy();
            }
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('app-confirmation-dialog'));
      });
    }

    function _resetApn() {
      _resetApnWarningDialog().then(result => {
        if (result) {
          ApnSettingsManager.restore(_serviceId).then(() => {
            var toast = {
              messageL10nId: 'apnSettings-reset-toast',
              latency: 3000,
              useTransition: true
            };
            Toaster.showToast(toast);
          });
        }
      });
    }

    function _browseApnItems(evt) {
      if (!evt.target.dataset.apnType) {
        return;
      }
      SettingsService.navigate(
        'apn-list', {
          type: evt.target.dataset.apnType,
          serviceId: _serviceId
        }
      );
    }

    function _addClickEventListener() {
      var i = _apnSettingsList.length - 1;
      for (i; i >= 0; i--) {
        _apnSettingsList[i].addEventListener('click', _browseApnItems);
      }
    }

    function _removeClickEventListener() {
      var i = _apnSettingsList.length - 1;
      for (i; i >= 0; i--) {
        _apnSettingsList[i].removeEventListener('click', _browseApnItems);
      }
    }

    /**
     * XXX: Hide the Message/A-GPS/Tethering APN settings item for India carrier
     */
    function _initUI(panel) {
      ApnSettingsManager._getPlmnAndMvnoInfo(_serviceId).then((values) => {
        var mcc = values[0];
        var mnc = values[1];
        var hidden = false;
        if (/^40[4-6]$/.test(mcc)) {
          hidden = true;
        }
        var itemlist = panel.querySelectorAll('li.apn-optional');
        var item = null;
        for (item of itemlist) {
          item.hidden = hidden;
        }
        if(hidden==false){
          var imshidden = false;
          var codelist = "23415,23877,23802,23430,23433,20404,52505";
          if(codelist.indexOf(mcc+mnc)!=-1){
            imshidden=true;
          }
          var imsli = panel.querySelector('a[data-apn-type="ims"]').parentNode;
          imsli.hidden = imshidden;
        }
      });
    }

    return SettingsPanel({
      onInit: function asp_onInit(rootElement) {
        _apnSettingsList = rootElement.querySelectorAll('a[data-apn-type]');
        //add by tfzhang on task 5133263 begin
        var xcapSettings = rootElement.querySelector('li[id=xcapSettings]');
        window.navigator.customization.getValue('hide.xcap.apn.bool').then(function (result) {
          if (JSON.stringify(result) !== undefined) {
            var resultValue = JSON.stringify(result);
            if (!resultValue && xcapSettings !== undefined && xcapSettings != null) {//Defect640-tf-zhang@t2mobile.com
              xcapSettings.classList.remove('hidden');//Defect857-tf-zhang@t2mobile.com-modify
            }
          } else {
            console.log('geterror hide.xcap.apn.bool='+result);
          }
        });
        //add by tfzhang on task 5133263 end
      },

      onBeforeShow: function asp_onBeforeShow(panel) {
        _initSoftKey();
        _serviceId = DsdsSettings.getIccCardIndexForCellAndDataSettings();
        _initUI(panel);
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
      },

      onShow: function() {
        _addClickEventListener();
      },

      onHide: function() {
        _removeClickEventListener();
      }
    });
  };
});
