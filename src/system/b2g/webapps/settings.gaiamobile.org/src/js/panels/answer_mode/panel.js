/**
 * Used to show Calling/Answer-Options panel
 */
define(['require','modules/settings_panel','shared/settings_listener','shared/toaster'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');
  var SettingsListener = require('shared/settings_listener');
  var Toaster = require('shared/toaster');

  return function createAnswerModePanel() {
    function _showToast() {
      var toast = {
        messageL10nId: 'changessaved',
        latency: 3000,
        useTransition: true
      };
      Toaster.showToast(toast);
    }

    return SettingsPanel({
        // Task5299556-yinghuizhang@t2mobile.com-begin
      onInit: function(panel) {
        this.flipopenSelect = document.getElementById('answer-flipopen');
        SettingsListener.observe('phone.answer.flipopen.enabled', true,
          value => {
            this.flipopenSelect.value = value;
          }
        );
          this.slideopenSelect = document.getElementById('answer-slideopen');
          SettingsListener.observe('phone.answer.slideopen.enabled', true,
              value => {
              this.slideopenSelect.value = value;
          }
          );
      },

      onBeforeShow: function() {
        this.flipopenSelect.addEventListener('change', _showToast);
        this.slideopenSelect.addEventListener('change', _showToast);
      },

      onBeforeHide: function() {
        this.flipopenSelect.removeEventListener('change', _showToast);
        this.slideopenSelect.removeEventListener('change', _showToast);
      }
    });
      // Task5299556-yinghuizhang@t2mobile.com-end
  };
});
