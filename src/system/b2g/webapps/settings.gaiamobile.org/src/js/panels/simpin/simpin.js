/* global SettingsSoftkey */

define(['require','shared/airplane_mode_helper','shared/simslot_manager','simcard_dialog','shared/template','shared/toaster','modules/settings_service'],function(require) {
  

  var _ = window.navigator.mozL10n.get;
  var AirplaneModeHelper = require('shared/airplane_mode_helper');
  var SIMSlotManager = require('shared/simslot_manager');
  var SimPinDialog = require('simcard_dialog');
  var Template = require('shared/template');
  var Toaster = require('shared/toaster');
  var SettingsService = require('modules/settings_service');

  var SimPin = function(elements) {
    this._elements = elements;
  };

  SimPin.prototype = {
    init: function simpin_init() {
      AirplaneModeHelper.ready(() => {
        this.conns = window.navigator.mozMobileConnections;
        this.iccManager = window.navigator.mozIccManager;
        this.isAirplaneMode = (AirplaneModeHelper.getStatus() === 'enabled');
        this.simPinTemplate = new Template(this._elements.simPinTmpl);
        this.simPinDialog = new SimPinDialog(this._elements.dialog);

        this._elements.simPinContainer.addEventListener('click', this);
        this.addIccDetectedEvent();
        this.addIccUndetectedEvent();
        this.addAirplaneModeChangeEvent();

        this.initSimPinBack();
        this.initSimPinsUI();
        this.updateSimPinsUI();
        this.addChangeEventOnIccs();
        // Modified by yingsen.zhang@t2mbole.com 2018.01.02 begin
        // Modified for listen SIM card name changed event
        this.addCardNameChangedEvent();
        // Modified by yingsen.zhang@t2mbole.com 2018.01.02 end
      });
    },
    initSimPinBack: function simpin_initSimPinBack() {
      // Because this panel is used in one-sim & two-sim structures,
      // the entry point of sim security is different.
      //
      // In this way, we have to make sure users can go back to the
      // right panel.
      this._elements.simPinHeader.dataset.href = SIMSlotManager.isMultiSIM() ?
        '#sim-manager': '#root';
    },
    initSimPinsUI: function simpin_initSimPinsUI() {
      var simPinHTMLs = [];

      [].forEach.call(this.conns, (conn, index) => {
        var simPinIndex = index + 1;

        if (!SIMSlotManager.isMultiSIM()) {
          simPinIndex = '';
        }

        simPinHTMLs.push(
          this.simPinTemplate.interpolate({
            'sim-index': index.toString(),
            'sim-name': _('simPinWithIndex', { 'index': simPinIndex }),
            'change-sim-label': _('changeSimPinWithIndex',
              { 'index': simPinIndex }),
            'validate-sim-label': _('validateSimPinWithIndex',
              { 'index': simPinIndex })
          })
        );
      });

      this._elements.simPinContainer.innerHTML = simPinHTMLs.join('');
    },
    updateSimPinUI: function simpin_updateSimPinUI(cardIndex) {
      var iccId = this.conns[cardIndex].iccId;
      var icc = this.iccManager.getIccById(iccId);
      var changeSimPinItem =
        this._elements.simPinContainer.querySelector(
          '.simpin-change-' + cardIndex);

      var simPinEnableSelect =
        this._elements.simPinContainer.querySelector(
          '.simpin-enabled-' + cardIndex + ' select');

      var simPinItem =
        this._elements.simPinContainer.querySelector(
          '.simpin-' + cardIndex);

      var validateSimPinItem =
        this._elements.simPinContainer.querySelector(
          '.simpin-validate-' + cardIndex);

      // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 begin
      var simPinTitle = simPinItem.querySelector('.simPin');
      var changePinTitle = changeSimPinItem.querySelector('a span');
      var validatePinTitle = validateSimPinItem.querySelector('a span');
      // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 end

      if (icc === null) {
        simPinEnableSelect.disabled = true;
        simPinItem.setAttribute('aria-disabled', true);
      } else {
        simPinEnableSelect.disabled = false;
        simPinItem.setAttribute('aria-disabled', false);

        // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 begin
        updateSIMCardName(cardIndex, function(cardIndex, cardName) {
          if (cardName && cardName.length !== 0) {
            simPinTitle.textContent = cardName + ' ' + navigator.mozL10n.get('simPin');
            if (navigator.language.startsWith('ko')) {
              // Korea language specific grammer
              changePinTitle.textContent = cardName + ' '
                + navigator.mozL10n.get('simPin') + ' ' + navigator.mozL10n.get('change');
            }
            else
            {
              changePinTitle.textContent = navigator.mozL10n.get('change') + ' ' + cardName + ' '
                + navigator.mozL10n.get('simPin');
            }
            validatePinTitle.textContent = navigator.mozL10n.get('sim_pin_title_prefix_validate')
              + ' ' + cardName + ' ' + navigator.mozL10n.get('simPin');
          }
        });
        // Modified by yingsen.zhang@t2mbole.com for sync SIM card name 2018.01.02 end
      }

      simPinEnableSelect.onchange = () => {
        var cardIndex = simPinEnableSelect.dataset &&
          simPinEnableSelect.dataset.simIndex;
        cardIndex = parseInt(cardIndex, 10);
        this.checkSimPinSelect(simPinEnableSelect, cardIndex);
      };

      var isSimAvailable = icc && icc.cardState && icc.cardState !== 'unknown';
      // when fugu is in airplane mode, icc.cardState will not be changed ...
      // in this way, we have to use isAirplaneMode to check this situation
      if (!isSimAvailable || this.isAirplaneMode) {
        changeSimPinItem.hidden = true;
        return;
      }

      // with SIM card, query its status
      var req = icc.getCardLock('pin');
      req.onsuccess = () => {
        var enabled = req.result.enabled;
        if (icc.cardState === 'pinRequired' || icc.cardState === 'pukRequired') {
          simPinItem.hidden = true;
          changeSimPinItem.hidden = true;
          validateSimPinItem.hidden = false;
        } else {
          simPinItem.hidden = false;
          validateSimPinItem.hidden = true;
          changeSimPinItem.hidden = !enabled;
          this.updateSelectState(simPinEnableSelect, enabled);
          window.dispatchEvent(new CustomEvent('panelready', {
            detail: {
              current: '#simpin',
              needFocused: null
            }
          }));
        }
      };

      req.onerror = () => {
        console.error('ERROR: SIM ' + (++cardIndex) +
          ' PIN status checked failed, reason: ' +
          req.error.name + '.');
      };
    },
    updateSelectState: function simpin_updateSelectState(simPinEnableSelect, enabled) {
      simPinEnableSelect.options[0].selected = enabled;
      simPinEnableSelect.options[1].selected = !enabled;
    },
    updateSimPinsUI: function simpin_updateSimPinsUI() {
      [].forEach.call(this.conns, (simcard, cardIndex) => {
        this.updateSimPinUI(cardIndex);
      });
    },
    handleEvent: function simpin_handleEvent(evt) {
      var target = evt.target;
      var cardIndex = target.dataset && target.dataset.simIndex;
      var type = target.dataset && target.dataset.type;

      if (!cardIndex) {
        return;
      }

      // We need number type
      cardIndex = parseInt(cardIndex, 10);
      var iccId = this.conns[cardIndex].iccId;
      var icc = this.iccManager.getIccById(iccId);

      switch (type) {
        case 'checkSimPin':
          this.checkSimPin(target, cardIndex);
          break;

        case 'validateSimPin':
          if (icc.cardState === 'pinRequired') {
            this.simPinDialog.show('unlock_pin', {
              cardIndex: cardIndex,
              onsuccess: () => {
                this.updateSimPinUI(cardIndex);
              },
              oncancel: () => {
                this.updateSimPinUI(cardIndex);
              }
            });
          }
          if (icc.cardState === 'pukRequired') {
            this.simPinDialog.show('unlock_puk', {
              cardIndex: cardIndex,
              onsuccess: () => {
                this.updateSimPinUI(cardIndex);
              },
              oncancel: () => {
                this.updateSimPinUI(cardIndex);
              }
            });
          }
          break;

        case 'changeSimPin':
          // TODO:
          // remember to update SimPinDialog for DSDS structure
          if (icc.cardState === 'pukRequired') {
            this.simPinDialog.show('unlock_puk', {
              cardIndex: cardIndex,
              onsuccess: () => {
                this.updateSimPinUI(cardIndex);
              },
              oncancel: () => {
                this.updateSimPinUI(cardIndex);
              }
            });
          } else {
            this.simPinDialog.show('change_pin', {
              cardIndex: cardIndex,
              // show toast after user successfully change pin
              onsuccess: function toastOnSuccess() {
                var toast;
                if (SIMSlotManager.isMultiSIM()) {
                  toast = {
                    messageL10nId: 'simPinChangedSuccessfullyWithIndex',
                    messageL10nArgs: {'index': +(cardIndex) + 1},
                    latency: 3000,
                    useTransition: true
                  };
                } else {
                  toast = {
                    messageL10nId: 'simPinChangedSuccessfully',
                    latency: 3000,
                    useTransition: true
                  };
                }
                Toaster.showToast(toast);
              }
            });
            break;
          }
        }
    },
    checkSimPinSelect: function simpin_checkSimPinSelect(simPinEnableSelect, cardIndex) {
      var enabled = (simPinEnableSelect.value === 'true');
      var iccId = this.conns[cardIndex].iccId;
      var icc = this.iccManager.getIccById(iccId);

      if (icc.cardState === 'pukRequired') {
        this.simPinDialog.show('unlock_puk', {
            cardIndex: cardIndex,
            onsuccess: () => {
              this.updateSimPinUI(cardIndex);
            },
            oncancel: () => {
              this.updateSimPinUI(cardIndex);
            }
        });
      } else {
        var action = enabled ? 'enable_lock' : 'disable_lock';
          this.simPinDialog.show(action, {
            cardIndex: cardIndex,
            onsuccess: () => {
              SettingsSoftkey.hide();
              this.updateSimPinUI(cardIndex);
            },
            oncancel: () => {
              SettingsSoftkey.hide();
              this.updateSimPinUI(cardIndex);
            }
        });
      }
    },
    addIccDetectedEvent: function simpin_addIccDetectedEvent() {
      // if there is a change that icc instance is available
      // we can update its cardstatus to make it reflect the
      // real world.
      this.iccManager.addEventListener('iccdetected', (evt) => {
        var iccId = evt.iccId;
        var icc = this.iccManager.getIccById(iccId);

        if (icc) {
          var cardIndex = this.getCardIndexByIccId(iccId);

          // we have to update its status and add change event
          // for it to make it reflect status on to UI
          this.updateSimPinUI(cardIndex);
          this.addChangeEventOnIccByIccId(iccId);
        }
      });
    },
    addIccUndetectedEvent: function simpin_addIccDetectedEvent() {
      // if there is a change that icc instance is not available
      // we have to update all cards' status
      this.iccManager.addEventListener('iccundetected', (evt) => {
        this.updateSimPinsUI();
      });
    },
    addAirplaneModeChangeEvent: function simpin_addAirplaneModeChangeEvent() {
      AirplaneModeHelper.addEventListener('statechange', (status) => {
        this.isAirplaneMode = (status === 'enabled');
        this.updateSimPinsUI();
      });
    },
    addChangeEventOnIccs: function simpin_addChangeEventOnIccs() {
      for (var i = 0; i < this.conns.length; i++) {
        var iccId = this.conns[i].iccId;
        var icc = this.iccManager.getIccById(iccId);
        if (icc) {
          this.addChangeEventOnIccByIccId(iccId);
        }
      }
    },
    // Modified by yingsen.zhang@t2mbole.com 2018.01.02 begin
    // Modified for listen SIM card name changed event
    addCardNameChangedEvent: function simpin_addCardNameChangedEvent() {
      window.addEventListener('cardnamechanged', function(e) {
        this.updateSimPinsUI();
      }.bind(this));
    },
    // Modified by yingsen.zhang@t2mbole.com 2018.01.02 end
    addChangeEventOnIccByIccId:
      function simpin_addChangeEventOnIccByIccId(iccId) {
        var icc = this.iccManager.getIccById(iccId);
        if (icc) {
          icc.addEventListener('cardstatechange', () => {
            var cardIndex = this.getCardIndexByIccId(iccId);
            this.updateSimPinUI(cardIndex);
          });
        }
    },
    getCardIndexByIccId: function simpin_getCardIndexByIccId(iccId) {
      var cardIndex;
      for (var i = 0; i < this.conns.length; i++) {
        if (this.conns[i].iccId == iccId) {
          cardIndex = i;
        }
      }
      return cardIndex;
    }
  };

  return function ctor_simpin(elements) {
    return new SimPin(elements);
  };
});
