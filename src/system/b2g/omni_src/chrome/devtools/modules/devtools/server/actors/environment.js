"use strict";const{ActorClass,Arg,RetVal,method}=require("devtools/server/protocol");const{createValueGrip}=require("devtools/server/actors/object");let EnvironmentActor=ActorClass({typeName:"environment",initialize:function(environment,threadActor){this.obj=environment;this.threadActor=threadActor;},form:function(){let form={actor:this.actorID};if(this.obj.type=="declarative"){form.type=this.obj.callee?"function":"block";}else{form.type=this.obj.type;}
if(this.obj.parent){form.parent=(this.threadActor.createEnvironmentActor(this.obj.parent,this.registeredPool).form());}
if(this.obj.type=="object"||this.obj.type=="with"){form.object=createValueGrip(this.obj.object,this.registeredPool,this.threadActor.objectGrip);}
if(this.obj.callee){form.function=createValueGrip(this.obj.callee,this.registeredPool,this.threadActor.objectGrip);}
if(this.obj.type=="declarative"){form.bindings=this.bindings();}
return form;},assign:method(function(name,value){
try{this.obj.setVariable(name,value);}catch(e){if(e instanceof Debugger.DebuggeeWouldRun){throw{error:"threadWouldRun",message:"Assigning a value would cause the debuggee to run"};}else{throw e;}}
return{from:this.actorID};},{request:{name:Arg(1),value:Arg(2)}}),bindings:method(function(){let bindings={arguments:[],variables:{}};
if(typeof this.obj.getVariable!="function"){return bindings;}
let parameterNames;if(this.obj.callee){parameterNames=this.obj.callee.parameterNames;}else{parameterNames=[];}
for(let name of parameterNames){let arg={};let value=this.obj.getVariable(name);
let desc={value:value,configurable:false,writable:!(value&&value.optimizedOut),enumerable:true};let descForm={enumerable:true,configurable:desc.configurable};if("value"in desc){descForm.value=createValueGrip(desc.value,this.registeredPool,this.threadActor.objectGrip);descForm.writable=desc.writable;}else{descForm.get=createValueGrip(desc.get,this.registeredPool,this.threadActor.objectGrip);descForm.set=createValueGrip(desc.set,this.registeredPool,this.threadActor.objectGrip);}
arg[name]=descForm;bindings.arguments.push(arg);}
for(let name of this.obj.names()){if(bindings.arguments.some(function exists(element){return!!element[name];})){continue;}
let value=this.obj.getVariable(name);
let desc={value:value,configurable:false,writable:!(value&&(value.optimizedOut||value.uninitialized||value.missingArguments)),enumerable:true};let descForm={enumerable:true,configurable:desc.configurable};if("value"in desc){descForm.value=createValueGrip(desc.value,this.registeredPool,this.threadActor.objectGrip);descForm.writable=desc.writable;}else{descForm.get=createValueGrip(desc.get||undefined,this.registeredPool,this.threadActor.objectGrip);descForm.set=createValueGrip(desc.set||undefined,this.registeredPool,this.threadActor.objectGrip);}
bindings.variables[name]=descForm;}
return bindings;},{request:{},response:{bindings:RetVal("json")}})});exports.EnvironmentActor=EnvironmentActor;