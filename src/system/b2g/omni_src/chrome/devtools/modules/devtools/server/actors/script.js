"use strict";const Services=require("Services");const{Cc,Ci,Cu,Cr,components,ChromeWorker}=require("chrome");const{ActorPool,OriginalLocation,GeneratedLocation}=require("devtools/server/actors/common");const{BreakpointActor,setBreakpointAtEntryPoints}=require("devtools/server/actors/breakpoint");const{EnvironmentActor}=require("devtools/server/actors/environment");const{FrameActor}=require("devtools/server/actors/frame");const{ObjectActor,createValueGrip,longStringGrip}=require("devtools/server/actors/object");const{SourceActor,getSourceURL}=require("devtools/server/actors/source");const{DebuggerServer}=require("devtools/server/main");const{ActorClass}=require("devtools/server/protocol");const DevToolsUtils=require("devtools/shared/DevToolsUtils");const{assert,dumpn,update,fetch}=DevToolsUtils;const promise=require("promise");const PromiseDebugging=require("PromiseDebugging");const xpcInspector=require("xpcInspector");const ScriptStore=require("./utils/ScriptStore");const{DevToolsWorker}=require("devtools/shared/worker/worker");const object=require("sdk/util/object");const{defer,resolve,reject,all}=promise;loader.lazyGetter(this,"Debugger",()=>{let Debugger=require("Debugger");hackDebugger(Debugger);return Debugger;});loader.lazyRequireGetter(this,"CssLogic","devtools/shared/inspector/css-logic",true);loader.lazyRequireGetter(this,"events","sdk/event/core");loader.lazyRequireGetter(this,"mapURIToAddonID","devtools/server/actors/utils/map-uri-to-addon-id");loader.lazyRequireGetter(this,"setTimeout","sdk/timers",true);function BreakpointActorMap(){this._size=0;this._actors={};}
BreakpointActorMap.prototype={get size(){return this._size;},findActors:function*(location=new OriginalLocation()){

if(this.size===0){return;}
function*findKeys(object,key){if(key!==undefined){if(key in object){yield key;}}
else{for(let key of Object.keys(object)){yield key;}}}
let query={sourceActorID:location.originalSourceActor?location.originalSourceActor.actorID:undefined,line:location.originalLine,};

if(location.originalLine){query.beginColumn=location.originalColumn?location.originalColumn:0;query.endColumn=location.originalColumn?location.originalColumn+1:Infinity;}else{query.beginColumn=location.originalColumn?query.originalColumn:undefined;query.endColumn=location.originalColumn?query.originalColumn+1:undefined;}
for(let sourceActorID of findKeys(this._actors,query.sourceActorID))
for(let line of findKeys(this._actors[sourceActorID],query.line))
for(let beginColumn of findKeys(this._actors[sourceActorID][line],query.beginColumn))
for(let endColumn of findKeys(this._actors[sourceActorID][line][beginColumn],query.endColumn)){yield this._actors[sourceActorID][line][beginColumn][endColumn];}},getActor:function(originalLocation){for(let actor of this.findActors(originalLocation)){return actor;}
return null;},setActor:function(location,actor){let{originalSourceActor,originalLine,originalColumn}=location;let sourceActorID=originalSourceActor.actorID;let line=originalLine;let beginColumn=originalColumn?originalColumn:0;let endColumn=originalColumn?originalColumn+1:Infinity;if(!this._actors[sourceActorID]){this._actors[sourceActorID]=[];}
if(!this._actors[sourceActorID][line]){this._actors[sourceActorID][line]=[];}
if(!this._actors[sourceActorID][line][beginColumn]){this._actors[sourceActorID][line][beginColumn]=[];}
if(!this._actors[sourceActorID][line][beginColumn][endColumn]){++this._size;}
this._actors[sourceActorID][line][beginColumn][endColumn]=actor;},deleteActor:function(location){let{originalSourceActor,originalLine,originalColumn}=location;let sourceActorID=originalSourceActor.actorID;let line=originalLine;let beginColumn=originalColumn?originalColumn:0;let endColumn=originalColumn?originalColumn+1:Infinity;if(this._actors[sourceActorID]){if(this._actors[sourceActorID][line]){if(this._actors[sourceActorID][line][beginColumn]){if(this._actors[sourceActorID][line][beginColumn][endColumn]){--this._size;}
delete this._actors[sourceActorID][line][beginColumn][endColumn];if(Object.keys(this._actors[sourceActorID][line][beginColumn]).length===0){delete this._actors[sourceActorID][line][beginColumn];}}
if(Object.keys(this._actors[sourceActorID][line]).length===0){delete this._actors[sourceActorID][line];}}}}};exports.BreakpointActorMap=BreakpointActorMap;function SourceActorStore(){ this._sourceActorIds=Object.create(null);}
SourceActorStore.prototype={getReusableActorId:function(aSource,aOriginalUrl){let url=this.getUniqueKey(aSource,aOriginalUrl);if(url&&url in this._sourceActorIds){return this._sourceActorIds[url];}
return null;},setReusableActorId:function(aSource,aOriginalUrl,actorID){let url=this.getUniqueKey(aSource,aOriginalUrl);if(url){this._sourceActorIds[url]=actorID;}},getUniqueKey:function(aSource,aOriginalUrl){if(aOriginalUrl){return aOriginalUrl;}
else{return getSourceURL(aSource);}}};exports.SourceActorStore=SourceActorStore;function EventLoopStack({thread,connection,hooks}){this._hooks=hooks;this._thread=thread;this._connection=connection;}
EventLoopStack.prototype={get size(){return xpcInspector.eventLoopNestLevel;},get lastPausedUrl(){let url=null;if(this.size>0){try{url=xpcInspector.lastNestRequestor.url}catch(e){
dumpn(e);}}
return url;},get lastConnection(){return xpcInspector.lastNestRequestor._connection;},push:function(){return new EventLoop({thread:this._thread,connection:this._connection,hooks:this._hooks});}};function EventLoop({thread,connection,hooks}){this._thread=thread;this._hooks=hooks;this._connection=connection;this.enter=this.enter.bind(this);this.resolve=this.resolve.bind(this);}
EventLoop.prototype={entered:false,resolved:false,get url(){return this._hooks.url;},enter:function(){let nestData=this._hooks.preNest?this._hooks.preNest():null;this.entered=true;xpcInspector.enterNestedEventLoop(this);if(xpcInspector.eventLoopNestLevel>0){const{resolved}=xpcInspector.lastNestRequestor;if(resolved){xpcInspector.exitNestedEventLoop();}}
if(this._hooks.postNest){this._hooks.postNest(nestData);}},resolve:function(){if(!this.entered){throw new Error("Can't resolve an event loop before it has been entered!");}
if(this.resolved){throw new Error("Already resolved this nested event loop!");}
this.resolved=true;if(this===xpcInspector.lastNestRequestor){xpcInspector.exitNestedEventLoop();return true;}
return false;},};const ThreadActor=ActorClass({typeName:"context",initialize:function(aParent,aGlobal){this._state="detached";this._frameActors=[];this._parent=aParent;this._dbg=null;this._gripDepth=0;this._threadLifetimePool=null;this._tabClosed=false;this._scripts=null;this._pauseOnDOMEvents=null;this._options={useSourceMaps:false,autoBlackBox:false};this.breakpointActorMap=new BreakpointActorMap();this.sourceActorStore=new SourceActorStore();this._debuggerSourcesSeen=null;
this._hiddenBreakpoints=new Map();this.global=aGlobal;this._allEventsListener=this._allEventsListener.bind(this);this.onNewGlobal=this.onNewGlobal.bind(this);this.onSourceEvent=this.onSourceEvent.bind(this);this.uncaughtExceptionHook=this.uncaughtExceptionHook.bind(this);this.onDebuggerStatement=this.onDebuggerStatement.bind(this);this.onNewScript=this.onNewScript.bind(this);this.objectGrip=this.objectGrip.bind(this);this.pauseObjectGrip=this.pauseObjectGrip.bind(this);this._onWindowReady=this._onWindowReady.bind(this);events.on(this._parent,"window-ready",this._onWindowReady);
this.wrappedJSObject=this;},_gripDepth:null,get dbg(){if(!this._dbg){this._dbg=this._parent.makeDebugger();this._dbg.uncaughtExceptionHook=this.uncaughtExceptionHook;this._dbg.onDebuggerStatement=this.onDebuggerStatement;this._dbg.onNewScript=this.onNewScript;this._dbg.on("newGlobal",this.onNewGlobal);this._dbg.enabled=this._state!="detached";}
return this._dbg;},get globalDebugObject(){if(!this._parent.window){return null;}
return this.dbg.makeGlobalObjectReference(this._parent.window);},get state(){return this._state;},get attached(){return this.state=="attached"||this.state=="running"||this.state=="paused";},get threadLifetimePool(){if(!this._threadLifetimePool){this._threadLifetimePool=new ActorPool(this.conn);this.conn.addActorPool(this._threadLifetimePool);this._threadLifetimePool.objectActors=new WeakMap();}
return this._threadLifetimePool;},get scripts(){if(!this._scripts){this._scripts=new ScriptStore();this._scripts.addScripts(this.dbg.findScripts());}
return this._scripts;},get sources(){return this._parent.sources;},get youngestFrame(){if(this.state!="paused"){return null;}
return this.dbg.getNewestFrame();},_prettyPrintWorker:null,get prettyPrintWorker(){if(!this._prettyPrintWorker){this._prettyPrintWorker=new DevToolsWorker("resource://devtools/server/actors/pretty-print-worker.js",{name:"pretty-print",verbose:dumpn.wantLogging});}
return this._prettyPrintWorker;},_threadPauseEventLoops:null,_pushThreadPause:function(){if(!this._threadPauseEventLoops){this._threadPauseEventLoops=[];}
const eventLoop=this._nestedEventLoops.push();this._threadPauseEventLoops.push(eventLoop);eventLoop.enter();},_popThreadPause:function(){const eventLoop=this._threadPauseEventLoops.pop();assert(eventLoop,"Should have an event loop.");eventLoop.resolve();},clearDebuggees:function(){if(this._dbg){this.dbg.removeAllDebuggees();}
this._sources=null;this._scripts=null;},onNewGlobal:function(aGlobal){this.conn.send({from:this.actorID,type:"newGlobal",hostAnnotations:aGlobal.hostAnnotations});},disconnect:function(){dumpn("in ThreadActor.prototype.disconnect");if(this._state=="paused"){this.onResume();}


this._sourceActorStore=null;events.off(this._parent,"window-ready",this._onWindowReady);this.sources.off("newSource",this.onSourceEvent);this.sources.off("updatedSource",this.onSourceEvent);this.clearDebuggees();this.conn.removeActorPool(this._threadLifetimePool);this._threadLifetimePool=null;if(this._prettyPrintWorker){this._prettyPrintWorker.destroy();this._prettyPrintWorker=null;}
if(!this._dbg){return;}
this._dbg.enabled=false;this._dbg=null;},exit:function(){this.disconnect();this._state="exited";}, onAttach:function(aRequest){if(this.state==="exited"){return{type:"exited"};}
if(this.state!=="detached"){return{error:"wrongState",message:"Current state is "+this.state};}
this._state="attached";this._debuggerSourcesSeen=new Set();Object.assign(this._options,aRequest.options||{});this.sources.setOptions(this._options);this.sources.on("newSource",this.onSourceEvent);this.sources.on("updatedSource",this.onSourceEvent);this._nestedEventLoops=new EventLoopStack({hooks:this._parent,connection:this.conn,thread:this});this.dbg.addDebuggees();this.dbg.enabled=true;try{let packet=this._paused();if(!packet){return{error:"notAttached"};}
packet.why={type:"attached"};this._restoreBreakpoints();

this.conn.send(packet);this._pushThreadPause();
return null;}catch(e){reportError(e);return{error:"notAttached",message:e.toString()};}},onDetach:function(aRequest){this.disconnect();this._state="detached";this._debuggerSourcesSeen=null;dumpn("ThreadActor.prototype.onDetach: returning 'detached' packet");return{type:"detached"};},onReconfigure:function(aRequest){if(this.state=="exited"){return{error:"wrongState"};}
const options=aRequest.options||{};if('observeAsmJS'in options){this.dbg.allowUnobservedAsmJS=!options.observeAsmJS;}
Object.assign(this._options,options); this.sources.setOptions(options);return{};},_pauseAndRespond:function(aFrame,aReason,onPacket=function(k){return k;}){try{let packet=this._paused(aFrame);if(!packet){return undefined;}
packet.why=aReason;let generatedLocation=this.sources.getFrameLocation(aFrame);this.sources.getOriginalLocation(generatedLocation).then((originalLocation)=>{if(!originalLocation.originalSourceActor){


DevToolsUtils.reportException('ThreadActor',new Error('Attempted to pause in a script with a sourcemap but '+'could not find original location.'));return undefined;}
packet.frame.where={source:originalLocation.originalSourceActor.form(),line:originalLocation.originalLine,column:originalLocation.originalColumn};resolve(onPacket(packet)).then(null,error=>{reportError(error);return{error:"unknownError",message:error.message+"\n"+error.stack};}).then(packet=>{this.conn.send(packet);});});this._pushThreadPause();}catch(e){reportError(e,"Got an exception during TA__pauseAndRespond: ");}


return this._tabClosed?null:undefined;},_makeOnEnterFrame:function({pauseAndRespond}){return aFrame=>{const generatedLocation=this.sources.getFrameLocation(aFrame);let{originalSourceActor}=this.unsafeSynchronize(this.sources.getOriginalLocation(generatedLocation));let url=originalSourceActor.url;return this.sources.isBlackBoxed(url)?undefined:pauseAndRespond(aFrame);};},_makeOnPop:function({thread,pauseAndRespond,createValueGrip}){return function(aCompletion){const generatedLocation=thread.sources.getFrameLocation(this);const{originalSourceActor}=thread.unsafeSynchronize(thread.sources.getOriginalLocation(generatedLocation));const url=originalSourceActor.url;if(thread.sources.isBlackBoxed(url)){return undefined;}

this.reportedPop=true;return pauseAndRespond(this,aPacket=>{aPacket.why.frameFinished={};if(!aCompletion){aPacket.why.frameFinished.terminated=true;}else if(aCompletion.hasOwnProperty("return")){aPacket.why.frameFinished.return=createValueGrip(aCompletion.return);}else if(aCompletion.hasOwnProperty("yield")){aPacket.why.frameFinished.return=createValueGrip(aCompletion.yield);}else{aPacket.why.frameFinished.throw=createValueGrip(aCompletion.throw);}
return aPacket;});};},_makeOnStep:function({thread,pauseAndRespond,startFrame,startLocation,steppingType}){if(steppingType==="break"){return function(){return pauseAndRespond(this);};}
return function(){



if(this===startFrame&&!this.script.getOffsetLocation(this.offset).isEntryPoint){return undefined;}
const generatedLocation=thread.sources.getFrameLocation(this);const newLocation=thread.unsafeSynchronize(thread.sources.getOriginalLocation(generatedLocation));



 
if(newLocation.originalUrl==null||thread.sources.isBlackBoxed(newLocation.originalUrl)){return undefined;} 
if(this!==startFrame||startLocation.originalUrl!==newLocation.originalUrl||startLocation.originalLine!==newLocation.originalLine){return pauseAndRespond(this);}

return undefined;};},_makeSteppingHooks:function(aStartLocation,steppingType){


const steppingHookState={pauseAndRespond:(aFrame,onPacket=k=>k)=>{return this._pauseAndRespond(aFrame,{type:"resumeLimit"},onPacket);},createValueGrip:v=>createValueGrip(v,this._pausePool,this.objectGrip),thread:this,startFrame:this.youngestFrame,startLocation:aStartLocation,steppingType:steppingType};return{onEnterFrame:this._makeOnEnterFrame(steppingHookState),onPop:this._makeOnPop(steppingHookState),onStep:this._makeOnStep(steppingHookState)};},_handleResumeLimit:function(aRequest){let steppingType=aRequest.resumeLimit.type;if(["break","step","next","finish"].indexOf(steppingType)==-1){return reject({error:"badParameterType",message:"Unknown resumeLimit type"});}
const generatedLocation=this.sources.getFrameLocation(this.youngestFrame);return this.sources.getOriginalLocation(generatedLocation).then(originalLocation=>{const{onEnterFrame,onPop,onStep}=this._makeSteppingHooks(originalLocation,steppingType);
let stepFrame=this._getNextStepFrame(this.youngestFrame);if(stepFrame){switch(steppingType){case"step":this.dbg.onEnterFrame=onEnterFrame;case"break":case"next":if(stepFrame.script){stepFrame.onStep=onStep;}
stepFrame.onPop=onPop;break;case"finish":stepFrame.onPop=onPop;}}
return true;});},_clearSteppingHooks:function(aFrame){if(aFrame&&aFrame.live){while(aFrame){aFrame.onStep=undefined;aFrame.onPop=undefined;aFrame=aFrame.older;}}},_maybeListenToEvents:function(aRequest){let events=aRequest.pauseOnDOMEvents;if(this.global&&events&&(events=="*"||(Array.isArray(events)&&events.length))){this._pauseOnDOMEvents=events;let els=Cc["@mozilla.org/eventlistenerservice;1"].getService(Ci.nsIEventListenerService);els.addListenerForAllEvents(this.global,this._allEventsListener,true);}},_onWindowReady:function(){this._maybeListenToEvents({pauseOnDOMEvents:this._pauseOnDOMEvents});},onResume:function(aRequest){if(this._state!=="paused"){return{error:"wrongState",message:"Can't resume when debuggee isn't paused. Current state is '"
+this._state+"'"};}

if(this._nestedEventLoops.size&&this._nestedEventLoops.lastPausedUrl&&(this._nestedEventLoops.lastPausedUrl!==this._parent.url||this._nestedEventLoops.lastConnection!==this.conn)){return{error:"wrongOrder",message:"trying to resume in the wrong order.",lastPausedUrl:this._nestedEventLoops.lastPausedUrl};}
let resumeLimitHandled;if(aRequest&&aRequest.resumeLimit){resumeLimitHandled=this._handleResumeLimit(aRequest)}else{this._clearSteppingHooks(this.youngestFrame);resumeLimitHandled=resolve(true);}
return resumeLimitHandled.then(()=>{if(aRequest){this._options.pauseOnExceptions=aRequest.pauseOnExceptions;this._options.ignoreCaughtExceptions=aRequest.ignoreCaughtExceptions;this.maybePauseOnExceptions();this._maybeListenToEvents(aRequest);}
let packet=this._resumed();this._popThreadPause();
if(Services.obs){Services.obs.notifyObservers(this,"devtools-thread-resumed",null);}
return packet;},error=>{return error instanceof Error?{error:"unknownError",message:DevToolsUtils.safeErrorString(error)}

:error;});},unsafeSynchronize:function(aPromise){let needNest=true;let eventLoop;let returnVal;aPromise.then((aResolvedVal)=>{needNest=false;returnVal=aResolvedVal;}).then(null,(aError)=>{reportError(aError,"Error inside unsafeSynchronize:");}).then(()=>{if(eventLoop){eventLoop.resolve();}});if(needNest){eventLoop=this._nestedEventLoops.push();eventLoop.enter();}
return returnVal;},maybePauseOnExceptions:function(){if(this._options.pauseOnExceptions){this.dbg.onExceptionUnwind=this.onExceptionUnwind.bind(this);}},_allEventsListener:function(event){if(this._pauseOnDOMEvents=="*"||this._pauseOnDOMEvents.indexOf(event.type)!=-1){for(let listener of this._getAllEventListeners(event.target)){if(event.type==listener.type||this._pauseOnDOMEvents=="*"){this._breakOnEnter(listener.script);}}}},_getAllEventListeners:function(eventTarget){let els=Cc["@mozilla.org/eventlistenerservice;1"].getService(Ci.nsIEventListenerService);let targets=els.getEventTargetChainFor(eventTarget);let listeners=[];for(let target of targets){let handlers=els.getListenerInfoFor(target);for(let handler of handlers){

if(!handler||!handler.listenerObject||!handler.type)
continue;let l=Object.create(null);l.type=handler.type;let listener=handler.listenerObject;let listenerDO=this.globalDebugObject.makeDebuggeeValue(listener);if(listenerDO.class=="Object"||listenerDO.class=="XULElement"){
if(!listenerDO.unwrap()){continue;}
let heDesc;while(!heDesc&&listenerDO){heDesc=listenerDO.getOwnPropertyDescriptor("handleEvent");listenerDO=listenerDO.proto;}
if(heDesc&&heDesc.value){listenerDO=heDesc.value;}}

while(listenerDO.isBoundFunction){listenerDO=listenerDO.boundTargetFunction;}
l.script=listenerDO.script;
if(!l.script)
continue;listeners.push(l);}}
return listeners;},_breakOnEnter:function(script){let offsets=script.getAllOffsets();for(let line=0,n=offsets.length;line<n;line++){if(offsets[line]){
let actor=new BreakpointActor(this);this.threadLifetimePool.addActor(actor);let scripts=this.scripts.getScriptsBySourceAndLine(script.source,line);let entryPoints=findEntryPointsForLine(scripts,line);setBreakpointAtEntryPoints(actor,entryPoints);this._hiddenBreakpoints.set(actor.actorID,actor);break;}}},_getNextStepFrame:function(aFrame){let stepFrame=aFrame.reportedPop?aFrame.older:aFrame;if(!stepFrame||!stepFrame.script){stepFrame=null;}
return stepFrame;},onClientEvaluate:function(aRequest){if(this.state!=="paused"){return{error:"wrongState",message:"Debuggee must be paused to evaluate code."};}
let frame=this._requestFrame(aRequest.frame);if(!frame){return{error:"unknownFrame",message:"Evaluation frame not found"};}
if(!frame.environment){return{error:"notDebuggee",message:"cannot access the environment of this frame."};}
let youngest=this.youngestFrame;let resumedPacket=this._resumed();this.conn.send(resumedPacket); let completion=frame.eval(aRequest.expression);let packet=this._paused(youngest);packet.why={type:"clientEvaluated",frameFinished:this.createProtocolCompletionValue(completion)};return packet;},onFrames:function(aRequest){if(this.state!=="paused"){return{error:"wrongState",message:"Stack frames are only available while the debuggee is paused."};}
let start=aRequest.start?aRequest.start:0;let count=aRequest.count;let frame=this.youngestFrame;let i=0;while(frame&&(i<start)){frame=frame.older;i++;}

let promises=[];for(;frame&&(!count||i<(start+count));i++,frame=frame.older){let form=this._createFrameActor(frame).form();form.depth=i;let promise=this.sources.getOriginalLocation(new GeneratedLocation(this.sources.createNonSourceMappedActor(frame.script.source),form.where.line,form.where.column)).then((originalLocation)=>{if(!originalLocation.originalSourceActor){return null;}
let sourceForm=originalLocation.originalSourceActor.form();form.where={source:sourceForm,line:originalLocation.originalLine,column:originalLocation.originalColumn};form.source=sourceForm;return form;});promises.push(promise);}
return all(promises).then(function(frames){return{frames:frames.filter(x=>!!x)};});},onReleaseMany:function(aRequest){if(!aRequest.actors){return{error:"missingParameter",message:"no actors were specified"};}
let res;for(let actorID of aRequest.actors){let actor=this.threadLifetimePool.get(actorID);if(!actor){if(!res){res={error:"notReleasable",message:"Only thread-lifetime actors can be released."};}
continue;}
actor.onRelease();}
return res?res:{};},_discoverSources:function(){const sourcesToScripts=new Map();const scripts=this.scripts.getAllScripts();for(let i=0,len=scripts.length;i<len;i++){let s=scripts[i];if(s.source){sourcesToScripts.set(s.source,s);}}
return all([...sourcesToScripts.values()].map(script=>{return this.sources.createSourceActors(script.source);}));},onSources:function(aRequest){return this._discoverSources().then(()=>{


return{sources:this.sources.iter().map(s=>s.form())};});},disableAllBreakpoints:function(){for(let bpActor of this.breakpointActorMap.findActors()){bpActor.removeScripts();}},onInterrupt:function(aRequest){if(this.state=="exited"){return{type:"exited"};}else if(this.state=="paused"){return{type:"paused",why:{type:"alreadyPaused"}};}else if(this.state!="running"){return{error:"wrongState",message:"Received interrupt request in "+this.state+" state."};}
try{
if(aRequest.when=="onNext"){let onEnterFrame=(aFrame)=>{return this._pauseAndRespond(aFrame,{type:"interrupted",onNext:true});};this.dbg.onEnterFrame=onEnterFrame;return{type:"willInterrupt"};}

let packet=this._paused();if(!packet){return{error:"notInterrupted"};}
packet.why={type:"interrupted"};

this.conn.send(packet);this._pushThreadPause();
return null;}catch(e){reportError(e);return{error:"notInterrupted",message:e.toString()};}},onEventListeners:function(aRequest){if(!this.global){return{error:"notImplemented",message:"eventListeners request is only supported in content debugging"};}
let els=Cc["@mozilla.org/eventlistenerservice;1"].getService(Ci.nsIEventListenerService);let nodes=this.global.document.getElementsByTagName("*");nodes=[this.global].concat([].slice.call(nodes));let listeners=[];for(let node of nodes){let handlers=els.getListenerInfoFor(node);for(let handler of handlers){let listenerForm=Object.create(null);let listener=handler.listenerObject;
if(!listener||!handler.type){continue;}
let selector=node.tagName?CssLogic.findCssSelector(node):"window";let nodeDO=this.globalDebugObject.makeDebuggeeValue(node);listenerForm.node={selector:selector,object:createValueGrip(nodeDO,this._pausePool,this.objectGrip)};listenerForm.type=handler.type;listenerForm.capturing=handler.capturing;listenerForm.allowsUntrusted=handler.allowsUntrusted;listenerForm.inSystemEventGroup=handler.inSystemEventGroup;let handlerName="on"+listenerForm.type;listenerForm.isEventHandler=false;if(typeof node.hasAttribute!=="undefined"){listenerForm.isEventHandler=!!node.hasAttribute(handlerName);}
if(!!node[handlerName]){listenerForm.isEventHandler=!!node[handlerName];}
let listenerDO=this.globalDebugObject.makeDebuggeeValue(listener);if(listenerDO.class=="Object"||listenerDO.class=="XULElement"){
if(!listenerDO.unwrap()){continue;}
let heDesc;while(!heDesc&&listenerDO){heDesc=listenerDO.getOwnPropertyDescriptor("handleEvent");listenerDO=listenerDO.proto;}
if(heDesc&&heDesc.value){listenerDO=heDesc.value;}}

while(listenerDO.isBoundFunction){listenerDO=listenerDO.boundTargetFunction;}
listenerForm.function=createValueGrip(listenerDO,this._pausePool,this.objectGrip);listeners.push(listenerForm);}}
return{listeners:listeners};},_requestFrame:function(aFrameID){if(!aFrameID){return this.youngestFrame;}
if(this._framePool.has(aFrameID)){return this._framePool.get(aFrameID).frame;}
return undefined;},_paused:function(aFrame){



if(this.state==="paused"){return undefined;}
this.dbg.onEnterFrame=undefined;this.dbg.onExceptionUnwind=undefined;if(aFrame){aFrame.onStep=undefined;aFrame.onPop=undefined;}

if(!isWorker&&this.global&&!this.global.toString().includes("Sandbox")){let els=Cc["@mozilla.org/eventlistenerservice;1"].getService(Ci.nsIEventListenerService);els.removeListenerForAllEvents(this.global,this._allEventsListener,true);for(let[,bp]of this._hiddenBreakpoints){bp.delete();}
this._hiddenBreakpoints.clear();}
this._state="paused";
assert(!this._pausePool,"No pause pool should exist yet");this._pausePool=new ActorPool(this.conn);this.conn.addActorPool(this._pausePool);
this._pausePool.threadActor=this;assert(!this._pauseActor,"No pause actor should exist yet");this._pauseActor=new PauseActor(this._pausePool);this._pausePool.addActor(this._pauseActor);let poppedFrames=this._updateFrames();let packet={from:this.actorID,type:"paused",actor:this._pauseActor.actorID};if(aFrame){packet.frame=this._createFrameActor(aFrame).form();}
if(poppedFrames){packet.poppedFrames=poppedFrames;}
return packet;},_resumed:function(){this._state="running";this.conn.removeActorPool(this._pausePool);this._pausePool=null;this._pauseActor=null;return{from:this.actorID,type:"resumed"};},_updateFrames:function(){let popped=[];let framePool=new ActorPool(this.conn);let frameList=[];for(let frameActor of this._frameActors){if(frameActor.frame.live){framePool.addActor(frameActor);frameList.push(frameActor);}else{popped.push(frameActor.actorID);}}

if(this._framePool){this.conn.removeActorPool(this._framePool);}
this._frameActors=frameList;this._framePool=framePool;this.conn.addActorPool(framePool);return popped;},_createFrameActor:function(aFrame){if(aFrame.actor){return aFrame.actor;}
let actor=new FrameActor(aFrame,this);this._frameActors.push(actor);this._framePool.addActor(actor);aFrame.actor=actor;return actor;},createEnvironmentActor:function(aEnvironment,aPool){if(!aEnvironment){return undefined;}
if(aEnvironment.actor){return aEnvironment.actor;}
let actor=new EnvironmentActor(aEnvironment,this);aPool.addActor(actor);aEnvironment.actor=actor;return actor;},createProtocolCompletionValue:function(aCompletion){let protoValue={};if(aCompletion==null){protoValue.terminated=true;}else if("return"in aCompletion){protoValue.return=createValueGrip(aCompletion.return,this._pausePool,this.objectGrip);}else if("throw"in aCompletion){protoValue.throw=createValueGrip(aCompletion.throw,this._pausePool,this.objectGrip);}else{protoValue.return=createValueGrip(aCompletion.yield,this._pausePool,this.objectGrip);}
return protoValue;},objectGrip:function(aValue,aPool){if(!aPool.objectActors){aPool.objectActors=new WeakMap();}
if(aPool.objectActors.has(aValue)){return aPool.objectActors.get(aValue).grip();}else if(this.threadLifetimePool.objectActors.has(aValue)){return this.threadLifetimePool.objectActors.get(aValue).grip();}
let actor=new PauseScopedObjectActor(aValue,{getGripDepth:()=>this._gripDepth,incrementGripDepth:()=>this._gripDepth++,decrementGripDepth:()=>this._gripDepth--,createValueGrip:v=>createValueGrip(v,this._pausePool,this.pauseObjectGrip),sources:()=>this.sources,createEnvironmentActor:(env,pool)=>this.createEnvironmentActor(env,pool),promote:()=>this.threadObjectGrip(actor),isThreadLifetimePool:()=>actor.registeredPool!==this.threadLifetimePool,getGlobalDebugObject:()=>this.globalDebugObject});aPool.addActor(actor);aPool.objectActors.set(aValue,actor);return actor.grip();},pauseObjectGrip:function(aValue){if(!this._pausePool){throw"Object grip requested while not paused.";}
return this.objectGrip(aValue,this._pausePool);},threadObjectGrip:function(aActor){
aActor.registeredPool.objectActors.delete(aActor.obj);this.threadLifetimePool.addActor(aActor);this.threadLifetimePool.objectActors.set(aActor.obj,aActor);},onThreadGrips:function(aRequest){if(this.state!="paused"){return{error:"wrongState"};}
if(!aRequest.actors){return{error:"missingParameter",message:"no actors were specified"};}
for(let actorID of aRequest.actors){let actor=this._pausePool.get(actorID);if(actor){this.threadObjectGrip(actor);}}
return{};},pauseLongStringGrip:function(aString){return longStringGrip(aString,this._pausePool);},threadLongStringGrip:function(aString){return longStringGrip(aString,this._threadLifetimePool);},uncaughtExceptionHook:function(aException){dumpn("Got an exception: "+aException.message+"\n"+aException.stack);},onDebuggerStatement:function(aFrame){
const generatedLocation=this.sources.getFrameLocation(aFrame);const{originalSourceActor}=this.unsafeSynchronize(this.sources.getOriginalLocation(generatedLocation));const url=originalSourceActor?originalSourceActor.url:null;return this.sources.isBlackBoxed(url)||aFrame.onStep?undefined:this._pauseAndRespond(aFrame,{type:"debuggerStatement"});},onExceptionUnwind:function(aFrame,aValue){let willBeCaught=false;for(let frame=aFrame;frame!=null;frame=frame.older){if(frame.script.isInCatchScope(frame.offset)){willBeCaught=true;break;}}
if(willBeCaught&&this._options.ignoreCaughtExceptions){return undefined;}

if(aValue==Cr.NS_ERROR_NO_INTERFACE){return undefined;}
const generatedLocation=this.sources.getFrameLocation(aFrame);const{originalSourceActor}=this.unsafeSynchronize(this.sources.getOriginalLocation(generatedLocation));const url=originalSourceActor?originalSourceActor.url:null;if(this.sources.isBlackBoxed(url)){return undefined;}
try{let packet=this._paused(aFrame);if(!packet){return undefined;}
packet.why={type:"exception",exception:createValueGrip(aValue,this._pausePool,this.objectGrip)};this.conn.send(packet);this._pushThreadPause();}catch(e){reportError(e,"Got an exception during TA_onExceptionUnwind: ");}
return undefined;},onNewScript:function(aScript,aGlobal){this._addSource(aScript.source);},onSourceEvent:function(name,source){this.conn.send({from:this._parent.actorID,type:name,source:source.form()}); if(name==="newSource"){this.conn.send({from:this.actorID,type:name,source:source.form()});}},_restoreBreakpoints:function(){if(this.breakpointActorMap.size===0){return;}
for(let s of this.scripts.getSources()){this._addSource(s);}},_addSource:function(aSource){if(!this.sources.allowSource(aSource)||this._debuggerSourcesSeen.has(aSource)){return false;}


this.scripts.addScripts(this.dbg.findScripts({source:aSource}));let sourceActor=this.sources.createNonSourceMappedActor(aSource);let bpActors=[...this.breakpointActorMap.findActors()];if(this._options.useSourceMaps){let promises=[];

let sourceActorsCreated=this.sources._createSourceMappedActors(aSource);if(bpActors.length){



this.unsafeSynchronize(sourceActorsCreated);}
for(let _actor of bpActors){
let actor=_actor;if(actor.isPending){promises.push(actor.originalLocation.originalSourceActor._setBreakpoint(actor));}else{promises.push(this.sources.getAllGeneratedLocations(actor.originalLocation).then((generatedLocations)=>{if(generatedLocations.length>0&&generatedLocations[0].generatedSourceActor.actorID===sourceActor.actorID){sourceActor._setBreakpointAtAllGeneratedLocations(actor,generatedLocations);}}));}}
if(promises.length>0){this.unsafeSynchronize(promise.all(promises));}}else{








for(let actor of bpActors){actor.originalLocation.originalSourceActor._setBreakpoint(actor);}}
this._debuggerSourcesSeen.add(aSource);return true;},onPrototypesAndProperties:function(aRequest){let result={};for(let actorID of aRequest.actors){
let actor=this.conn.getActor(actorID);if(!actor){return{from:this.actorID,error:"noSuchActor"};}
let handler=actor.onPrototypeAndProperties;if(!handler){return{from:this.actorID,error:"unrecognizedPacketType",message:('Actor "'+actorID+'" does not recognize the packet type '+'"prototypeAndProperties"')};}
result[actorID]=handler.call(actor,{});}
return{from:this.actorID,actors:result};}});ThreadActor.prototype.requestTypes=object.merge(ThreadActor.prototype.requestTypes,{"attach":ThreadActor.prototype.onAttach,"detach":ThreadActor.prototype.onDetach,"reconfigure":ThreadActor.prototype.onReconfigure,"resume":ThreadActor.prototype.onResume,"clientEvaluate":ThreadActor.prototype.onClientEvaluate,"frames":ThreadActor.prototype.onFrames,"interrupt":ThreadActor.prototype.onInterrupt,"eventListeners":ThreadActor.prototype.onEventListeners,"releaseMany":ThreadActor.prototype.onReleaseMany,"sources":ThreadActor.prototype.onSources,"threadGrips":ThreadActor.prototype.onThreadGrips,"prototypesAndProperties":ThreadActor.prototype.onPrototypesAndProperties});exports.ThreadActor=ThreadActor;function PauseActor(aPool)
{this.pool=aPool;}
PauseActor.prototype={actorPrefix:"pause"};function PauseScopedActor()
{}
PauseScopedActor.withPaused=function(aMethod){return function(){if(this.isPaused()){return aMethod.apply(this,arguments);}else{return this._wrongState();}};};PauseScopedActor.prototype={isPaused:function(){

return this.threadActor?this.threadActor.state==="paused":true;},_wrongState:function(){return{error:"wrongState",message:this.constructor.name+" actors can only be accessed while the thread is paused."};}};function PauseScopedObjectActor(obj,hooks){ObjectActor.call(this,obj,hooks);this.hooks.promote=hooks.promote;this.hooks.isThreadLifetimePool=hooks.isThreadLifetimePool;}
PauseScopedObjectActor.prototype=Object.create(PauseScopedActor.prototype);Object.assign(PauseScopedObjectActor.prototype,ObjectActor.prototype);Object.assign(PauseScopedObjectActor.prototype,{constructor:PauseScopedObjectActor,actorPrefix:"pausedobj",onOwnPropertyNames:PauseScopedActor.withPaused(ObjectActor.prototype.onOwnPropertyNames),onPrototypeAndProperties:PauseScopedActor.withPaused(ObjectActor.prototype.onPrototypeAndProperties),onPrototype:PauseScopedActor.withPaused(ObjectActor.prototype.onPrototype),onProperty:PauseScopedActor.withPaused(ObjectActor.prototype.onProperty),onDecompile:PauseScopedActor.withPaused(ObjectActor.prototype.onDecompile),onDisplayString:PauseScopedActor.withPaused(ObjectActor.prototype.onDisplayString),onParameterNames:PauseScopedActor.withPaused(ObjectActor.prototype.onParameterNames),onThreadGrip:PauseScopedActor.withPaused(function(aRequest){this.hooks.promote();return{};}),onRelease:PauseScopedActor.withPaused(function(aRequest){if(this.hooks.isThreadLifetimePool()){return{error:"notReleasable",message:"Only thread-lifetime actors can be released."};}
this.release();return{};}),});Object.assign(PauseScopedObjectActor.prototype.requestTypes,{"threadGrip":PauseScopedObjectActor.prototype.onThreadGrip,});function hackDebugger(Debugger){ Debugger.Script.prototype.toString=function(){let output="";if(this.url){output+=this.url;}
if(typeof this.staticLevel!="undefined"){output+=":L"+this.staticLevel;}
if(typeof this.startLine!="undefined"){output+=":"+this.startLine;if(this.lineCount&&this.lineCount>1){output+="-"+(this.startLine+this.lineCount-1);}}
if(typeof this.startLine!="undefined"){output+=":"+this.startLine;if(this.lineCount&&this.lineCount>1){output+="-"+(this.startLine+this.lineCount-1);}}
if(this.strictMode){output+=":strict";}
return output;};Object.defineProperty(Debugger.Frame.prototype,"line",{configurable:true,get:function(){if(this.script){return this.script.getOffsetLocation(this.offset).lineNumber;}else{return null;}}});}
function ChromeDebuggerActor(aConnection,aParent)
{ThreadActor.prototype.initialize.call(this,aParent);}
ChromeDebuggerActor.prototype=Object.create(ThreadActor.prototype);Object.assign(ChromeDebuggerActor.prototype,{constructor:ChromeDebuggerActor,actorPrefix:"chromeDebugger"});exports.ChromeDebuggerActor=ChromeDebuggerActor;function AddonThreadActor(aConnect,aParent){ThreadActor.prototype.initialize.call(this,aParent);}
AddonThreadActor.prototype=Object.create(ThreadActor.prototype);Object.assign(AddonThreadActor.prototype,{constructor:AddonThreadActor,actorPrefix:"addonThread"});exports.AddonThreadActor=AddonThreadActor;var oldReportError=reportError;reportError=function(aError,aPrefix=""){assert(aError instanceof Error,"Must pass Error objects to reportError");let msg=aPrefix+aError.message+":\n"+aError.stack;oldReportError(msg);dumpn(msg);}
function findEntryPointsForLine(scripts,line){const entryPoints=[];for(let script of scripts){const offsets=script.getLineOffsets(line);if(offsets.length){entryPoints.push({script,offsets});}}
return entryPoints;}
exports.unwrapDebuggerObjectGlobal=wrappedGlobal=>{try{



let global=wrappedGlobal.unsafeDereference();Object.getPrototypeOf(global)+"";return global;}
catch(e){return undefined;}};