"use strict";const nsIDialogParamBlock=Components.interfaces.nsIDialogParamBlock;var dialogParams;function onLoad()
{dialogParams=window.arguments[0].QueryInterface(nsIDialogParamBlock);let selectElement=document.getElementById("tokens");let count=dialogParams.GetInt(0);for(let i=0;i<count;i++){let menuItemNode=document.createElement("menuitem");let token=dialogParams.GetString(i);menuItemNode.setAttribute("value",token);menuItemNode.setAttribute("label",token);selectElement.firstChild.appendChild(menuItemNode);if(i==0){selectElement.selectedItem=menuItemNode;}}}
function doOK()
{let tokenList=document.getElementById("tokens");dialogParams.SetInt(0,1);dialogParams.SetString(0,tokenList.value);return true;}
function doCancel()
{dialogParams.SetInt(0,0);return true;}