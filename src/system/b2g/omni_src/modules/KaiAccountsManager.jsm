"use strict";this.EXPORTED_SYMBOLS=["KaiAccountsManager"];const{classes:Cc,interfaces:Ci,utils:Cu}=Components;Cu.import("resource://gre/modules/XPCOMUtils.jsm");Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/KaiAccounts.jsm");Cu.import("resource://gre/modules/Promise.jsm");Cu.import("resource://gre/modules/KaiAccountsCommon.js");XPCOMUtils.defineLazyServiceGetter(this,"permissionManager","@mozilla.org/permissionmanager;1","nsIPermissionManager");this.KaiAccountsManager={init:function(){Services.obs.addObserver(this,ONLOGOUT_NOTIFICATION,false);Services.obs.addObserver(this,ON_FXA_UPDATE_NOTIFICATION,false);},observe:function(aSubject,aTopic,aData){ this._activeSession=null;if(aData==ONVERIFIED_NOTIFICATION){log.debug("KaiAccountsManager: cache cleared, broadcasting: "+aData);Services.obs.notifyObservers(null,aData,null);}},
_kaiAccounts:kaiAccounts,
_activeSession:null,_otpId:null,_refreshing:false,get _user(){if(!this._activeSession||!this._activeSession.accountId){return null;}
return{accountId:this._activeSession.accountId,birthday:this._activeSession.birthday,gender:this._activeSession.gender,verified:this._activeSession.verified}},_error:function(aErrorString){log.error(aErrorString);let reason={error:aErrorString};return Promise.reject(reason);},_getError:function(aServerResponse){if(!aServerResponse||!aServerResponse.error||!aServerResponse.error.errno){return;}
let error=SERVER_ERRNO_TO_ERROR[aServerResponse.error.errno];return error;},_serverError:function(aServerResponse){let error=this._getError({error:aServerResponse});return this._error(error?error:ERROR_SERVER_ERROR);},



_getKaiAccountsClient:function(){return this._kaiAccounts.getAccountsClient();},_signIn:function(aAccountId,aPassword){if(Services.io.offline){return this._error(ERROR_OFFLINE);}
if(!aAccountId){return this._error(ERROR_INVALID_ACCOUNTID);}
if(!aPassword){return this._error(ERROR_INVALID_PASSWORD);}
let client=this._getKaiAccountsClient();return this._kaiAccounts.getSignedInUser().then(user=>{if(!this._otpId){return client.signIn(aAccountId,aPassword);}else{return client.signInWithOtp(aAccountId,aPassword,this._otpId);}}).then(user=>{let error=this._getError(user);if(error){return this._error(error);}
this._otpId=null;this._activeSession=null;return this._kaiAccounts.setSignedInUser(user).then(()=>{log.debug("User signed in: "+JSON.stringify(this._user));return this._kaiAccounts.getAccountInfo().then(accountInfo=>{},reason=>{log.error("Obtaining account info failed reason "+JSON.stringify(reason));}).then(()=>{return Promise.resolve({accountCreated:false,user:this._user});});});},reason=>{return this._serverError(reason);});},_signUp:function(aAccountId,aPassword,aInfo){if(Services.io.offline){return this._error(ERROR_OFFLINE);}
if(!aAccountId){return this._error(ERROR_INVALID_ACCOUNTID);}
if(!aPassword){return this._error(ERROR_INVALID_PASSWORD);}
let client=this._getKaiAccountsClient();return this._kaiAccounts.getSignedInUser().then(user=>{return client.signUp(aAccountId,aPassword,aInfo);}).then(user=>{let error=this._getError(user);if(error){return this._error(error);}
log.debug("User signed Up: "+JSON.stringify(this._user));return Promise.resolve({accountCreated:true,user:this._user});},reason=>{return this._serverError(reason);});},_handleGetAssertionError:function(reason,aAudience,aPrincipal){log.debug("KaiAccountsManager._handleGetAssertionError()");return this._localSignOut().then(()=>{return this._uiRequest(UI_REQUEST_SIGN_IN_FLOW,aAudience,aPrincipal);},(reason)=>{ log.error("Signing out in response to server error threw: "+
reason);return this._error(reason);});},_getAssertion:function(aAudience,aPrincipal){return this._kaiAccounts.getAssertion(aAudience).then((result)=>{if(aPrincipal){this._addPermission(aPrincipal);}
return result;},(reason)=>{return this._handleGetAssertionError(reason,aAudience,aPrincipal);});},_refreshAuthentication:function(aAudience,aAccountId,aPrincipal,logoutOnFailure=false){this._refreshing=true;return this._uiRequest(UI_REQUEST_REFRESH_AUTH,aAudience,aPrincipal,aAccountId).then((assertion)=>{this._refreshing=false;return assertion;},(reason)=>{this._refreshing=false;if(logoutOnFailure){return this._signOut(this._activeSession).then(()=>{return this._error(reason);});}
return this._error(reason);});},_localSignOut:function(){return this._kaiAccounts.signOut(true);},_signOut:function(user){if(Services.io.offline){return this._error(ERROR_OFFLINE);}
let client=this._getKaiAccountsClient();return this._kaiAccounts.getCertificate(user).then(cert=>{return client.signOut(cert).then(result=>{let error=this._getError(result);if(error){return this._error(error);}
return this._localSignOut().then(()=>{log.debug("Signed out");return Promise.resolve();});},reason=>{log.error("Sign out account failed reason "+JSON.stringify(reason));return this._serverError(reason);});});},_uiRequest:function(aRequest,aAudience,aPrincipal,aParams){if(Services.io.offline){return this._error(ERROR_OFFLINE);}
let ui=Cc["@mozilla.org/fxaccounts/fxaccounts-ui-glue;1"].createInstance(Ci.nsIFxAccountsUIGlue);if(!ui[aRequest]){return this._error(ERROR_UI_REQUEST);}
if(!aParams||!Array.isArray(aParams)){aParams=[aParams];}
return ui[aRequest].apply(this,aParams).then(result=>{
return this.getAccount().then(user=>{if(user&&user.verified){return this._getAssertion(aAudience,aPrincipal);}
return this._error(ERROR_UNVERIFIED_ACCOUNT);});},error=>{return this._error(ERROR_UI_ERROR);});},_addPermission:function(aPrincipal){
try{permissionManager.addFromPrincipal(aPrincipal,KAIACCOUNTS_PERMISSION,Ci.nsIPermissionManager.ALLOW_ACTION);}catch(e){log.warn("Could not add permission "+e);}},signIn:function(aAccountId,aPassword){return this._signIn(aAccountId,aPassword);},signUp:function(aAccountId,aPassword,aInfo){return this._signUp(aAccountId,aPassword,aInfo);},signOut:function(){if(!this._activeSession){
return this.getAccount().then(result=>{if(!result){return Promise.resolve();}
return this._signOut(result);});}
return this._signOut(this._activeSession);},resendVerificationEmail:function(){return this._kaiAccounts.resendVerificationEmail().then((result)=>{return result;},(error)=>{return this._error(ERROR_SERVER_ERROR);});},requestVerificationOtp:function(aAccountId){if(Services.io.offline){return this._error(ERROR_OFFLINE);}
if(!aAccountId){return this._error(ERROR_INVALID_ACCOUNTID);}
let client=this._getKaiAccountsClient();return client.requestVerificationOtp(aAccountId).then(result=>{let error=this._getError(result);if(error){return this._error(error,result);} 
this._otpId=result.id;return Promise.resolve();},reason=>{return this._serverError(reason);});},getAccount:function(){if(this._activeSession){log.debug("Account "+JSON.stringify(this._user));return Promise.resolve(this._user);}
return this._kaiAccounts.getSignedInUser().then(user=>{if(!user||!user.accountId){log.debug("No signed in account");return Promise.resolve(null);}
this._activeSession=user;log.debug("Account "+JSON.stringify(this._user));return Promise.resolve(this._user);});},changePassword:function(aOldPassword,aNewPassword){if(Services.io.offline){return this._error(ERROR_OFFLINE);}
if(!aOldPassword||!aNewPassword){return this._error(ERROR_INVALID_PASSWORD);}
if(aOldPassword==aNewPassword){return this._error(ERROR_INVALID_PASSWORD);}
return this._kaiAccounts.changePassword(aOldPassword,aNewPassword).then(result=>{return Promise.resolve();},reason=>{log.error("Changing password failed reason "+JSON.stringify(reason));return this._serverError(reason);});},queryAccount:function(aAccountId){log.debug("queryAccount "+aAccountId);if(Services.io.offline){return this._error(ERROR_OFFLINE);}
let deferred=Promise.defer();if(!aAccountId){return this._error(ERROR_INVALID_ACCOUNTID);}
let client=this._getKaiAccountsClient();return client.accountExists(aAccountId).then(result=>{log.debug("Account"+(result?"":" does not")+" exists");let error=this._getError(result);if(error){return this._error(error);}
return Promise.resolve({registered:result});},reason=>{this._serverError(reason);});},getAssertion:function(aAudience,aPrincipal,aOptions){if(!aAudience){return this._error(ERROR_INVALID_AUDIENCE);}
let secMan=Cc["@mozilla.org/scriptsecuritymanager;1"].getService(Ci.nsIScriptSecurityManager);let uri=Services.io.newURI(aPrincipal.origin,null,null);log.debug("KaiAccountsManager.getAssertion() aPrincipal: ",aPrincipal.origin,aPrincipal.appId,aPrincipal.isInBrowserElement);let principal=secMan.getAppCodebasePrincipal(uri,aPrincipal.appId,aPrincipal.isInBrowserElement);return this.getAccount().then(user=>{if(user){if(!user.verified){return this._error(ERROR_UNVERIFIED_ACCOUNT);}
if(aOptions&&(typeof(aOptions.refreshAuthentication)!="undefined")){let gracePeriod=aOptions.refreshAuthentication;if(typeof(gracePeriod)!=="number"||isNaN(gracePeriod)){return this._error(ERROR_INVALID_REFRESH_AUTH_VALUE);}
if(aOptions.silent){return this._error(ERROR_NO_SILENT_REFRESH_AUTH);}
return this._refreshAuthentication(aAudience,user.accountId,principal,false );}





let permission=permissionManager.testPermissionFromPrincipal(principal,KAIACCOUNTS_PERMISSION);if(permission==Ci.nsIPermissionManager.PROMPT_ACTION&&!this._refreshing){return this._refreshAuthentication(aAudience,user.accountId,principal,false );}else if(permission==Ci.nsIPermissionManager.DENY_ACTION&&!this._refreshing){return this._error(ERROR_PERMISSION_DENIED);}
return this._getAssertion(aAudience,principal);}
log.debug("No signed in user");if(aOptions&&aOptions.silent){return Promise.resolve(null);}
return this._uiRequest(UI_REQUEST_SIGN_IN_FLOW,aAudience,principal);});}};KaiAccountsManager.init();