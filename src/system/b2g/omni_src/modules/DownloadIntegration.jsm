"use strict";this.EXPORTED_SYMBOLS=["DownloadIntegration",];const Cc=Components.classes;const Ci=Components.interfaces;const Cu=Components.utils;const Cr=Components.results;Cu.import("resource://gre/modules/Integration.jsm");Cu.import("resource://gre/modules/XPCOMUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"AsyncShutdown","resource://gre/modules/AsyncShutdown.jsm");XPCOMUtils.defineLazyModuleGetter(this,"DeferredTask","resource://gre/modules/DeferredTask.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Downloads","resource://gre/modules/Downloads.jsm");XPCOMUtils.defineLazyModuleGetter(this,"DownloadStore","resource://gre/modules/DownloadStore.jsm");XPCOMUtils.defineLazyModuleGetter(this,"DownloadImport","resource://gre/modules/DownloadImport.jsm");XPCOMUtils.defineLazyModuleGetter(this,"DownloadUIHelper","resource://gre/modules/DownloadUIHelper.jsm");XPCOMUtils.defineLazyModuleGetter(this,"FileUtils","resource://gre/modules/FileUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"NetUtil","resource://gre/modules/NetUtil.jsm");XPCOMUtils.defineLazyModuleGetter(this,"OS","resource://gre/modules/osfile.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Promise","resource://gre/modules/Promise.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Services","resource://gre/modules/Services.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Task","resource://gre/modules/Task.jsm");XPCOMUtils.defineLazyModuleGetter(this,"NetUtil","resource://gre/modules/NetUtil.jsm");XPCOMUtils.defineLazyServiceGetter(this,"gDownloadPlatform","@mozilla.org/toolkit/download-platform;1","mozIDownloadPlatform");XPCOMUtils.defineLazyServiceGetter(this,"gEnvironment","@mozilla.org/process/environment;1","nsIEnvironment");XPCOMUtils.defineLazyServiceGetter(this,"gMIMEService","@mozilla.org/mime;1","nsIMIMEService");XPCOMUtils.defineLazyServiceGetter(this,"gExternalProtocolService","@mozilla.org/uriloader/external-protocol-service;1","nsIExternalProtocolService");XPCOMUtils.defineLazyServiceGetter(this,"gNetworkManager","@mozilla.org/network/manager;1","nsINetworkManager");XPCOMUtils.defineLazyGetter(this,"gParentalControlsService",function(){if("@mozilla.org/parental-controls-service;1"in Cc){return Cc["@mozilla.org/parental-controls-service;1"].createInstance(Ci.nsIParentalControlsService);}
return null;});XPCOMUtils.defineLazyServiceGetter(this,"gApplicationReputationService","@mozilla.org/downloads/application-reputation-service;1",Ci.nsIApplicationReputationService);XPCOMUtils.defineLazyServiceGetter(this,"volumeService","@mozilla.org/telephony/volume-service;1","nsIVolumeService");
Integration.downloads.defineModuleGetter(this,"gCombinedDownloadIntegration","resource://gre/modules/DownloadIntegration.jsm","DownloadIntegration");const Timer=Components.Constructor("@mozilla.org/timer;1","nsITimer","initWithCallback");const kSaveDelayMs=1500;const kPrefImportedFromSqlite="browser.download.importedFromSqlite";const kObserverTopics=["quit-application-requested","offline-requested","last-pb-context-exiting","last-pb-context-exited","sleep_notification","suspend_process_notification","wake_notification","resume_process_notification","network-active-changed","xpcom-will-shutdown",];const kVerdictMap={[Ci.nsIApplicationReputationService.VERDICT_DANGEROUS]:Downloads.Error.BLOCK_VERDICT_MALWARE,[Ci.nsIApplicationReputationService.VERDICT_UNCOMMON]:Downloads.Error.BLOCK_VERDICT_UNCOMMON,[Ci.nsIApplicationReputationService.VERDICT_POTENTIALLY_UNWANTED]:Downloads.Error.BLOCK_VERDICT_POTENTIALLY_UNWANTED,[Ci.nsIApplicationReputationService.VERDICT_DANGEROUS_HOST]:Downloads.Error.BLOCK_VERDICT_MALWARE,}; this.DownloadIntegration={_store:null,shouldKeepBlockedData(){const FIREFOX_ID="{ec8030f7-c20a-464f-9b0e-13a3a9e97384}";return Services.appinfo.ID==FIREFOX_ID;},initializePublicDownloadList:Task.async(function*(list){try{yield this.loadPublicDownloadListFromStore(list);}catch(ex){Cu.reportError(ex);}


new DownloadHistoryObserver(list);}),loadPublicDownloadListFromStore:Task.async(function*(list){if(this._store){throw new Error("Initialization may be performed only once.");}
this._store=new DownloadStore(list,OS.Path.join(OS.Constants.Path.profileDir,"downloads.json"));this._store.onsaveitem=this.shouldPersistDownload.bind(this);try{if(this._importedFromSqlite){yield this._store.load();}else{let sqliteDBpath=OS.Path.join(OS.Constants.Path.profileDir,"downloads.sqlite");if(yield OS.File.exists(sqliteDBpath)){let sqliteImport=new DownloadImport(list,sqliteDBpath);yield sqliteImport.import();let importCount=(yield list.getAll()).length;if(importCount>0){try{yield this._store.save();}catch(ex){}}
OS.File.remove(sqliteDBpath).then(null,Cu.reportError);}
Services.prefs.setBoolPref(kPrefImportedFromSqlite,true);
OS.File.remove(OS.Path.join(OS.Constants.Path.profileDir,"downloads.rdf")).catch(()=>{});}}catch(ex){Cu.reportError(ex);}


yield new DownloadAutoSaveView(list,this._store).initialize();}),_getDefaultDownloadDirectory:Task.async(function*(){let directoryPath;let win=Services.wm.getMostRecentWindow("navigator:browser");let storages=win.navigator.getDeviceStorages("sdcard");let preferredStorageName;storages.forEach((aStorage)=>{if(aStorage.default||!preferredStorageName){preferredStorageName=aStorage.storageName;}});if(preferredStorageName){let volume=volumeService.getVolumeByName(preferredStorageName);if(volume&&volume.state===Ci.nsIVolume.STATE_MOUNTED){directoryPath=OS.Path.join(volume.mountPoint,"downloads");yield OS.File.makeDir(directoryPath,{ignoreExisting:true});}}
if(directoryPath){return directoryPath;}else{throw new Components.Exception("No suitable storage for downloads.",Cr.NS_ERROR_FILE_UNRECOGNIZED_PATH);}}),shouldPersistDownload(aDownload){

if(!aDownload.stopped||aDownload.hasPartialData||aDownload.hasBlockedData){return true;}
let maxTime=Date.now()-
Services.prefs.getIntPref("dom.downloads.max_retention_days")*24*60*60*1000;return aDownload.startTime>maxTime;},getSystemDownloadsDirectory:Task.async(function*(){if(this._downloadsDirectory){return this._downloadsDirectory;}
let directoryPath=null;directoryPath=this._getDefaultDownloadDirectory();this._downloadsDirectory=directoryPath;return this._downloadsDirectory;}),_downloadsDirectory:null,getPreferredDownloadsDirectory:Task.async(function*(){let directoryPath=null;directoryPath=this._getDefaultDownloadDirectory();return directoryPath;}),getTemporaryDownloadsDirectory:Task.async(function*(){let directoryPath=null;directoryPath=yield this.getSystemDownloadsDirectory();return directoryPath;}),shouldBlockForParentalControls(aDownload){let isEnabled=gParentalControlsService&&gParentalControlsService.parentalControlsEnabled;let shouldBlock=isEnabled&&gParentalControlsService.blockFileDownloadsEnabled;if(isEnabled&&gParentalControlsService.loggingEnabled){gParentalControlsService.log(gParentalControlsService.ePCLog_FileDownload,shouldBlock,NetUtil.newURI(aDownload.source.url),null);}
return Promise.resolve(shouldBlock);},shouldBlockForRuntimePermissions(){return Promise.resolve(false);},shouldBlockForReputationCheck(aDownload){let hash;let sigInfo;let channelRedirects;try{hash=aDownload.saver.getSha256Hash();sigInfo=aDownload.saver.getSignatureInfo();channelRedirects=aDownload.saver.getRedirects();}catch(ex){return Promise.resolve({shouldBlock:false,verdict:"",});}
if(!hash||!sigInfo){return Promise.resolve({shouldBlock:false,verdict:"",});}
let deferred=Promise.defer();let aReferrer=null;if(aDownload.source.referrer){aReferrer:NetUtil.newURI(aDownload.source.referrer);}
gApplicationReputationService.queryReputation({sourceURI:NetUtil.newURI(aDownload.source.url),referrerURI:aReferrer,fileSize:aDownload.currentBytes,sha256Hash:hash,suggestedFileName:OS.Path.basename(aDownload.target.path),signatureInfo:sigInfo,redirects:channelRedirects},function onComplete(aShouldBlock,aRv,aVerdict){deferred.resolve({shouldBlock:aShouldBlock,verdict:(aShouldBlock&&kVerdictMap[aVerdict])||"",});});return deferred.promise;},downloadDone:Task.async(function*(aDownload){



try{


let isTemporaryDownload=aDownload.launchWhenSucceeded&&(aDownload.source.isPrivate||Services.prefs.getBoolPref("browser.helperApps.deleteTempFileOnExit"));
let options={};if(isTemporaryDownload){options.unixMode=0o400;options.winAttributes={readOnly:true};}else{options.unixMode=0o666;}
yield OS.File.setPermissions(aDownload.target.path,options);}catch(ex){


if(!(ex instanceof OS.File.Error)||ex.unixErrno!=OS.Constants.libc.EPERM){Cu.reportError(ex);}}
gDownloadPlatform.downloadDone(NetUtil.newURI(aDownload.source.url),new FileUtils.File(aDownload.target.path),aDownload.contentType,aDownload.source.isPrivate);}),launchDownload:Task.async(function*(aDownload){let file=new FileUtils.File(aDownload.target.path);




if(file.isExecutable()&&!(yield this.confirmLaunchExecutable(file.path))){return;}


let fileExtension=null,mimeInfo=null;let match=file.leafName.match(/\.([^.]+)$/);if(match){fileExtension=match[1];}
try{

mimeInfo=gMIMEService.getFromTypeAndExtension(aDownload.contentType,fileExtension);}catch(e){}
if(aDownload.launcherPath){if(!mimeInfo){

throw new Error("Unable to create nsIMIMEInfo to launch a custom application");} 
let localHandlerApp=Cc["@mozilla.org/uriloader/local-handler-app;1"].createInstance(Ci.nsILocalHandlerApp);localHandlerApp.executable=new FileUtils.File(aDownload.launcherPath);mimeInfo.preferredApplicationHandler=localHandlerApp;mimeInfo.preferredAction=Ci.nsIMIMEInfo.useHelperApp;this.launchFile(file,mimeInfo);return;}

if(mimeInfo){mimeInfo.preferredAction=Ci.nsIMIMEInfo.useSystemDefault;try{this.launchFile(file,mimeInfo);return;}catch(ex){}}
try{this.launchFile(file);return;}catch(ex){}

gExternalProtocolService.loadUrl(NetUtil.newURI(file));}),confirmLaunchExecutable:Task.async(function*(path){

return yield DownloadUIHelper.getPrompter().confirmLaunchExecutable(path);}),launchFile(file,mimeInfo){if(mimeInfo){mimeInfo.launchWithFile(file);}else{file.launch();}},showContainingDirectory:Task.async(function*(aFilePath){let file=new FileUtils.File(aFilePath);try{file.reveal();return;}catch(ex){}

let parent=file.parent;if(!parent){throw new Error("Unexpected reference to a top-level directory instead of a file");}
try{parent.launch();return;}catch(ex){}

gExternalProtocolService.loadUrl(NetUtil.newURI(parent));}),_createDownloadsDirectory(aName){

let directoryPath=OS.Path.join(this._getDirectory(aName),DownloadUIHelper.strings.downloadsFolder);return OS.File.makeDir(directoryPath,{ignoreExisting:true}).then(()=>directoryPath);},_getDirectory(name){return Services.dirsvc.get(name,Ci.nsIFile).path;},addListObservers(aList,aIsPrivate){DownloadObserver.registerView(aList,aIsPrivate);if(!DownloadObserver.observersAdded){DownloadObserver.observersAdded=true;for(let topic of kObserverTopics){Services.obs.addObserver(DownloadObserver,topic,false);}}
return Promise.resolve();},forceSave(){if(this._store){return this._store.save();}
return Promise.resolve();},get _importedFromSqlite(){try{return Services.prefs.getBoolPref(kPrefImportedFromSqlite);}catch(ex){return false;}},};this.DownloadObserver={observersAdded:false,_wakeTimer:null,_publicInProgressDownloads:new Set(),_privateInProgressDownloads:new Set(),_canceledOfflineDownloads:new Set(),registerView:function DO_registerView(aList,aIsPrivate){let downloadsSet=aIsPrivate?this._privateInProgressDownloads:this._publicInProgressDownloads;let downloadsView={onDownloadAdded:aDownload=>{if(!aDownload.stopped){downloadsSet.add(aDownload);}},onDownloadChanged:aDownload=>{if(aDownload.stopped){downloadsSet.delete(aDownload);}else{downloadsSet.add(aDownload);}},onDownloadRemoved:aDownload=>{downloadsSet.delete(aDownload);this._canceledOfflineDownloads.delete(aDownload);}};aList.addView(downloadsView).then(null,Cu.reportError);},_confirmCancelDownloads:function DO_confirmCancelDownload(aCancel,aDownloadsCount,aPrompter,aPromptType){if((aCancel instanceof Ci.nsISupportsPRBool)&&aCancel.data){return;} 
if(gCombinedDownloadIntegration._testPromptDownloads){gCombinedDownloadIntegration._testPromptDownloads=aDownloadsCount;return;}
aCancel.data=aPrompter.confirmCancelDownloads(aDownloadsCount,aPromptType);},_resumeOfflineDownloads:function DO_resumeOfflineDownloads(){this._wakeTimer=null;for(let download of this._canceledOfflineDownloads){download.start().catch(()=>{});}}, observe:function DO_observe(aSubject,aTopic,aData){let downloadsCount;let p=DownloadUIHelper.getPrompter();switch(aTopic){case"network-active-changed":let active=gNetworkManager.activeNetworkInfo;for(let download of this._publicInProgressDownloads){download.cancel();this._canceledOfflineDownloads.add(download);}
for(let download of this._privateInProgressDownloads){download.cancel();this._canceledOfflineDownloads.add(download);}
if(active){this._resumeOfflineDownloads();}
break;case"quit-application-requested":downloadsCount=this._publicInProgressDownloads.size+
this._privateInProgressDownloads.size;this._confirmCancelDownloads(aSubject,downloadsCount,p,p.ON_QUIT);break;case"offline-requested":downloadsCount=this._publicInProgressDownloads.size+
this._privateInProgressDownloads.size;this._confirmCancelDownloads(aSubject,downloadsCount,p,p.ON_OFFLINE);break;case"last-pb-context-exiting":downloadsCount=this._privateInProgressDownloads.size;this._confirmCancelDownloads(aSubject,downloadsCount,p,p.ON_LEAVE_PRIVATE_BROWSING);break;case"last-pb-context-exited":let promise=Task.spawn(function(){let list=yield Downloads.getList(Downloads.PRIVATE);let downloads=yield list.getAll();for(let download of downloads){list.remove(download).then(null,Cu.reportError);download.finalize(true).then(null,Cu.reportError);}}); if(gCombinedDownloadIntegration._testResolveClearPrivateList){gCombinedDownloadIntegration._testResolveClearPrivateList(promise);}else{promise.catch(ex=>Cu.reportError(ex));}
break;case"sleep_notification":case"suspend_process_notification":for(let download of this._publicInProgressDownloads){download.cancel();this._canceledOfflineDownloads.add(download);}
for(let download of this._privateInProgressDownloads){download.cancel();this._canceledOfflineDownloads.add(download);}
break;case"wake_notification":case"resume_process_notification":let wakeDelay=10000;try{wakeDelay=Services.prefs.getIntPref("browser.download.manager.resumeOnWakeDelay");}catch(e){}
if(wakeDelay>=0){this._wakeTimer=new Timer(this._resumeOfflineDownloads.bind(this),wakeDelay,Ci.nsITimer.TYPE_ONE_SHOT);}
break;




case"xpcom-will-shutdown":for(let topic of kObserverTopics){Services.obs.removeObserver(this,topic);}
break;}}, QueryInterface:XPCOMUtils.generateQI([Ci.nsIObserver])};
this.DownloadHistoryObserver=function(aList){} 
this.DownloadAutoSaveView=function(aList,aStore)
{this._list=aList;this._store=aStore;this._downloadsMap=new Map();this._writer=new DeferredTask(()=>this._store.save(),kSaveDelayMs);AsyncShutdown.profileBeforeChange.addBlocker("DownloadAutoSaveView: writing data",()=>this._writer.finalize());}
this.DownloadAutoSaveView.prototype={_list:null,_store:null,_initialized:false,initialize:function()
{
return this._list.addView(this).then(()=>this._initialized=true);},_downloadsMap:null,_writer:null,saveSoon:function()
{this._writer.arm();}, onDownloadAdded:function(aDownload)
{if(gCombinedDownloadIntegration.shouldPersistDownload(aDownload)){this._downloadsMap.set(aDownload,aDownload.getSerializationHash());if(this._initialized){this.saveSoon();}}},onDownloadChanged:function(aDownload)
{if(!gCombinedDownloadIntegration.shouldPersistDownload(aDownload)){if(this._downloadsMap.has(aDownload)){this._downloadsMap.delete(aDownload);this.saveSoon();}
return;}
let hash=aDownload.getSerializationHash();if(this._downloadsMap.get(aDownload)!=hash){this._downloadsMap.set(aDownload,hash);this.saveSoon();}},onDownloadRemoved:function(aDownload)
{if(this._downloadsMap.has(aDownload)){this._downloadsMap.delete(aDownload);this.saveSoon();}},};