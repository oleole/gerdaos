this.EXPORTED_SYMBOLS=["PromptUtils","EnableDelayHelper"];const Cc=Components.classes;const Ci=Components.interfaces;const Cr=Components.results;const Cu=Components.utils;Cu.import("resource://gre/modules/Services.jsm");this.PromptUtils={




fireDialogEvent:function(domWin,eventName,maybeTarget,detail){let target=maybeTarget||domWin;let eventOptions={cancelable:true,bubbles:true};if(detail){eventOptions.detail=detail;}
let event=new domWin.CustomEvent(eventName,eventOptions);let winUtils=domWin.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils);winUtils.dispatchEventToChromeOnly(target,event);},objectToPropBag:function(obj){let bag=Cc["@mozilla.org/hash-property-bag;1"].createInstance(Ci.nsIWritablePropertyBag2);bag.QueryInterface(Ci.nsIWritablePropertyBag);for(let propName in obj)
bag.setProperty(propName,obj[propName]);return bag;},propBagToObject:function(propBag,obj){


for(let propName in obj)
obj[propName]=propBag.getProperty(propName);},};this.EnableDelayHelper=function({enableDialog,disableDialog,focusTarget}){this.enableDialog=makeSafe(enableDialog);this.disableDialog=makeSafe(disableDialog);this.focusTarget=focusTarget;this.disableDialog();this.focusTarget.addEventListener("blur",this,false);this.focusTarget.addEventListener("focus",this,false);this.focusTarget.document.addEventListener("unload",this,false);this.startOnFocusDelay();};this.EnableDelayHelper.prototype={get delayTime(){return Services.prefs.getIntPref("security.dialog_enable_delay");},handleEvent:function(event){if(event.target!=this.focusTarget&&event.target!=this.focusTarget.document)
return;switch(event.type){case"blur":this.onBlur();break;case"focus":this.onFocus();break;case"unload":this.onUnload();break;}},onBlur:function(){this.disableDialog();
if(this._focusTimer){this._focusTimer.cancel();this._focusTimer=null;}},onFocus:function(){this.startOnFocusDelay();},onUnload:function(){this.focusTarget.removeEventListener("blur",this,false);this.focusTarget.removeEventListener("focus",this,false);this.focusTarget.document.removeEventListener("unload",this,false);if(this._focusTimer){this._focusTimer.cancel();this._focusTimer=null;}
this.focusTarget=this.enableDialog=this.disableDialog=null;},startOnFocusDelay:function(){if(this._focusTimer)
return;this._focusTimer=Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);this._focusTimer.initWithCallback(()=>{this.onFocusTimeout();},this.delayTime,Ci.nsITimer.TYPE_ONE_SHOT);},onFocusTimeout:function(){this._focusTimer=null;this.enableDialog();},};function makeSafe(fn){return function(){try{fn();}catch(e){}};}